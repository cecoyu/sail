<?php

namespace sail\plugin;

use yii\base\Model;

use sail\db\MigrationManager;

interface PluginInterface
{
	/**
	 * 插件的唯一引用句柄
	 * @return string
	 */
	public function getHandle();

	/**
	 * 获取插件版本
	 * @return string
	 */
	public function getVersion();

	/**
	 * 执行插件安装
	 * @return void|false
	 */
	public function install();

	/**
	 * 执行插件卸载
	 * @return void|false
	 */
	public function uninstall();

	/**
	 * 获取插件设置
	 * @return Model|null
	 */
	public function getSettings();

	/**
	 * 保存插件设置
	 *
	 * @param array $settings
	 */
	public function setSettings($settings);

	/**
	 * @return MigrationManager
	 */
	public function getMigrator();

	/**
	 * 返回该插件的后台导航定义
	 * @return array|null
	 */
	public function getNavItems();
}