<?php

namespace sail\plugin;

use Sail;
use sail\i18n\PhpMessageSource;
use sail\db\MigrationException;
use sail\db\Migration;
use sail\db\MigrationManager;
use yii\base\Model;
use yii\base\Module;
use yii\helpers\ArrayHelper;

/**
 * @property string $handle
 * @property MigrationManager $migrator
 */
class Plugin extends Module implements PluginInterface
{
	/**
	 * @var string|null 插件名称
	 */
	public $name;

	/**
	 * @var string|null 插件描述
	 */
	public $description;

	/**
	 * @var string|null 插件作者
	 */
	public $author;

	/**
	 * @var string|null 作者主页
	 */
	public $homePage;

	/**
	 * @var string|null 作者邮箱
	 */
	public $email;

	/**
	 * @var string|null 插件文档地址，使用markdown格式
	 */
	public $document;

	/**
	 * @var string|null 插件更改日志地址，使用markdown格式
	 */
	public $changelog;

	/**
	 * @var string|null 插件下载地址
	 */
	public $download;

	/**
	 * @var string|null 翻译目录
	 */
	public $t9nCategory;

	/**
	 * 当前模块指定的源语言
	 * @var string
	 */
	public $sourceLanguage = 'zh-CN';

	/**
	 * @var bool 该模块是否可以设置参数
	 */
	public $hasSettings = false;

	/**
	 * @var bool 该插件是否已安装
	 */
	public $isInstalled = false;

	/**
	 * 插件设置模型
	 * @var Model|bool|null
	 */
	private $_settingsModel;

	/** @inheritdoc */
	public function __construct($id, $parent = null, $config = [])
	{
		$this->t9nCategory    = ArrayHelper::remove($config, 't9nCategory', $this->t9nCategory ?: $id);
		$this->sourceLanguage = ArrayHelper::remove($config, 'sourceLanguage', $this->sourceLanguage);

		if (($basePath = ArrayHelper::remove($config, 'basePath')) !== null) {
			$this->setBasePath($basePath);
		}

		// 翻译目录
		$i18n = Sail::$app->getI18n();
		if (!isset($i18n->translations[$this->t9nCategory]) && !isset($i18n->translations[$this->t9nCategory . '*'])) {
			$i18n->translations[$this->t9nCategory] = [
				'class'          => PhpMessageSource::class,
				'sourceLanguage' => $this->sourceLanguage,
				'basePath'       => $this->getBasePath() . DIRECTORY_SEPARATOR . 'translations',
				'forceTranslation' => true,
				'allowOverrides' => true
			];
		}

		// 将其设置为模块类的全局实例
		static::setInstance($this);

		if ($this->controllerNamespace === null && ($pos = strrpos(static::class, '\\')) !== false) {
			$namespace = substr(static::class, 0, $pos);
			$this->controllerNamespace = $namespace . '\\controllers';
		}

		// 完成剩下的初始化工作
		parent::__construct($id, $parent, $config);
	}

	/** @inheritdoc */
	public function getHandle()
	{
		return $this->id;
	}

	/** @inheritdoc */
	public function install()
	{
		if ($this->beforeInstall() === false) {
			return false;
		}

		$migrator = $this->getMigrator();

		// 运行安装迁移动作
		if (($migration = $this->createInstallMigration()) !== null) {
			try {
				$migrator->migrateUp($migration);
			} catch (MigrationException $e) {
				return false;
			}
		}

		foreach ($migrator->getNewMigrations() as $name) {
		    $migrator->addMigrationHistory($name);
		}

		$this->isInstalled = true;

		$this->afterInstall();

		return null;
	}

	/** @inheritdoc */
	public function uninstall()
	{
		if ($this->beforeUninstall() === false) {
			return false;
		}

		if (($migration = $this->createInstallMigration()) !== null) {
			try {
				$this->getMigrator()->migrateDown($migration);
			} catch (MigrationException $e) {
				return false;
			}
		}

		$this->afterUninstall();

		return null;
	}

	/** @inheritdoc */
	public function getSettings()
	{
		if ($this->_settingsModel === null) {
			$this->_settingsModel = $this->createSettingsModel() ?: false;
		}

		if ($this->_settingsModel !== false) {
			return $this->_settingsModel;
		}

		return null;
	}

	/** @inheritdoc */
	public function setSettings($settings)
	{
		$this->getSettings()->setAttributes($settings, false);
	}

	/** @inheritdoc */
	public function getNavItems()
	{
		$ret = [
			'label' => $this->name,
			'url' => $this->id
		];

		// 指定菜单项图标
		$navIconPath = $this->getBasePath() . DIRECTORY_SEPARATOR . 'icon-nav.svg';
		if (is_file($navIconPath)) {
			$ret['icon'] = $navIconPath;
		}

		return $ret;
	}

	/**
	 * @inheritdoc
	 */
	public function getMigrator()
	{
		return $this->get('migrator');
	}

	/**
	 * @return Migration|null
	 */
	protected function createInstallMigration()
	{
		// 检查模块的迁移文件夹，看是否需要安装迁移数据
		$migrator = $this->getMigrator();
		$path = $migrator->migrationPath . DIRECTORY_SEPARATOR . 'Install.php';

		if (!is_file($path)) {
			return null;
		}

		require_once $path;
		$class = $migrator->migrationNamespace . '\\Install';

		return new $class;
	}

	/**
	 * 在模块安装前执行该函数
	 * @return bool
	 */
	protected function beforeInstall()
	{
		return true;
	}

	/**
	 * 在模块安装后执行该函数
	 */
	public function afterInstall()
	{
	}

	/**
	 * 在模块卸载前执行该函数
	 * @return bool
	 */
	protected function beforeUninstall()
	{
		return true;
	}

	/**
	 * 在模块卸载后执行该函数
	 */
	protected function afterUninstall()
	{
	}

	/**
	 * 创建并返回一个存储插件设置的模型
	 * @return Model|null
	 */
	protected function createSettingsModel()
	{
		return null;
	}
}