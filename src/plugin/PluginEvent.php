<?php

namespace sail\plugin;

use yii\base\Event;

class PluginEvent extends Event
{
	/**
	 * @var PluginInterface|null 与当前事件关联的插件
	 */
	public $plugin;
}