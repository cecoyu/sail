<?php

namespace sail\plugin;

use yii\base\Exception;

class InvalidPluginException extends Exception
{
	/**
	 * @var string|null 无效的插件句柄
	 */
	public $handle;

	/**
	 * constructor.
	 *
	 * @param string $handle
	 * @param null $message
	 * @param int $code
	 */
	public function __construct($handle, $message = null, $code = 0)
	{
		$this->handle = $handle;

		if ($message === null) {
			$message = "No plugin exists with the handle \"{$handle}\".";
		}

		parent::__construct($message, $code);
	}
}