<?php

namespace sail\plugin;

use yii\base\Component;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\Inflector;

use Sail;
use sail\helpers\File;
use sail\helpers\Db;
use sail\helpers\Arr;
use sail\helpers\DT;
use sail\db\MigrationManager;

/**
 * PluginManager Service
 *
 * 逻辑：
 *
 * 该组件的主要业务逻辑是管理插件。
 * 该组件在实例化时会从扩展文件读取所有已添加的（包括未安装的）插件信息
 * 在安装系统时需要将核心插件数据写入到数据库和扩展文件中。扩展文件用于保存插件的基础信息，
 *
 * 每次初始化系统都要从扩展文件读取，需要使用缓存减少IO请求降低资源使用
 *
 * 系统初始化时将调用loadPlugins读取已安装且已启用的插件信息，注册到系统中，从而实现对系统进行扩展。
 *
 * 只允许后台上传zip格式的插件压缩包，可提供创建插件基础模板功能。
 * 插件解压转移到插件目录前要进行检查，不符合条件的终止安装。
 *
 * 模仿chrome添加开发者模式，开启后可以读取已解压的扩展插件（即不是后台上传的）, 对插件进行开发调试.
 */
class PluginManager extends Component
{
	const CONFIG_FILE = 'config.php';

	/**
	 * @event \yii\base\Event 在加载插件前触发该事件
	 */
	const EVENT_BEFORE_LOAD_PLUGINS = 'beforeLoadPlugins';

	/**
	 * @event \yii\base\Event 在加载插件后触发该事件
	 */
	const EVENT_AFTER_LOAD_PLUGINS = 'afterLoadPlugins';

	/**
	 * @event \yii\base\Event 在激活插件前触发该事件
	 */
	const EVENT_BEFORE_ENABLE_PLUGIN = 'beforeEnablePlugin';

	/**
	 * @event \yii\base\Event 在激活插件后触发该事件
	 */
	const EVENT_AFTER_ENABLE_PLUGIN = 'afterEnablePlugin';

	/**
	 * @event \yii\base\Event 在禁用插件前触发该事件
	 */
	const EVENT_BEFORE_DISABLE_PLUGIN = 'beforeDisablePlugin';

	/**
	 * @event \yii\base\Event 在禁用插件后触发该事件
	 */
	const EVENT_AFTER_DISABLE_PLUGIN = 'afterDisablePlugin';

	/**
	 * @event \yii\base\Event 在安装插件前触发该事件
	 */
	const EVENT_BEFORE_INSTALL_PLUGIN = 'beforeInstallPlugin';

	/**
	 * @event \yii\base\Event 在安装插件后触发该事件
	 */
	const EVENT_AFTER_INSTALL_PLUGIN = 'afterInstallPlugin';

	/**
	 * @event \yii\base\Event 在卸载插件前触发该事件
	 */
	const EVENT_BEFORE_UNINSTALL_PLUGIN = 'beforeUninstallPlugin';

	/**
	 * @event \yii\base\Event 在卸载插件后触发该事件
	 */
	const EVENT_AFTER_UNINSTALL_PLUGIN = 'afterUninstallPlugin';

	/**
	 * @event \yii\base\Event 在保存插件设置前触发该事件
	 */
	const EVENT_BEFORE_SAVE_PLUGIN_SETTINGS = 'beforeSavePluginSettings';

	/**
	 * @event \yii\base\Event 在保存插件设置后触发该事件
	 */
	const EVENT_AFTER_SAVE_PLUGIN_SETTINGS = 'afterSavePluginSettings';

	/**
	 * @var bool 是否已为当前请求加载了插件
	 */
	private $_pluginsLoaded = false;

	/**
	 * @var bool 当前是否正在加载插件
	 */
	private $_loadingPlugins = false;

	/**
	 * @var array|null 插件信息集合
	 */
	private $_allPluginInfo;

	/**
	 * @var array|null 缓存已启用的插件信息集合
	 */
	private $_enabledPluginInfo;

	/**
	 * @var array|null 缓存已禁用的插件信息集合
	 */
	private $_disabledPluginInfo;

	/**
	 * @var PluginInterface[] 已注册到系统的插件实例集合
	 */
	private $_plugins = [];

	/**
	 * @var PluginInterface[] 已禁用的插件集合
	 */
	private $_disabledPlugins = [];

	/**
	 * @var array 插件和其所在路径的映射
	 */
	private $_pathMap = [];

	/**
	 * @var string[] 以插件类作为索引的插件句柄集合
	 */
	private $_classPluginHandles = [];

	/**
	 * @var string 插件目录所在路径
	 */
	private $_pluginsPath = '@app/plugins';

	/**
	 * @var string 存储插件信息的文件路径
	 */
	private $_extensionFile = '@sail/extensions.php';

	/**
	 * @var string
	 */
	protected $metaFile;

	/** @inheritdoc */
	public function init()
	{
		$this->_allPluginInfo = [];

		$this->_pluginsPath    = Sail::getAlias($this->_pluginsPath);
		$this->_extensionFile = Sail::getAlias($this->_extensionFile);

		// 加载插件信息
		if (file_exists($this->_extensionFile)) {
			$plugins = require $this->_extensionFile;

			foreach ($plugins as $name => $plugin) {
				if (isset($plugin['basePath'])) {
					$plugin['basePath'] = File::normalizePath(realpath($plugin['basePath']));
				}

				$handle = $this->_normalizeHandle(Arr::remove($plugin, 'handle'));
				$this->_allPluginInfo[$handle] = $plugin;
			}
		}
	}

	/**
	 * 加载已安装且已激活启用的插件到系统
	 *
	 * @return void
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function loadPlugins()
	{
		if ($this->_pluginsLoaded || $this->_loadingPlugins) {
			return;
		}

		$this->_loadingPlugins = true;

		// 触发beforeLoadPlugins事件
		if ($this->hasEventHandlers(self::EVENT_BEFORE_LOAD_PLUGINS)) {
			$this->trigger(self::EVENT_BEFORE_LOAD_PLUGINS);
		}

		// 获取已安装并且已激活的插件
		$this->_enabledPluginInfo = $this->_createPluginQuery()
			->where(['enabled' => 'Y'])
			->indexBy('handle')
			->all();

		foreach ($this->_enabledPluginInfo as $handle => &$row) {
			$row['settings']   = Json::decode($row['settings']);
			$row['created_at'] = DT::toDateTime($row['created_at']);

			try {
				$plugin = $this->createPlugin($handle, $row);
			} catch (InvalidPluginException $e) {
				$plugin = null;
			}

			// 注册插件到系统
			if (!is_null($plugin)) {
				$this->_registerPlugin($plugin);
			}
		}
		unset($row);

		$names = array_column($this->_plugins, 'name');
		array_multisort($names, SORT_NATURAL | SORT_FLAG_CASE, $this->_plugins);

		$this->_loadingPlugins = false;
		$this->_pluginsLoaded  = true;

		// 触发afterLoadPlugins事件
		if ($this->hasEventHandlers(self::EVENT_AFTER_LOAD_PLUGINS)) {
			$this->trigger(self::EVENT_AFTER_LOAD_PLUGINS);
		}
	}

	/**
	 * 是否已为当前请求加载了插件
	 * @return bool
	 */
	public function arePluginsLoaded()
	{
		return $this->_pluginsLoaded;
	}

	/**
	 * 通过句柄返回已安装插件实例
	 *
	 * @param string $handle
	 *
	 * @return PluginInterface|null
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\Exception
	 */
	public function getPlugin($handle)
	{
		$this->loadPlugins();

		if (isset($this->_plugins[$handle])) {
			return $this->_plugins[$handle];
		}

		return null;
	}

	/**
	 * 通过类名返回已安装插件实例
	 *
	 * @param string $class
	 *
	 * @return string|null
	 */
	public function getPluginHandleByClass($class)
	{
		if (array_key_exists($class, $this->_classPluginHandles)) {
			return $this->_classPluginHandles[$class];
		}

		// 找到包含该类的文件夹路径
		try {
			// 添加一个尾部斜杠，避免误报
			$classPath = File::normalizePath(dirname((new \ReflectionClass($class))->getFileName())) . DIRECTORY_SEPARATOR;
		} catch (\ReflectionException $e) {
			return $this->_classPluginHandles[$class] = null;
		}

		// 找到包含此路径的插件（如果存在）
		foreach ($this->_allPluginInfo as $handle => $info) {
			if (isset($info['basePath']) && strpos($classPath, $info['basePath'] . DIRECTORY_SEPARATOR) === 0) {
				return $this->_classPluginHandles[$class] = $handle;
			}
		}

		return $this->_classPluginHandles[$class] = null;
	}

	/**
	 * 返回所有已激活的插件
	 * @return PluginInterface[]
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getAllPlugins()
	{
		$this->loadPlugins();

		return $this->_plugins;
	}

	/**
	 * 返回所有插件的信息，无论是否已经安装
	 * @return array
	 * @throws InvalidPluginException
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\Exception
	 */
	public function getAllPluginInfo()
	{
		$this->loadPlugins();

		// 将要返回的插件信息集合
		$info = [];
		// 保存插件名称，用于排序
		$names = [];

		foreach (array_keys($this->_allPluginInfo) as $handle) {
			$info[$handle] = $this->getPluginInfo($handle);
			$names[]       = $info[$handle]['name'];
		}

		// 按名称排序
		array_multisort($names, SORT_NATURAL | SORT_FLAG_CASE, $info);

		return $info;
	}

	/**
	 * 激活启用指定插件句柄的插件
	 *
	 * @param string $handle
	 *
	 * @return bool
	 * @throws \ReflectionException
	 * @throws \sail\plugin\InvalidPluginException
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\Exception
	 */
	public function enablePlugin($handle)
	{
		if ($this->isPluginEnabled($handle)) {
			// 该插件已处于激活状态
			return true;
		}

		if (($info = $this->getStoredPluginInfo($handle)) === null) {
			throw new InvalidPluginException($handle);
		}

		if (($plugin = $this->createPlugin($handle, $info)) === null) {
			throw new InvalidPluginException($handle);
		}

		// 触发beforeEnablePlugin事件
		if ($this->hasEventHandlers(self::EVENT_BEFORE_ENABLE_PLUGIN)) {
			$this->trigger(self::EVENT_BEFORE_ENABLE_PLUGIN, new PluginEvent([
						'plugin' => $plugin,
					]
				)
			);
		}

		Sail::$app->getDb()
			->createCommand()
			->update('{{%plugins}}', ['enabled' => 'Y'], ['id' => $info['id']])
			->execute();

		$this->_enabledPluginInfo[$handle] = $info;
		$this->_registerPlugin($plugin);

		// 触发afterEnablePlugin事件
		if ($this->hasEventHandlers(self::EVENT_AFTER_ENABLE_PLUGIN)) {
			$this->trigger(self::EVENT_AFTER_ENABLE_PLUGIN, new PluginEvent(['plugin' => $plugin,]));
		}

		return true;
	}

	/**
	 * 禁用插件
	 *
	 * @param string $handle
	 *
	 * @return bool
	 * @throws \ReflectionException
	 * @throws \sail\plugin\InvalidPluginException
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\Exception
	 */
	public function disablePlugin($handle)
	{
		if (!$this->isPluginInstalled($handle)) {
			throw new InvalidPluginException($handle);
		}

		if (!$this->isPluginEnabled($handle)) {
			// 该插件已处于禁用状态
			return true;
		}

		if (($plugin = $this->getPlugin($handle)) === null) {
			throw new InvalidPluginException($handle);
		}

		// 触发beforeDisablePlugin事件
		if ($this->hasEventHandlers(self::EVENT_BEFORE_DISABLE_PLUGIN)) {
			$this->trigger(self::EVENT_BEFORE_DISABLE_PLUGIN, new PluginEvent([
						'plugin' => $plugin,
					]
				)
			);
		}

		Sail::$app->getDb()
			->createCommand()
			->update('{{%plugins}}', ['enabled' => 'N'], ['handle' => $handle])
			->execute();

		unset($this->_enabledPluginInfo[$handle]);
		$this->_unregisterPlugin($plugin);

		// 触发afterDisablePlugin事件
		if ($this->hasEventHandlers(self::EVENT_AFTER_DISABLE_PLUGIN)) {
			$this->trigger(self::EVENT_AFTER_DISABLE_PLUGIN, new PluginEvent([
						'plugin' => $plugin,
					]
				)
			);
		}

		return true;
	}

	/**
	 * 获取指定插件信息
	 *
	 * @param string $handle
	 *
	 * @return array
	 * @throws InvalidPluginException
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\Exception
	 */
	public function getPluginInfo($handle)
	{
		if (!isset($this->_allPluginInfo[$handle])) {
			throw new InvalidPluginException($handle);
		}

		// 从数据库获取已禁用的插件信息
		$this->_loadDisabledPluginInfo();
		//$pluginInfo = isset($this->_enabledPluginInfo[$handle]) ? $this->_enabledPluginInfo[$handle] : isset($this->_disabledPluginInfo[$handle]) ? $this->_disabledPluginInfo[$handle] : null;

		// php 7
		$pluginInfo = $this->_enabledPluginInfo[$handle] ?? $this->_disabledPluginInfo[$handle] ?? null;

		/** @var Plugin|null $plugin */
		$plugin = $this->getPlugin($handle);

		$info = array_merge([
			'developer'        => null,
			'developerUrl'     => null,
			'description'      => null,
			'documentationUrl' => null,
		], $this->_allPluginInfo[$handle]);

		$info['icon'] = $this->getPluginIcon($handle);
		$info['isInstalled'] = $installed = $pluginInfo !== null;
		$info['isEnabled']   = $plugin !== null;
		$info['moduleId']    = $handle;
		$info['hasSettings'] = ($plugin !== null && $plugin->hasSettings);

		return $info;
	}

	/**
	 * 根据插件句柄获取该插件图标
	 * https://www.flaticon.com/
	 * @param string $handle
	 *
	 * @return string
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\Exception
	 */
	public function getPluginIcon($handle)
	{
		// 如果插件已安装则返回自身所在路径
		if (($plugin = $this->getPlugin($handle)) !== null) {
			/** @var Plugin $plugin */
			$basePath = $plugin->getBasePath();
		} else {
			if (($basePath = $this->_allPluginInfo[$handle]['basePath'] ?? false) !== false) {
				$basePath = Sail::getAlias($basePath);
			}
		}

		$iconPath = ($basePath !== false) ? $basePath . DIRECTORY_SEPARATOR . 'icon.svg' : false;

		if ($iconPath === false || !is_file($iconPath) || !File::isSvg($iconPath)) {
			// 设置默认插件图标
			$iconPath = Sail::getAlias('@sail/plugin/icon.svg');
		}

		return file_get_contents($iconPath);
	}

	/**
	 * 根据插件句柄安装插件
	 *
	 * @param string $handle
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function installPlugin($handle)
	{
		// 从数据库加载插件信息
		$this->loadPlugins();

		// 该插件是否已安装
		if ($this->getStoredPluginInfo($handle) !== null) {
			return true;
		}

		/** @var Plugin $plugin 创建插件实例 */
		$plugin = $this->createPlugin($handle);

		// 触发beforeInstallPlugin事件
		if ($this->hasEventHandlers(self::EVENT_BEFORE_INSTALL_PLUGIN)) {
			$this->trigger(self::EVENT_BEFORE_INSTALL_PLUGIN, new PluginEvent([
				'plugin' => $plugin
			]));
		}

		// 开启事务
		$transaction = Sail::$app->getDb()->beginTransaction();
		try {
			$info = [
				'handle'     => $handle,
				'version'    => $plugin->getVersion(),
				'enabled' => true,
				'created_at' => Db::prepareDateForDb(new \DateTime()),
			];

			Sail::$app->getDb()->createCommand()
				->insert('{{%plugin}}', $info)
				->execute();

			$info['id']         = Sail::$app->getDb()->getLastInsertID('{{%plugin}}');

			$this->_setPluginMigrator($plugin, $info['id']);

			if ($plugin->install() === false) {
				$transaction->rollBack();

				return false;
			}

			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();

			throw $e;
		}

		$this->_enabledPluginInfo[$handle] = $info;
		$this->_registerPlugin($plugin);

		// 触发afterInstallPlugin事件
		if ($this->hasEventHandlers(self::EVENT_AFTER_INSTALL_PLUGIN)) {
			$this->trigger(self::EVENT_AFTER_INSTALL_PLUGIN, new PluginEvent(['plugin' => $plugin])
			);
		}

		return true;
	}

	/**
	 * 根据插件句柄卸载插件
	 *
	 * @param string $handle
	 *
	 * @return bool
	 * @throws InvalidPluginException
	 * @throws \Exception
	 */
	public function uninstallPlugin($handle)
	{
		$this->loadPlugins();

		if (!$this->isPluginEnabled($handle)) {
			// 不允许卸载已禁用了的插件
			/*if ($this->isPluginInstalled($handle)) {
				throw new InvalidPluginException($handle, 'Uninstalling disabled plugins is not allowed.');
			}*/

			// 该插件已卸载
			return true;
		}

		$plugin = $this->getPlugin($handle);

		if ($plugin === null) {
			throw new InvalidPluginException($handle);
		}

		// 卸载前触发beforeUninstallPlugin事件
		if ($this->hasEventHandlers(self::EVENT_BEFORE_UNINSTALL_PLUGIN)) {
			$this->trigger(self::EVENT_BEFORE_UNINSTALL_PLUGIN, new PluginEvent([
						'plugin' => $plugin,
					]
				)
			);
		}

		$transaction = Sail::$app->getDb()->beginTransaction();
		try {
			// 让插件卸载它自己
			if ($plugin->uninstall() === false) {
				$transaction->rollback();

				return false;
			}

			// 清理插件、迁移表
			$id = $this->_enabledPluginInfo[$handle]['id'];

			Sail::$app->getDb()->createCommand()->delete('{{%plugins%}}', ['id' => $id])->execute();

			$transaction->commit();
		} catch (\Exception $e) {
			// 回滚
			$transaction->rollBack();

			throw $e;
		}

		$this->_unregisterPlugin($plugin);
		unset($this->_enabledPluginInfo[$handle]);

		// 触发afterUninstallPlugin事件
		if ($this->hasEventHandlers(self::EVENT_AFTER_UNINSTALL_PLUGIN)) {
			$this->trigger(self::EVENT_AFTER_UNINSTALL_PLUGIN, new PluginEvent(['plugin' => $plugin])
			);
		}

		return true;
	}

	/**
	 * @param string $handle
	 *
	 * @return array|bool|mixed|null
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getStoredPluginInfo($handle)
	{
		$this->loadPlugins();

		if (isset($this->_enabledPluginInfo[$handle])) {
			return $this->_enabledPluginInfo[$handle];
		}

		$row = $this->_createPluginQuery()->where(['handle' => $handle])->one();

		if (!$row) {
			return null;
		}

		$row['settings']  = Json::decode($row['settings']);
		$row['createdAt'] = DT::toDateTime($row['createdAt']);

		return $row;
	}

	/**
	 * @return array
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getNotInstallPlugins()
	{
		$plugins = [];

		if (!is_dir($this->_pluginsPath)) {
			return [];
		}

		foreach (scandir($this->_pluginsPath) as $name) {
			if ($name === '.' || $name === '..' || $this->isPluginInstalled($name)) {
				continue;
			}

			$pluginDir  = $this->getPluginPath($name);
			$configFile = $pluginDir . DIRECTORY_SEPARATOR . self::CONFIG_FILE;

			if (is_dir($pluginDir) && file_exists($configFile)) {
				$config = Json::decode(file_get_contents($configFile));

				if (!isset($config['handle'])) {
					$config['handle'] = $name;
				}

				$plugins[$config['handle']] = $config;
			}
		}

		return $plugins;
	}

	/**
	 * 根据句柄创建并返回一个新插件实例
	 *
	 * @param string $handle
	 * @param array|null $row
	 *
	 * @return PluginInterface
	 * @throws \ReflectionException
	 * @throws \sail\plugin\InvalidPluginException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function createPlugin($handle, $row = null)
	{
		// 确保存在该插件
		if (!isset($this->_allPluginInfo[$handle])) {
			throw new InvalidPluginException($handle);
		}

		// 读取插件配置文件
		$config = $this->_allPluginInfo[$handle];

		// 注册路径别名
		if (isset($config['aliases'])) {
			foreach ($config['aliases'] as $alias => $path) {
				Sail::setAlias($alias, $path);
			}

			unset($config['aliases']);
		}

		$class = $config['class'];

		// 确保该类存在并实现了PluginInterface接口
		if (!is_subclass_of($class, PluginInterface::class)) {
			return null;
		}

		// 当前插件已安装
		if ($row !== null) {
			$settings = Arr::merge($row['settings'] ?? [], // todo: php7
				Sail::$app->config->getConfigFromFile($handle)
			);

			if ($settings !== []) {
				$config['settings'] = $settings;
			}
		}

		// 创建该插件实例
		/** @var Plugin $plugin */
		$plugin = Sail::createObject($config, [$handle, Sail::$app]);

		if ($row !== null) {
			$this->_setPluginMigrator($plugin, $row['id']);
		}

		return $plugin;
	}

	/**
	 * 保存插件设置
	 *
	 * @param PluginInterface $plugin
	 * @param array $settings
	 *
	 * @return bool
	 * @throws \yii\db\Exception
	 */
	public function savePluginSettings($plugin, $settings)
	{
		/** @var Plugin $plugin */
		// 将设置保存在插件上
		$plugin->getSettings()->setAttributes($settings, false);

		// 验证设置
		if ($plugin->getSettings()->validate() === false) {
			return false;
		}

		// 触发beforeSavePluginSettings事件
		if ($this->hasEventHandlers(self::EVENT_BEFORE_SAVE_PLUGIN_SETTINGS)) {
			$this->trigger(self::EVENT_BEFORE_SAVE_PLUGIN_SETTINGS, new PluginEvent(['plugin' => $plugin])
			);
		}

		$affectedRows = Sail::$app->getDb()
			->createCommand()
			->update('{{%plugin%}}', ['settings' => Json::encode($plugin->getSettings())], ['handle' => $plugin->id]
			)
			->execute();

		// 触发afterSavePluginSettings事件
		if ($this->hasEventHandlers(self::EVENT_AFTER_SAVE_PLUGIN_SETTINGS)) {
			$this->trigger(self::EVENT_AFTER_SAVE_PLUGIN_SETTINGS, new PluginEvent(['plugin' => $plugin])
			);
		}

		return (bool)$affectedRows;
	}

	/**
	 * 检查指定插件是否已激活
	 *
	 * @param string $handle
	 *
	 * @return bool
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function isPluginEnabled($handle)
	{
		$this->loadPlugins();

		return isset($this->_enabledPluginInfo[$handle]);
	}

	/**
	 * 检查指定插件是否已禁用
	 *
	 * @param string $handle
	 *
	 * @return bool
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function isPluginDisabled($handle)
	{
		return !$this->isPluginEnabled($handle) && $this->isPluginInstalled($handle);
	}

	/**
	 * 检查特定插件是否已安装，即使已被禁用
	 *
	 * @param string $handle
	 *
	 * @return bool
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function isPluginInstalled($handle)
	{
		$this->loadPlugins();

		if (isset($this->_enabledPluginInfo[$handle])) {
			return true;
		}

		return $this->_createPluginQuery()->where(['handle' => $handle])->exists();
	}

	/**
	 * @param string $handle
	 *
	 * @return void
	 * @throws \sail\plugin\InvalidPluginException
	 */
	public function addPlugin($handle)
	{
		$pluginPath = $this->getPluginPath($handle);
		$config = require $pluginPath . DIRECTORY_SEPARATOR . self::CONFIG_FILE;

		// 查找PSR-4，入口类和基础路径
		$class    = isset($config['class']) ? $config['class'] : null;
		$basePath = isset($config['basePath']) ? $config['basePath'] : null;

		// 生成默认别名，参考Yii自身插件扩展架构
		if (empty($config['psr-4'])) {
			$aliases = null;
		} else {
			$aliases = [];

			foreach ($config['psr-4'] as $namespace => $path) {
				if (is_array($path)) {
					continue;
				}

				// 将$path转换为绝对路径
				if (!File::isAbsolutePath($path)) {
					$path = $this->getPluginPath($handle) . DIRECTORY_SEPARATOR . $path;
				}

				$path  = File::normalizePath($path);
				$alias = '@' . str_replace('\\', '/', trim($namespace, '\\'));

				$aliases[$alias] = $path;

				// 使用Plugin.php作为默认插件入口
				if ($class === null && file_exists($path . '/Plugin.php')) {
					$class = $namespace . 'Plugin';
				}

				if ($basePath === null && $class !== null) {
					$n = strlen($namespace);
					// namespace\Plugin
					if (strncmp($namespace, $class, $n) === 0) {
						$testClassPath = $path . '/' . str_replace('\\', '/', substr($class, $n)) . '.php';
						if (file_exists($testClassPath)) {
							$basePath = dirname($testClassPath);
						}
					}
				}
			}
		}

		// class 和 basePath是必须的
		if ($class === null) {
			throw new InvalidPluginException($handle, 'Unable to determine the Plugin class');
		}

		if ($basePath === null) {
			throw new InvalidPluginException($handle, 'Unable to determine the base path');
		}

		// 检查handle是否存在及要符合命名要求
		if (!isset($config['handle']) || !preg_match('/^[a-zA-Z][\w\-]*$/', $config['handle'])) {
			throw new InvalidPluginException($handle, 'Invalid or missing plugin handle');
		}

		// 规范化handle
		$handle = $config['handle'];
		if (strtolower($handle) !== $handle) {
			$handle = $this->$this->_normalizeHandle($handle);
			// 防止存在两个'-'
			$handle = preg_replace('/\-{2,}/', '-', $handle);
		}

		$plugin = [
			'class'    => $class,
			'basePath' => $basePath,
			'handle'   => $handle,
		];

		// 路径别名
		if ($aliases) {
			$plugin['aliases'] = $aliases;
		}

		// 插件名称
		if (isset($config['name'])) {
			$plugin['name'] = $config['name'];
		} else {
			$plugin['name'] = $handle;
		}

		// 版本号
		if (isset($config['version'])) {
			$plugin['version'] = $config['version'];
		} else {
			$plugin['version'] = '1.0.0';
		}

		// 插件描述
		if (isset($config['description'])) {
			$plugin['description'] = $config['description'];
		}

		// 开发人员
		if (isset($config['author'])) {
			$plugin['author'] = $config['author'];
		}

		// 翻译
		if (isset($config['t9nCategory'])) {
			$plugin['t9nCategory'] = $config['t9nCategory'];
		}

		// 是否存在后台设置
		if (isset($config['hasSettings'])) {
			$plugin['hasSettings'] = (bool)$config['hasSettings'];
		}

		// 组件定义
		if (isset($config['components'])) {
			$plugin['components'] = $config['components'];
		}

		// 子模块定义
		if (isset($config['modules'])) {
			$plugin['modules'] = $config['modules'];
		}

		// bootstrap
		if (isset($config['bootstrap'])) {
			$plugin['bootstrap'] = $config['bootstrap'];
		}

		$extensions = $this->loadExtensions();
		$extensions[$handle] = $plugin;
		$this->saveExtensions($extensions);
	}

	protected function loadExtensions()
	{
		$file = $this->_extensionFile;
		if (!is_file($file)) {
			return [];
		}

		if (function_exists('opcache_invalidate')) {
			@opcache_invalidate($file, true);
		}

		$extensions = require($file);

		return $extensions;
	}

	protected function saveExtensions($extensions)
	{
		$file = $this->_extensionFile;
		if (!file_exists(dirname($file))) {
			mkdir(dirname($file), 0777, true);
		}

		$array = str_replace("'<plugins-dir>", '$pluginsDir . \'', var_export($extensions, true));
		file_put_contents($file, "<?php\n\n\$pluginsDir = dirname(__DIR__);\n\nreturn $array;\n");

		if (function_exists('opcache_invalidate')) {
			@opcache_invalidate($file, true);
		}
	}

	/**
	 * 获取插件目录路径
	 *
	 * @param string $handle
	 *
	 * @return string
	 */
	protected function getPluginPath($handle)
	{
		return $this->_pluginsPath . DIRECTORY_SEPARATOR . $handle;
	}

	/**
	 * 返回一个查询实例
	 * @return Query
	 */
	private function _createPluginQuery()
	{
		return (new Query())->select([
				'id',
				'handle',
				'name',
				'description',
				'settings',
				'author',
				'version',
				'enabled',
				'iscore',
				'created_at',
			]
		)->from(['{{%plugin}}']);
	}

	/**
	 * 规范化插件句柄的命名方式。将驼峰式(camelCase)命名转换成短横分隔式(kebab-case)
	 *
	 * @param string $handle
	 *
	 * @return string
	 */
	private function _normalizeHandle($handle)
	{
		if (strtolower($handle) !== $handle) {
			$handle = preg_replace('/\-{2,}/', '-', Inflector::camel2id($handle));
		}

		return $handle;
	}

	/**
	 * 在插件上设置'migrator'组件
	 *
	 * @param PluginInterface $plugin
	 * @param int $id
	 *
	 * @throws \ReflectionException
	 * @throws \yii\base\InvalidConfigException
	 */
	private function _setPluginMigrator($plugin, $id)
	{
		$ref = new \ReflectionClass($plugin);
		$ns  = $ref->getNamespaceName();
		/** @var Plugin $plugin */
		$plugin->set('migrator', [
				'class'              => MigrationManager::class,
				'type'               => MigrationManager::TYPE_PLUGIN,
				'pluginId'           => $id,
				'migrationNamespace' => ($ns ? $ns . '\\' : '') . 'migrations',
				'migrationPath'      => $plugin->getBasePath() . DIRECTORY_SEPARATOR . 'migrations',
			]
		);
	}

	/**
	 * 注册插件到应用
	 *
	 * @param PluginInterface $plugin
	 */
	private function _registerPlugin($plugin)
	{
		/** @var Plugin $plugin */
		$this->_plugins[$plugin->id] = $plugin;
		Sail::$app->setModule($plugin->id, $plugin);
	}

	/**
	 * 从应用中卸载插件
	 *
	 * @param PluginInterface $plugin
	 */
	private function _unregisterPlugin($plugin)
	{
		/** @var Plugin $plugin */
		unset($this->_plugins[$plugin->id]);
		Sail::$app->setModule($plugin->id, null);
	}

	/**
	 * 加载已禁用插件记录列表
	 */
	private function _loadDisabledPluginInfo()
	{
		if ($this->_disabledPluginInfo === null) {
			$this->_disabledPluginInfo = $this->_createPluginQuery()
				->where(['enabled' => 'N'])
				->indexBy('handle')
				->all();
		}
	}
}