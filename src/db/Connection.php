<?php

namespace sail\db;

use Sail;
use sail\config\Db;


/**
 * @inheritdoc
 * @method Command createCommand($sql = null, $params = [])
 */
class Connection extends \yii\db\Connection
{
	/**
	 * @event BackupEvent 在创建备份前触发该事件
	 */
	const EVENT_BEFORE_CREATE_BACKUP = 'beforeCreateBackup';

	/**
	 * @event BackupEvent 在创建备份后触发该事件
	 */
	const EVENT_AFTER_CREATE_BACKUP = 'afterCreateBackup';

	/**
	 * @event RestoreEvent 在恢复备份前触发该事件
	 */
	const EVENT_BEFORE_RESTORE_BACKUP = 'beforeRestoreBackup';

	/**
	 * @event RestoreEvent 在恢复备份后触发该事件
	 */
	const EVENT_AFTER_RESTORE_BACKUP = 'afterRestoreBackup';

	/**
	 * 当前是否为MySQL连接
	 * @return bool
	 */
	public function getIsMysql()
	{
		return $this->driverName === Db::DRIVER_MYSQL;
	}

	public function backup()
	{
		$currentVersion = 'v' . Sail::$app->getVersion();
		//$systemName = FileHelper::sanitizeFilename($this->_getFixedSystemName(), ['ascillOnly' => true]);
	}

	/**
	 * @param $table
	 * @param null $refresh
	 *
	 * @return bool
	 * @throws \yii\base\NotSupportedException
	 * @throws \yii\db\Exception
	 */
	public function tableExists($table, $refresh = null)
	{
		if ($refresh || (!is_null($refresh) && !Sail::$app->getIsInstalled())) {
			$this->getSchema()->refresh();
		}

		$table = $this->schema->getRawTableName($table);

		return in_array($table, $this->schema->tableNames, true);
	}
}