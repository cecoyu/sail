<?php

namespace sail\db;

use yii\db\Connection;

class Query extends \yii\db\Query
{
	/**
	 * @event \yii\base\Event
	 */
	const EVENT_INIT = 'init';

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		if ($this->hasEventHandlers(self::EVENT_INIT)) {
			$this->trigger(self::EVENT_INIT);
		}
	}

	/**
	 * @inheritdoc
	 */
	public function one($db = null)
	{
		$limit = $this->limit;
		$this->limit = 1;
		try {
			$result = parent::one($db);
			if ($result === false) {
				$result = null;
			}
		} catch (QueryAbortedException $e) {
			$result = false;
		}

		$this->limit = $limit;
		return $result;
	}

	/**
	 * @inheritdoc
	 */
	public function scalar($db = null)
	{
		$limit = $this->limit;
		$this->limit = 1;
		try {
			$result = parent::scalar($db);
		} catch (QueryAbortedException $e) {
			$result = false;
		}
		$this->limit = $limit;
		return $result;
	}

	/**
	 * @inheritdoc
	 */
	public function column($db = null)
	{
		try {
			return parent::column($db);
		} catch (QueryAbortedException $e) {
			return [];
		}
	}

	/**
	 * @inheritdoc
	 */
	public function exists($db = null)
	{
		try {
			return parent::exists($db);
		} catch (QueryAbortedException $e) {
			return false;
		}
	}

	/**
	 * 执行查询并返回指定偏移值的单行结果
	 *
	 * @param int $n
	 * @param Connection $db
	 * @return array|null
	 */
	public function nth($n, $db = null)
	{
		$offset = $this->offset;
		$this->offset = ($offset ?: 0) + $n;
		$result = $this->one($db);
		$this->offset = $offset;

		return $result;
	}

	/**
	 * `createCommand()->getRawSql()`的快捷方式
	 *
	 * @param Connection $db
	 * @return string
	 */
	public function getRawSql($db)
	{
		return $this->createCommand($db)->getRawSql();
	}

	/**
	 * @inheritdoc
	 */
	protected function queryScalar($selectExpression, $db)
	{
		try {
			return parent::queryScalar($selectExpression, $db);
		} catch (QueryAbortedException $e) {
			return false;
		}
	}
}