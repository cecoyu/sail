<?php

namespace sail\db;

use yii\db\ColumnSchemaBuilder;

abstract class Migration extends \yii\db\Migration
{
	/**
	 * @param bool $throwExceptions
	 *
	 * @return bool|null
	 * @throws \Throwable
	 * @throws \yii\db\Exception
	 */
	public function up($throwExceptions = false)
	{
		$transaction = $this->db->beginTransaction();
		try {
			if ($this->safeUp() === false) {
				$transaction->rollBack();
				return false;
			}
			$transaction->commit();
		} catch (\Throwable $e) {
			$this->_printException($e);
			$transaction->rollBack();
			if ($throwExceptions) {
				throw $e;
			}
			return false;
		}

		return null;
	}

	/**
	 * @param bool $throwExceptions
	 *
	 * @return bool|null
	 * @throws \Throwable
	 */
	public function down($throwExceptions = false)
	{
		$transaction = $this->db->beginTransaction();
		try {
			if ($this->safeDown() === false) {
				$transaction->rollBack();
				return false;
			}
			$transaction->commit();
		} catch (\Throwable $e) {
			$this->_printException($e);
			$transaction->rollBack();
			if ($throwExceptions) {
				throw $e;
			}

			return false;
		}

		return null;
	}

	/**
	 * @param $e \Throwable|\Exception $e
	 */
	private function _printException($e)
	{
		echo 'Exception:' . $e->getMessage() . ' (' . $e->getFile() . ':' . $e->getLine() . ")\n";
		echo $e->getTraceAsString() . "\n";
	}
}