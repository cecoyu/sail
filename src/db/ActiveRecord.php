<?php

namespace sail\db;

use sail\helpers\Db;
use sail\helpers\Str;

/**
 * Active Record
 *
 * @property string $created_at
 * @property string $updated_at
 * @property string $uuid
 */
abstract class ActiveRecord extends \yii\db\ActiveRecord
{
	/**
	 * 在插入或更新记录前调用该方法
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		foreach ($this->fields() as $attribute) {
			$this->$attribute = Db::prepareValueForDb($this->$attribute);
		}

		// 自动维护created_at, updated_at, UID等字段
		$now = Db::prepareDateForDb(new \DateTime());

		if ($this->isNewRecord) {
			if ($this->hasAttribute('created_at')) {
				$this->created_at = $now;
			}

			if ($this->hasAttribute('uuid')) {
				$this->uuid = Str::UUID();
			}
		}

		if ($this->hasAttribute('updated_at')) {
			$this->updated_at = $now;
		}

		return parent::beforeSave($insert);
	}
}