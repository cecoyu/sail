<?php

namespace sail\db;

use yii\base\Exception;
use yii\db\Migration;

class MigrationException extends Exception
{
	/**
	 * @var Migration
	 */
	public $migration;

	/**
	 * @var string|null
	 */
	public $output;

	/**
	 * MigrationException constructor.
	 *
	 * @param Migration $migration
	 * @param string|null $output
	 * @param string|null $message
	 * @param int $code
	 * @param \Throwable $previous
	 */
	public function __construct($migration, $output = null, $message = null, $code = 0, $previous = null)
	{
		$this->migration = $migration;
		$this->output = $output;

		if ($message === null) {
			$message = 'An error occurred while executing the ' . get_class($migration) . ' migration' . ($previous ? ': ' . $previous->getMessage() : '.');
		}

		parent::__construct($message, $code, $previous);
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return 'Migration Error';
	}
}