<?php

namespace sail\db;

use Sail;

class DbConnectException extends \yii\base\UserException
{
	public function getName()
	{
		return Sail::t('app', 'Database Connection Exception');
	}
}