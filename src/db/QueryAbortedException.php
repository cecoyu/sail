<?php

namespace sail\db;

use yii\base\Exception;

class QueryAbortedException extends Exception
{
	public function getName()
	{
		return 'Query Aborted Exception';
	}
}