<?php

namespace sail\db;

use Sail;
use sail\helpers\File;
use sail\helpers\Db;

use yii\base\Exception;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\db\MigrationInterface;
use yii\di\Instance;

class MigrationManager extends Component
{
	// 虚设的迁移名称，标记整个迁移历史的开始
	const BASE_MIGRATION = 'm000000_000000_base';

	const TYPE_APP    = 'app';
	const TYPE_PLUGIN = 'plugin';

	/**
	 * @var string|null 当前迁移的类型
	 */
	public $type;

	/**
	 * @var string|null 插件ID，如果[[type]]是'plugin'
	 */
	public $pluginId;

	/**
	 * @var string|null 迁移类所在的命名空间
	 */
	public $migrationNamespace;

	/**
	 * @var string|null 迁移所在的目录路径
	 */
	public $migrationPath;

	/**
	 * @var Connection|array|string DB连接的组件实例
	 */
	public $db = 'db';

	/**
	 * @var string 迁移表名称
	 */
	public $migrationTable = '{{%migrations}}';

	/**
	 * @inheritdoc
	 * @throws InvalidConfigException
	 */
	public function init()
	{
		parent::init();

		if (is_null($this->migrationPath)) {
			throw new InvalidConfigException('The migration folder path has not been set.');
		}

		if (!in_array($this->type, [self::TYPE_APP, self::TYPE_PLUGIN], true)) {
			throw new InvalidConfigException('Invalid migration type: ' . $this->type);
		}

		if ($this->type === self::TYPE_PLUGIN && is_null($this->pluginId)) {
			throw new InvalidConfigException('The plugin ID has not been set.');
		}

		if (is_null($this->migrationPath)) {
			throw new InvalidConfigException('The migration path has not been set.');
		}

		$this->migrationPath = File::normalizePath(getAlias($this->migrationPath));

		$this->db = Instance::ensure($this->db, Connection::class);
	}

	/**
	 * 创建新的迁移实例
	 *
	 * @param string $name
	 *
	 * @return MigrationInterface|\yii\db\Migration
	 * @throws Exception
	 */
	public function createMigration($name)
	{
		if (!is_dir($this->migrationPath)) {
			throw new Exception("Can't instantiate migrations because the migration folder doesn't exist");
		}

		$file  = $this->migrationPath . DIRECTORY_SEPARATOR . $name . '.php';
		$class = $this->migrationNamespace . '\\' . $name;

		require_once $file;

		return new $class;
	}

	/**
	 * 通过应用新迁移来升级应用
	 *
	 * @param int $limit
	 *
	 * @throws Exception
	 * @throws InvalidConfigException
	 * @throws MigrationException
	 * @throws \yii\db\Exception
	 */
	public function up($limit = 0)
	{
		$this->iniSet();

		$migrationNames = $this->getNewMigrations();

		if (empty($migrationNames)) {
			Sail::info('No new migration found. Your system is up-to-date.', __METHOD__);

			return;
		}

		$total = count($migrationNames);

		if ($limit !== 0) {
			$migrationNames = array_slice($migrationNames, 0, $limit);
		}

		$n = count($migrationNames);

		if ($n === $total) {
			$logMessage = "Total $n new " . ($n === 1 ? 'migration' : 'migrations') . ' to be applied:';
		} else {
			$logMessage = "Total $n out of $total new " . ($total === 1 ? 'migration' : 'migrations') . ' to be applied:';
		}

		foreach ($migrationNames as $migrationName) {
			$logMessage .= "\n\t$migrationName";
		}

		Sail::info($logMessage, __METHOD__);

		foreach ($migrationNames as $migrationName) {
			try {
				$this->migrateUp($migrationName);
			} catch (MigrationException $e) {
				Sail::error('Migration failed. The rest of the migrations are cancelied.', __METHOD__);

				throw $e;
			}
		}

		Sail::info('Migrated up successfully.', __METHOD__);
	}

	/**
	 * 通过还原旧迁移来降级应用
	 *
	 * @param int $limit
	 *
	 * @throws Exception
	 * @throws InvalidConfigException
	 * @throws MigrationException
	 * @throws \yii\db\Exception
	 */
	public function down($limit = 1)
	{
		$this->iniSet();

		$migrationNames = array_keys($this->getMigrationHistory($limit));

		if (empty($migrationNames)) {
			Sail::info('No migration has been done before.', __METHOD__);

			return;
		}

		$n          = count($migrationNames);
		$logMessage = "Total $n " . ($n === 1 ? 'migration' : 'migrations') . ' to be reverted:';

		foreach ($migrationNames as $migrationName) {
			$logMessage .= "\n\t$migrationName";
		}

		Sail::info($logMessage, __METHOD__);

		foreach ($migrationNames as $migrationName) {
			try {
				$this->migrateDown($migrationName);
			} catch (MigrationException $e) {
				Sail::error('Migration failed. The rest of the migrations are cancelled.', __METHOD__);

				throw $e;
			}
		}

		Sail::info('Migrated down successfully.', __METHOD__);
	}

	/**
	 * 使用指定的迁移进行升级
	 *
	 * @param string|MigrationInterface|\yii\db\Migration $migration
	 *
	 * @throws Exception
	 * @throws InvalidConfigException
	 * @throws MigrationException
	 * @throws \yii\db\Exception
	 */
	public function migrateUp($migration)
	{
		list($migrationName, $migration) = $this->_normalizeMigration($migration);

		if ($migrationName === self::BASE_MIGRATION) {
			return;
		}

		$migration = Instance::ensure($migration, MigrationInterface::class);

		Sail::info("Applying $migrationName", __METHOD__);

		$isConsoleRequest = Sail::$app->getRequest()->getIsConsoleRequest();

		if (!$isConsoleRequest) {
			ob_start();
		}

		$start = microtime(true);
		try {
			if ($migration instanceof Migration) {
				$success = ($migration->up(true) !== false);
			} else {
				$success = ($migration->up() !== false);
			}
		} catch (\Throwable $e) {
			$success = false;
		}
		$time = microtime(true) - $start;

		$log = ($success ? 'Applied ' : 'Failed to apply ') . $migrationName . ' (time: ' . sprintf('%.3f', $time) . 's).';
		if (!$isConsoleRequest) {
			$output = ob_get_clean();
			$log    .= " Output:\n" . $output;
		}

		if (!$success) {
			Sail::error($log, __METHOD__);
			throw new MigrationException($migration, isset($output) ? $output : null, null, 0, isset($e) ? $e : null);
		}

		Sail::info($log, __METHOD__);
		$this->addMigrationHistory($migrationName);
	}

	/**
	 * 使用指定的迁移进行降级
	 *
	 * @param string|MigrationInterface|\yii\db\Migration $migration
	 *
	 * @throws Exception
	 * @throws InvalidConfigException
	 * @throws MigrationException
	 * @throws \yii\db\Exception
	 */
	public function migrateDown($migration)
	{
		list($migrationName, $migration) = $this->_normalizeMigration($migration);

		if ($migrationName === self::BASE_MIGRATION) {
			return;
		}

		$migration = Instance::ensure($migration, MigrationInterface::class);

		Sail::info("Reverting $migrationName", __METHOD__);

		$isConsoleRequest = Sail::$app->getRequest()->getIsConsoleRequest();

		if (!$isConsoleRequest) {
			ob_start();
		}

		$start = microtime(true);
		try {
			if ($migration instanceof Migration) {
				$success = ($migration->down(true) !== false);
			} else {
				$success = ($migration->down() !== false);
			}
		} catch (\Throwable $e) {
			$success = false;
		}

		$time = microtime(true) - $start;

		$log = ($success ? 'Reverted ' : 'Failed to revert ') . $migrationName . ' (time: ' . sprintf('%.3f', $time) . 's).';
		if (!$isConsoleRequest) {
			$output = ob_get_clean();
			$log    .= " Output:\n" . $output;
		}

		if (!$success) {
			Sail::error($log, __METHOD__);
			throw new MigrationException($migration, isset($output) ? $output : null, null, 0, isset($e) ? $e : null);
		}

		Sail::info($log, __METHOD__);
		$this->removeMigrationHistory($migrationName);
	}

	/**
	 * 返回迁移历史纪录
	 *
	 * @param int $limit
	 *
	 * @return mixed
	 */
	public function getMigrationHistory($limit = 0)
	{
		$query = $this->_createMigrationQuery();
		if ($limit !== 0) {
			$query->limit($limit);
		}
		$history = $query->pairs($this->db);
		unset($history[self::BASE_MIGRATION]);

		return $history;
	}

	/**
	 * 添加迁移历史纪录
	 *
	 * @param $name
	 *
	 * @throws \yii\db\Exception
	 */
	public function addMigrationHistory($name)
	{
		Sail::$app->getDb()->createCommand()
			->insert(
				$this->migrationTable,
				[
					'type'      => $this->type,
					'pluginId'  => $this->pluginId,
					'name'      => $name,
					'applyTime' => Db::prepareDateForDb(new \DateTime()),
				]
			)->execute();
	}

	/**
	 * 移除迁移历史纪录
	 *
	 * @param $name
	 *
	 * @throws \yii\db\Exception
	 */
	public function removeMigrationHistory($name)
	{
		Sail::$app->db->createCommand()
			->delete(
				$this->migrationTable,
				[
					'type'     => $this->type,
					'pluginId' => $this->pluginId,
					'name'     => $name,
				]
			)->execute();
	}

	/**
	 * 是否已应用给定的迁移
	 *
	 * @param string $name
	 *
	 * @return bool
	 */
	public function hasRun($name)
	{
		return $this->_createMigrationQuery()->andWhere(['name' => $name])->exists($this->db);
	}

	/**
	 * 返回尚未应用的迁移
	 *
	 * @return array
	 */
	public function getNewMigrations()
	{
		$migrations = [];

		if (!is_dir($this->migrationPath)) {
			return $migrations;
		}

		// 使用已应用的历史纪录和文件夹中的迁移对比
		$history = $this->getMigrationHistory();
		$handle  = opendir($this->migrationPath);

		while (($file = readdir($handle)) !== false) {
			if ($file === '.' || $file === '..') {
				continue;
			}

			$path = $this->migrationPath . DIRECTORY_SEPARATOR . $file;

			if (preg_match('/^(m\d{6}_\d{6}_.*?)\.php$/', $file, $matches) && is_file($path) && !isset($history[$matches[1]])) {
				$migrations[] = $matches[1];
			}
		}

		closedir($handle);
		sort($migrations);

		return $migrations;
	}

	/**
	 * @param $migration
	 *
	 * @return array
	 * @throws Exception
	 */
	private function _normalizeMigration($migration)
	{
		if (is_string($migration)) {
			$migrationName = $migration;
			$migration     = $this->createMigration($migration);
		} else {
			$classParts    = explode('\\', get_class($migration));
			$migrationName = array_pop($classParts);
		}

		return [$migrationName, $migration];
	}

	/**
	 * @return Query
	 */
	private function _createMigrationQuery()
	{
		// TODO: Remove after next breakpoint
		if (version_compare(Sail::$app->getInfo()->version, '3.0', '<')) {
			$query = (new Query())->select(['version as name', 'applyTime'])
				->from([$this->migrationTable])
				->orderBy(['name' => SORT_DESC]);

			if ($this->type === self::TYPE_PLUGIN) {
				$query->where(['pluginId' => $this->pluginId]);
			} else {
				$query->where(['pluginId' => null]);
			}

			return $query;
		}

		$query = (new Query())->select(['name', 'applyTime'])
			->from([$this->migrationTable])
			->orderBy(['name' => SORT_DESC])
			->where(['type' => $this->type]);

		if ($this->type === self::TYPE_PLUGIN) {
			$query->andWhere(['pluginId' => $this->pluginId]);
		}

		return $query;
	}


	/**
	 * 增加运行时内存大小以及禁用脚本的运行时间
	 */
	private function iniSet()
	{
		$generalConfig = Sail::$app->getConfig()->getGeneral();

		if ($generalConfig->phpMaxMenoryLimit !== '') {
			@ini_set('memory_limit', $generalConfig->phpMaxMemoryLimit);
		} else {
			// 不限制使用内存
			@ini_set('memory_limit', -1);
		}

		// 禁用脚本最大执行时间
		@set_time_limit(0);
	}
}