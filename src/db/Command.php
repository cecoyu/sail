<?php

namespace sail\db;

use Sail;
use sail\helpers\Db;
use sail\helpers\Str;

class Command extends \yii\db\Command
{
	/**
	 * @inheritdoc
	 * @param string $table the table that new rows will be inserted into.
	 * @param array|\yii\db\Query $columns the column data (name => value) to be inserted into the table or instance
	 * of [[yii\db\Query|Query]] to perform INSERT INTO ... SELECT SQL statement.
	 * Passing of [[yii\db\Query|Query]] is available since version 2.0.11.
	 * @param bool $includeAuditColumns 是否将`createdAt`, `updatedAt`, `uid`添加到$columns
	 * @return $this the command object itself
	 */
	public function insert($table, $columns, $includeAuditColumns = true)
	{
		if ($includeAuditColumns) {
			$now = Db::prepareDateForDb(new \DateTime());

			if (empty($columns['createdAt'])) {
				$columns['createdAt'] = $now;
			}
			if (empty($columns['updatedAt'])) {
				$columns['updatedAt'] = $now;
			}
			if (empty($columns['uid'])) {
				$columns['uid'] = Str::UUID();
			}
		}

		parent::insert($table, $columns);

		return $this;
	}

	/**
	 * @inheritdoc
	 * @param string $table the table that new rows will be inserted into.
	 * @param array $columns the column names
	 * @param array|\Generator $rows the rows to be batch inserted into the table
	 * @param bool $includeAuditColumns 是否将`createdAt`, `updatedAt`, `uid`添加到$columns
	 * @return $this the command object itself
	 */
	public function batchInsert($table, $columns, $row, $includeAuditColumns = true)
	{
		if (empty($rows)) {
			return $this;
		}

		if ($includeAuditColumns) {
			$columns[] = 'createdAt';
			$columns[] = 'updatedAt';
			$columns[] = 'uid';

			$date = Db::prepareDateForDb(new \DateTime());

			foreach ($rows as &$row) {
				$row[] = $date;
				$row[] = $date;
				$row[] = Str::UUID();
			}
			unset($row);
		}

		parent::batchInsert($table, $columns, $rows);

		return $this;
	}

	public function upsert($table, $insertColumns, $updateColumns = true, $params = [], $includeAuditColumns = true)
	{
		if (is_bool($params)) {
			$includeAuditColumns = $params;
			$params = [];
			Sail::$app->getDeprecator()->log('sail\\db\\Command::upsert($includeAuditColumns)', 'The $includeAuditColumns argument on craft\\db\\Command::upsert() has been moved to the 5th position');
		}

		if ($includeAuditColumns && $updateColumns !== false) {
			if ($updateColumns === true) {
				$updateColumns = array_merge($insertColumns);
			}
			$now = Db::prepareDateForDb(new \DateTime());
			$updateColumns['createdAt'] = $now;
			$updateColumns['updatedAt'] = $now;
			$updateColumns['uid'] = Str::UUID();
		}

		$insertColumns = array_merge($updateColumns, $insertColumns);

		parent::upsert($table, $insertColumns, $updateColumns, $params);
		return $this;
	}

	public function update($table, $columns, $condition = '', $params = [], $includeAuditColumns = true)
	{
		if ($includeAuditColumns) {
			$columns['updatedAt'] = Db::prepareDateForDb(new \DateTime());
		}

		parent::update($table, $columns, $condition, $params);

		return $this;
	}

	/**
	 * @param string $table
	 * @param string $column
	 * @param string $find
	 * @param string $replace
	 * @param string string $condition
	 * @param array $params
	 *
	 * @return $this
	 */
	public function replace($table, $column, $find, $replace, $condition = '', $params = [])
	{
		$sql = $this->db->getQueryBuilder()->replace($table, $column, $find, $replace, $condition, $params);

		return $this->setSql($sql)->bindValues($params);
	}

	/**
	 * @param string $table
	 *
	 * @return $this
	 */
	public function dropTableIfExists($table)
	{
		$sql = $this->db->getQueryBuilder()->dropTableIfExists($table);

		return $this->setSql($sql);
	}

	/**
	 * @param string $oldName
	 * @param string $newName
	 *
	 * @return $this
	 */
	public function renameSequence($oldName, $newName)
	{
		$sql = $this->db->getQueryBuilder()->renameSequence($oldName, $newName);

		return $this->setSql($sql);
	}
}