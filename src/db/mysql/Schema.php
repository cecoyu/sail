<?php

namespace sail\db\mysql;

use Sail;
use sail\db\TableSchema;
use sail\helpers\File;
use yii\db\Exception;

class Schema extends \yii\db\mysql\Schema
{
	// 自定义字段类型
	const TYPE_TINYTEXT   = 'tinytext';
	const TYPE_MEDIUMTEXT = 'mediumtext';
	const TYPE_LONGTEXT   = 'longtext';
	const TYPE_ENUM       = 'enum';

	/**
	 * @var int 对象名称的最大长度
	 */
	public $maxObjectNameLength = 64;

	/** @inheritdoc */
	public function init()
	{
		parent::init();

		$this->typeMap['tinytext']   = self::TYPE_TINYTEXT;
		$this->typeMap['mediumtext'] = self::TYPE_MEDIUMTEXT;
		$this->typeMap['longtext']   = self::TYPE_LONGTEXT;
		$this->typeMap['enum']       = self::TYPE_ENUM;
	}

	/**
	 * @inheritdoc
	 */
	public function createQueryBuilder()
	{
		return new QueryBuilder($this->db, [
			'separator' => "\n"
		]);
	}

	/**
	 * 应用用于查询的数据库名称
	 * @param $name
	 *
	 * @return string
	 */
	public function quoteDatabaseName($name)
	{
		return '`' . $name . '`';
	}

	/**
	 * @inheritdoc
	 *
	 * @throws Exception
	 */
	public function releaseSavepoint($name)
	{
		try {
			parent::releaseSavepoint($name);
		} catch (Exception $e) {
			// 是否为"SAVEPOINT does not exists"错误
			if ($e->getCode() === 42000 && isset($e->errorInfo[1]) && $e->errorInfo[1] == 1305) {
				Sail::warning('Tried to release a savepoint, but it does not exist: ' . $e->getMessage(), __METHOD__);
			} else {
				throw $e;
			}
		}
	}

	/**
	 * @inheritdoc
	 *
	 * @throws Exception
	 */
	public function rollBackSavepoint($name)
	{
		try {
			parent::rollBackSavepoint($name);
		} catch (Exception $e) {
			// 是否为"SAVEPOINT does not exists"错误
			if ($e->getCode() == 42000 && isset($e->errorInfo[1]) && $e->errorInfo[1] == 1305) {
				Sail::warning('Tried to roll back a savepoint, but it does not exist: ' . $e->getMessage(), __METHOD__);
			} else {
				throw $e;
			}
		}
	}

	/** @inheritdoc */
	public function createColumnSchemaBuilder($type, $length = null)
	{
		return new ColumnSchemaBuilder($type, $length, $this->db);
	}

	public function getDefaultBackupCommand()
	{
		$defaultTableIgnoreList = [
			'{{%assetindexdata}}',
			'{{%assettransformindex}}',
			'{{%cache}}',
			'{{%sessions}}',
			'{{%templatecaches}}',
			'{{%templatecachecriteria}}',
			'{{%templatecacheelements}}',
		];

		$dbSchema = Sail::$app->getDb()->getSchema();

		foreach ($defaultTableIgnoreList as $key => $ignoreTable) {
		    $defaultTableIgnoreList[$key] = ' --ignore-table={database}.' . $dbSchema->getRawTableName($ignoreTable);
		}
	}

	public function getDefaultRestoreCommand()
	{

	}
}