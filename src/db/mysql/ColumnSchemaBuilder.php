<?php

namespace sail\db\mysql;

use Sail;

class ColumnSchemaBuilder extends \yii\db\mysql\ColumnSchemaBuilder
{
	/** @inheritdoc */
	public function init()
	{
		$this->categoryMap[Schema::TYPE_TINYTEXT]   = self::CATEGORY_STRING;
		$this->categoryMap[Schema::TYPE_MEDIUMTEXT] = self::CATEGORY_STRING;
		$this->categoryMap[Schema::TYPE_LONGTEXT]   = self::CATEGORY_STRING;
		$this->categoryMap[Schema::TYPE_ENUM]       = self::CATEGORY_STRING;
	}

	/** @inheritdoc */
	protected function buildLengthString()
	{
		if ($this->type === Schema::TYPE_ENUM) {
			$schema = Sail::$app->getDb()->getSchema();

			$str = '(';
			foreach ($this->length as $i => $value) {
				if ($i !== 0) {
					$str .= ',';
				}
				$str .= $schema->quoteValue($value);
			}
			$str .= ')';

			return $str;
		}

		return parent::buildLengthString();
	}
}