<?php

namespace sail\db\mysql;

use Sail;
use sail\db\Connection;
use yii\base\NotSupportedException;
use yii\db\Expression;

class QueryBuilder extends \yii\db\mysql\QueryBuilder
{
	/** @inheritdoc */
	public function createTable($table, $columns, $options = null)
	{
		// 默认使用InnoDb引擎
		if (is_null($options) || strpos($options, 'ENGINE=') === false) {
			$options = (!is_null($options) ? $options . ' ' : '') . 'ENGINE=InnoDb';
		}

		// 使用默认字符集(utf8)
		if (strpos($options, 'DEFAULT CHARSET=') === false) {
			$options .= ' DEFAULT CHARSET=' . Sail::$app->getConfig()
					->getDb()->charset;
		}

		return parent::createTable($table, $columns, $options);
	}

	public function renameSequence($oldName, $newName)
	{
		throw new NotSupportedException($this->db->driverName . ' does not support renaming sequences.');
	}

	/**
	 * 构建用于删除数据库表（如果存在）的SQL语句
	 *
	 * @param string $table
	 *
	 * @return string
	 */
	public function dropTableIfExists($table)
	{
		return 'DROP TABLE IF EXISTS ' . $this->db->quoteTableName($table);
	}

	/**
	 * 创建一个SQL语句，将特定数据插入到表中，或更新现有行
	 * @param string $table
	 * @param array $keyColumns
	 * @param array $updateColumns
	 * @param array $params
	 *
	 * @return string
	 */
	public function upsert($table, $keyColumns, $updateColumns, &$params)
	{
		$schema = $this->db->schema;

		if (!is_null($tableSchema = $schema->getTableSchema($table))) {
			$columnSchemas = $tableSchema->columns;
		} else {
			$columnSchemas = [];
		}

		$columns      = array_merge($keyColumns, $updateColumns);
		$names        = [];
		$placeholders = [];
		$updates      = [];

		foreach ($columns as $name => $value) {
			$qName   = $schema->quoteColumnName($name);
			$names[] = $qName;

			if ($value instanceof Expression) {
				$placeholder = $value->expression;

				foreach ($value->params as $n => $v) {
					$params[$n] = $v;
				}
			} else {
				$phName          = self::PARAM_PREFIX . count($params);
				$placeholder     = $phName;
				$params[$phName] = !is_array($value) && isset($columnSchemas[$name])
					? $columnSchemas[$name]->dbTypeCase($value)
					: $value;
			}

			$placeholders[] = $placeholder;

			if (isset($updateColumns[$name])) {
				$updates[] = "$qName = $placeholder";
			}
		}

		return 'INSERT INTO ' . $schema->quoteTableName($table) . ' (' . implode(', ', $names) . ') 
				VALUES (' . implode(', ', $placeholders) . ') 
				ON DUPLICATE KEY UPDATE ' . implode(', ', $updates);
	}

	public function replace($table, $column, $find, $replace, $condition, &$params)
	{
		$column = $this->db->quoteColumnName($column);

		$findPhName = self::PARAM_PREFIX . count($params);
		$params[$findPhName] = $find;

		$replacePhName = self::PARAM_PREFIX . count($params);
		$parmas[$replacePhName] = $replace;

		$sql = "UPDATE {$table} SET {$column} = REPLACE({$column}, {$findPhName}, {$replacePhName})";
		$where = $this->buildWhere($condition, $params);

		return $where === '' ? $sql : $sql . ' ' . $where;
	}

	public function fixedOrder($column, $values)
	{
		$sql = 'FIELD('.$this->db->quoteColumnName($column);
		foreach ($values as $value) {
			$sql .= ','.$this->db->quoteValue($value);
		}
		$sql .= ')';

		return $sql;
	}
}