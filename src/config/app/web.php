<?php

return [
	'components' => [
		'urlManager'  => [
			'enablePrettyUrl' => true,
			'showScriptName'  => false,
		],
		'assetManager' => [
			'linkAssets' => true,
			'appendTimestamp' => true
		],
		'user' => function () {
			$request = Sail::$app->getRequest();

			if ($request->getIsBackend()) {
				$loginUrl = ['/backend/sign-in/login'];
			} else {
				$loginUrl = ['/user/login'];
			}

			return Sail::createObject([
				'class' => sail\web\User::class,
				'identityClass' => sail\user\models\User::class,
				'enableAutoLogin' => true,
				'autoRenewCookie' => true,
				'loginUrl' => $loginUrl,
				// 用户长时间不活动将自动注销的秒数
				// todo: 作为系统设置
				'authTimeout' => 3600,
			]);
		},
		'request'     => function () {
			$request = Sail::createObject(
				[
					'class'                  => sail\web\Request::class,
					'enableCookieValidation' => true,
					'enableCsrfValidation' => false,
					'cookieValidationKey'    => 'test',
				]
			);

			return $request;
		},
		'authManager' => [
			'class' => sail\auth\DbManager::class,
		],
	],
	// 访问控制过滤
	'as access'  => [
		'class'               => sail\auth\AccessControl::class,
		'strict'              => true,
		'onlyRegisteredRoute' => false,
		'except'              => [
			'site/*', '*'
		],
	],
];