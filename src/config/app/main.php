<?php

return [
	'id'          => 'Sail',
	'name'        => 'Sail Base System',
	'version'     => '1.0.0',
	'basePath'    => ROOT,
	'runtimePath' => '@storage/runtime',

	// 组件配置
	'components' => [
		// 插件
		'plugins' => [
			'class' => sail\plugin\PluginManager::class,
		],

		// 数据库
		'db' => function () {
			$dbConfig = Sail::$app->config->getDb();

			$db = Sail::createObject(
				[
					'class'       => sail\db\Connection::class,
					'dsn'         => $dbConfig->dsn,
					'username'    => $dbConfig->user,
					'password'    => $dbConfig->password,
					'charset'     => $dbConfig->charset,
					'tablePrefix' => $dbConfig->tablePrefix,
					'attributes'  => $dbConfig->attributes,

					'enableSchemaCache' => true,
					'schemaCacheDuration' => 3600,
					'schemaCache' => 'cache'
				]
			);

			$db->setDriverName($dbConfig->driver);

			return $db;
		},

		// 数据库迁移
		'migrator' => [
			'class'              => sail\db\MigrationManager::class,
			'type'               => sail\db\MigrationManager::TYPE_APP,
			'migrationNamespace' => 'sail\migrations',
			'migrationPath'      => '@app/migrations',
		],

		'i18n' => [
			'class'        => sail\i18n\I18N::class,
			'translations' => [
				'yii' => [
					'class'          => yii\i18n\PhpMessageSource::class,
					'sourceLanguage' => 'en-US',
					'basePath'       => '@yii/messages',
				],
				'app' => [
					'class'          => yii\i18n\PhpMessageSource::class,
					'sourceLanguage' => 'en-US',
					'basePath'       => '@app/translations',
				],
			],
		],

		'cache' => function () {
			$config = [
				'class' => \yii\caching\FileCache::class,
			];

			return Sail::createObject($config);
		},

		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets'    => [
				[
					'class'  => yii\log\FileTarget::class,
					'levels' => ['error', 'warning'],
				],
			],
		],

		'sysSetting' => [
			'class'    => sail\services\SysSetting::class,
			'defaults' => [

			],
		],

		'view' => [
			'renderers' => [
				'tpl' => [
					'class' => yii\smarty\ViewRenderer::class
				]
			],
			'defaultExtension' => 'tpl',
		],

		'security' => [
			'class' => sail\base\Security::class
		]
	],
];