<?php

namespace sail\config;

use yii\base\BaseObject;

class General extends BaseObject
{
	// 图片处理方式
	const IMAGE_DRIVER_AUTO    = 'auto';
	const IMAGE_DRIVER_GD      = 'gd';
	const IMAGE_DRIVER_IMAGICK = 'imagick';

	/**
	 * @var string|null 系统时区
	 */
	public $timezone;

	/**
	 * @var array 允许上传的文件扩展名列表
	 */
	public $allowFileExtensions = [

	];

	/**
	 * @var int cost值越高生成hash密码及验证时间越长，但暴力破解的速度也越低。
	 */
	public $blowfishHashCost = 13;

	/**
	 * @var int 缓存有效期
	 */
	public $cacheDuration = 86400;

	/**
	 * @var bool 设置为true则任何上传的文件名都将删除多字节字符，并转换高位ASCII字符到对应的低位
	 */
	public $convertFilenamesToAscii = false;

	/**
	 * 登录失败次数过多用户账号被锁定后重新尝试登录之前必须等待的时间。
	 * 设置为0将无限期锁定，需要管理员手动解锁用户
	 *
	 * @var int 秒数
	 */
	public $cooldownDuration = 300;

	/**
	 * @var string 用于CSRF验证时的令牌名称
	 */
	public $csrfTokenName = 'SAIL_CSRF_TOKEN';

	/**
	 * @var string 自定义cookie的domain。设置为'.domain.com'则cookie适用于所有子域。空字符串则由浏览器自己确定
	 */
	public $defaultCookieDomain = '';

	/**
	 * @var int|null 为新生成的目录设置的默认权限。设为null则由当前环境确定
	 */
	public $defaultDirMode = 0775;

	/**
	 * @var int|null 为新生成的文件设置的默认权限。设为null则由当前环境确定
	 */
	public $defaultFileMode;

	/**
	 * @var int 保存JPG或PNG时所使用的质量级别。0-100范围
	 */
	public $defaultImageQuality = 85;

	/**
	 * @var string 如果Imagick已安装则优先使用，否则使用GD库
	 */
	public $imageDriver = self::IMAGE_DRIVER_AUTO;

	/**
	 * @var int 一周的第一天。0是星期天---6是星期六
	 */
	public $defaultWeekStartDay = 1;

	/**
	 * @var bool
	 */
	public $enableCsrfCookie = true;

	/** @var mixed */
	public $elevatedSessionDuration = 300;

	/**
	 * @var string 用于加密的密匙
	 */
	public $securityKey;

	/**
	 * @var int 最大允许失败登录次数
	 */
	public $maxInvalidLogin = 5;

	/**
	 * 用于跟踪用户无效登录时间范围
	 * @var int 秒数
	 */
	public $invalidLoginDuration = 3600;

	/**
	 * 系统将记住用户名，并在登陆界面填充~，设置为0则禁用
	 * @var int
	 */
	public $rememberUsernameDuration = 31536000;

	/**
	 * @var int 用户在登录页上选择“记住我”后，记住的时间。设为0则禁用，默认半个月
	 */
	public $rememberUserDuration = 1209600;

	/**
	 * @var int
	 */
	public $rememberSessionDuration = 3600;

	/**
	 * @var bool 防止用户枚举
	 */
	public $preventUserEnumeration = false;
}