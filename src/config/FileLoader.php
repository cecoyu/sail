<?php

namespace sail\config;

use sail\helpers\File;

class FileLoader implements LoaderInterface
{
	/**
	 * 默认配置路径
	 *
	 * @var string
	 */
	protected $defaultPath;

	/**
	 * 所有已命名的路径hints
	 *
	 * @var array
	 */
	protected $hints = [];

	/**
	 * 是否存在命名空间和组的缓存
	 *
	 * @var array
	 */
	protected $exists = [];

	/**
	 * 创建新的文件配置加载器
	 *
	 * @param string $defaultPath
	 */
	public function __construct($defaultPath)
	{
		$this->defaultPath = $defaultPath;
	}

	/**
	 * 加载指定的配置组
	 *
	 * @param string $env
	 * @param string $group
	 * @param string $namespace
	 * @return array
	 */
	public function load($env, $group, $namespace = null)
	{
		$items = [];

		$path = $this->getPath($namespace);

		if (is_null($path)) {
			return $items;
		}

		$file = "{$path}/{$group}.php";

		if (File::exists($file)) {
			$items = $this->getRequire($file);
		}

		$file = "{$path}/{$env}/{$group}.php";

		if (File::exists($file)) {
			$items = $this->mergeEnv($items, $file);
		}

		return $items;
	}

	/**
	 * 确定指定的配置组是否存在
	 *
	 * @param string $group
	 * @param string $namespace
	 * @return bool
	 */
	public function exists($group, $namespace = null)
	{
		$key = $group.$namespace;

		if (isset($this->exists[$key])) {
			return $this->exists[$key];
		}

		$path = $this->getPath($namespace);

		if (is_null($path)) {
			return $this->exists[$key] = false;
		}

		$file = "{$path}/{$group}.php";

		$exists = File::exists($file);

		return $this->exists[$key] = $exists;
	}

	/**
	 * 添加一个新的命名空间到加载器
	 *
	 * @param string $namespace
	 * @param string $hint
	 * @return void
	 */
	public function addNamespace($namespace, $hint)
	{
		$this->hints[$namespace] = $hint;
	}

	/**
	 * 返回所有已注册的命名空间
	 * @return array
	 */
	public function getNamespaces()
	{
		return $this->hints;
	}

	/**
	 * Apply any cascades to an array of package options.
	 *
	 * @param string $env
	 * @param string $package
	 * @param string $group
	 * @param array $items
	 * @return array
	 */
	public function cascadePackage($env, $package, $group, $items)
	{
		$path = $this->getPackagePath($package, $group);

		if (File::exists($path)) {
			$items = array_merge($items, $this->getRequire($path));
		}

		$path = $this->getPackagePath($package, $group, $env);

		if (File::exists($path)) {
			$items = array_merge($items, $this->getRequire($path));
		}

		return $items;
	}

	/**
	 * 从指定的命名空间获取配置路径
	 *
	 * @param string $namespace
	 * @return string
	 */
	protected function getPath($namespace)
	{
		if (is_null($namespace)) {
			return $this->defaultPath;
		} elseif (isset($this->hints[$namespace])) {
			return $this->hints[$namespace];
		}
	}

	/**
	 * 获取文件内容
	 *
	 * @param string $path
	 * @return mixed
	 */
	protected function getRequire($path)
	{
		return File::getRequire($path);
	}

	/**
	 * @param array $items
	 * @param string $file
	 * @return array
	 */
	protected function mergeEnv($items, $file)
	{
		return array_replace_recursive($items, $this->getRequire($file));
	}

	/**
	 * 获取环境和组的包路径
	 *
	 * @param string $package
	 * @param string $group
	 * @param string $env
	 * @return string
	 */
	protected function getPackagePath($package, $group, $env = null)
	{
		$package = strtolower(str_replace('.', '/', $package));

		if (!$env) {
			$file = "{$package}/{$group}.php";
		} else {
			$file = "{$package}/{$env}/{$group}.php";
		}

		return $this->defaultPath . '/' . $file;
	}
}