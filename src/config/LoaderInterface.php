<?php

namespace sail\config;

interface LoaderInterface
{
	/**
	 * 加载指定的配置组
	 *
	 * @param string $env
	 * @param string $group
	 * @param string $namespace
	 * @return array
	 */
	public function load($env, $group, $namespace = null);

	/**
	 * 确定指定的配置组是否存在
	 *
	 * @param string $group
	 * @param string $namespace
	 * @return bool
	 */
	public function exists($group, $namespace = null);

	/**
	 * 添加一个新的命名空间到加载器
	 *
	 * @param string $namespace
	 * @param string $hint
	 * @return void
	 */
	public function addNamespace($namespace, $hint);

	/**
	 * 返回所有已注册的命名空间
	 * @return array
	 */
	public function getNamespaces();

	/**
	 * Apply any cascades to an array of package options.
	 *
	 * @param string $env
	 * @param string $package
	 * @param string $group
	 * @param array $items
	 * @return array
	 */
	public function cascadePackage($env, $package, $group, $items);
}