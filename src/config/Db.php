<?php

namespace sail\config;

use yii\base\BaseObject;
use yii\base\InvalidConfigException;

use sail\helpers\Str;

class Db extends BaseObject
{
	const DRIVER_MYSQL = 'mysql';

	/**
	 * @var array 将要传递给PDO构造函数的属性.
	 */
	public $attributes = [];

	/**
	 * @var string 用来创建数据表的字符集
	 */
	public $charset = 'utf8';

	/**
	 * @var string 数据库名称
	 */
	public $database = '';

	/** @var string 要使用的数据库驱动 */
	public $driver = self::DRIVER_MYSQL;

	/**
	 * @var  string dsn连接字符串
	 * mysql: 'mysql:host=[server];port=[port];dbname=[database]...参考官方文档
	 * 设置后server, port, user, password, database, driver, unixSocket等单独设置将无效
	 */
	public $dsn;

	/**
	 * @var string 用来连接数据库服务器的密码
	 */
	public $password = '';

	/**
	 * @var string 数据库服务器所使用的端口
	 */
	public $port;

	/**
	 * @var string 数据库服务器名或IP地址，一般为localhost或127.0.0.1
	 */
	public $server = '127.0.0.1';

	/**
	 * @var string 数据库表前缀
	 */
	public $tablePrefix = '';

	/**
	 * @var string|null MYSQL Unix套接字
	 * 如果设置则CLI连接字符串（用于yiic）将连接到Unix套接字而不是服务器端口，并且server和port设置将被忽略
	 */
	public $unixScoket;

	/**
	 * @var string|null 数据库连接URL
	 */
	public $url;

	/**
	 * @var string 连接数据库服务器的用户名
	 */
	public $user = 'root';

	/** @inheritdoc */
	public function init()
	{
		// 解析url并赋值给各个属性（如果已设置）
		if (!is_null($this->url)) {
			$url = parse_url($this->url);
			if (isset($url['scheme'])) {
				$scheme = strtolower($url['scheme']);
				// 支持多个数据库时的判断
				if (in_array($scheme, [self::DRIVER_MYSQL], true)) {
					$this->driver = self::DRIVER_MYSQL;
				}
			}

			if (isset($url['user'])) {
				$this->user = $url['user'];
			}

			if (isset($url['pass'])) {
				$this->password = $url['pass'];
			}

			if (isset($url['host'])) {
				$this->server = $url['host'];
			}

			if (isset($url['port'])) {
				$this->port = $url['post'];
			}

			if (isset($url['path'])) {
				$this->database = trim($url['path'], '/');
			}
		}

		// 验证驱动
		if (!in_array($this->driver, [self::DRIVER_MYSQL], true)) {
			throw new InvalidConfigException('不支持的数据库驱动值:' . $this->driver);
		}

		// 验证表前缀
		if ($this->tablePrefix) {
			// 确保以'_'作为结束字符
			$this->tablePrefix = Str::ensureRight($this->tablePrefix, '_');
		}

		// 转换为小写
		$this->server = strtolower($this->server);
		if (!is_null($this->unixScoket)) {
			$this->unixScoket = strtolower($this->unixScoket);
		}

		// 根据数据库类型设置端口
		if (is_null($this->port) || $this->port === '') {
			switch ($this->driver) {
				case self::DRIVER_MYSQL:
					$this->port = 3306;
					break;
			}
		} else {
			$this->port = (int)$this->port;
		}

		if (is_null($this->dsn) || $this->dsn === '') {
			$this->updateDsn();
		}
	}

	public function updateDsn()
	{
		if ($this->driver === self::DRIVER_MYSQL && $this->unixScoket) {
			$this->dsn = "{$this->driver}:unix_scoket={$this->unixScoket};dbname={$this->database};";
		} else {
			$this->dsn = "{$this->driver}:host={$this->server};dbname={$this->database};port={$this->port};";
		}
	}
}