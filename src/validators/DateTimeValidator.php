<?php

namespace sail\validators;

use sail\helpers\DT;
use yii\validators\Validator;

class DateTimeValidator extends Validator
{
	public function validateAttribute($model, $attribute)
	{
		$value = $model->$attribute;

		if ($value && !$value instanceof \DateTime) {
			$model->$attribute = DT::toDateTime($value);
		}
	}
}