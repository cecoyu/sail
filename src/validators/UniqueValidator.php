<?php

namespace sail\validators;

use sail\helpers\Str;
use yii\base\Model;
use yii\db\ActiveRecord;

class UniqueValidator extends \yii\validators\UniqueValidator
{
	/**
	 * @var string|string[]
	 */
	public $pk;

	/**
	 * @var Model|null
	 */
	protected $originalModel;

	public function validateAttribute($model, $attribute)
	{
		if ($this->targetClass) {
			/** @var ActiveRecord $record */
			$record = new $this->targetClass();

			$pks = $record::primaryKey();
			if ($this->pk !== null) {
				$pkMap = is_string($this->pk) ? Str::split($this->pk) : $this->pk;
			} else {
				$pkMap = $pks;
			}
			$isNewRecord = true;

			foreach ($pkMap as $k => $v) {
				if (is_int($k)) {
					$sourcePk = $v;
					$targetPk = $pks[$k];
				} else {
					$sourcePk = $k;
					$targetPk = $v;
				}
				if ($model->$sourcePk) {
					$record->$targetPk = $model->$sourcePk;
					$isNewRecord = false;
				}
			}

			$record->setIsNewRecord($isNewRecord);

			$targetAttribute = $this->targetAttribute ?: $attribute;

			if (is_array($targetAttribute)) {
				foreach ($targetAttribute as $k => $v) {
					$record->$v = is_int($k) ? $model->$v : $model->$k;
				}
			} else {
				$record->$targetAttribute = $model->$attribute;
			}

			$this->originalModel = $model;
			parent::validateAttribute($record, $attribute);
			$this->originalModel = null;
		} else {
			parent::validateAttribute($model, $attribute);
		}
	}

	/** @inheritdoc */
	public function addError($model, $attribute, $message, $params = [])
	{
		if ($this->originalModel !== null) {
			$model = $this->originalModel;
		}

		parent::addError($model, $attribute, $message, $params);
	}
}