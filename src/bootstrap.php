<?php

// 注册全局助手函数
require_once  'helpers.php';

// 设置字符编码的检测顺序，"auto"会根据mbstring.language自动扩展
mb_detect_order('auto');

// 规范化PHP字符串函数的行为(例如strtoupper)
setlocale(
	LC_CTYPE,
	'c.UTF-8',
	'C.utf8',
	'en_US.UTF-8',
	'en_US.utf8'
);

// 验证应用类型
if (!isset($appType) || !in_array($appType, ['web', 'console'])) {
	throw new Exception('$appType 必须设置为`web`或`console`！');
}

// 目录路径
$vendorPath  = ROOT . DIRECTORY_SEPARATOR . 'vendor';
$systemPath  = ROOT . DIRECTORY_SEPARATOR . 'system';
$configPath  = ROOT . DIRECTORY_SEPARATOR . 'config';
$storagePath = ROOT . DIRECTORY_SEPARATOR . 'storage';

// debug
ini_set('log_errors', 1);
ini_set('error_log', ROOT . '/storage/logs/php_errors.log');
error_reporting(E_ALL);

$config = new \sail\services\Config;
var_dump($config->get('app.debug'));exit;

// 根据当前的模式决定是否显示错误异常
ini_set('display_errors', true ? 1 : 0);
defined('YII_DEBUG') || define('YII_DEBUG', true);
defined('YII_ENV') || define('YII_ENV', (true ? 'dev' : 'prod'));

// 加载核心文件
require $vendorPath . '/yiisoft/yii2/Yii.php';
require $systemPath . '/Sail.php';

spl_autoload_unregister(['Yii', 'autoload']);
spl_autoload_register(['Yii', 'autoload'], true, false);

// 设置别名路径
Sail::setAlias('@sail', $systemPath);
Sail::setAlias('@config', $configPath);
Sail::setAlias('@storage', $storagePath);

// 要实例化的应用类
$class = "sail\\{$appType}\\Application";

// 合并应用配置
$config = \yii\helpers\ArrayHelper::merge(
	[
		'vendorPath' => $vendorPath,
		'components' => [
			'config' => [
				'class'     => sail\services\Config::class,
				'configDir' => $configPath,
			],
		],
	],
	require "$systemPath/config/app/main.php",
	require "$systemPath/config/app/$appType.php"
);

/**
 * @var sail\web\Application|sail\console\Application
 */
$app = new $class($config);

return $app;