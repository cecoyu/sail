<?php

if (!function_exists('app')) {
	/**
	 * `Sail::$app`的快捷方式
	 *
	 * @param null|string $id      组件ID
	 * @param bool $throwException 如果组件未定义是否抛出异常
	 *
	 * @return null|object|\sail\console\Application|\sail\web\Application
	 * @throws \yii\base\InvalidConfigException
	 */
	function app($id = null, $throwException = true)
	{
		if (is_null($id)) {
			return Sail::$app;
		}

		return Sail::$app->get($id, $throwException);
	}
}

if (!function_exists('getAlias')) {
	/**
	 * `Sail::getAlias`的快捷方式
	 *
	 * @param $path
	 *
	 * @return bool|string
	 */
	function getAlias($path)
	{
		return Sail::getAlias($path);
	}
}

if (!function_exists('setAlias')) {
	/**
	 * `Sail::setAlias`的快捷方式
	 *
	 * @param string $name
	 * @param string $path
	 */
	function setAlias($name, $path)
	{
		Sail::setAlias($name, $path);
	}
}

if (!function_exists('plugins_path')) {
	/**
	 * 获取插件目录路径
	 *
	 * @param string $path
	 *
	 * @return string
	 */
	function plugins_path($path = '')
	{
		return getAlias('@plugin') . ($path ? '/' . $path : $path);
	}
}

if (!function_exists('e')) {
	/**
	 * 转义字符串中的HTML特殊字符
	 *
	 * @param string $value
	 * @param bool $doubleEncode
	 *
	 * @return string
	 */
	function e($value, $doubleEncode = false)
	{
		return htmlspecialchars($value, ENT_QUOTES, 'UTF-8', $doubleEncode);
	}
}

if (!function_exists('retry')) {
	/**
	 * 对任务重复执行一定次数
	 *
	 * @param int $times         要执行的次数
	 * @param callable $callback 回调函数
	 * @param int $sleep         每次执行的间隔时间
	 *
	 * @return mixed
	 * @throws Exception
	 */
	function retry($times, $callback, $sleep = 0)
	{
		$times--;

		beginning:
		try {
			return $callback();
		} catch (Exception $e) {
			if (!$times) {
				throw $e;
			}

			$times--;

			if ($sleep) {
				usleep($sleep * 1000);
			}

			goto beginning;
		}
	}
}

if (!function_exists('post')) {
	/**
	 * 支持逗号语法，获取post值
	 *
	 * @param null|string $name
	 * @param null|string $default
	 *
	 * @return mixed|array
	 * @throws \yii\base\InvalidConfigException
	 */
	function post($name = null, $default = null)
	{
		return app('request')->post($name, $default);
	}
}

if (!function_exists('get')) {
	/**
	 * @param null|string $name
	 * @param null|string $default
	 *
	 * @return null|object
	 * @throws \yii\base\InvalidConfigException
	 */
	function get($name = null, $default = null)
	{
		return app('request')->get($name, $default);
	}
}