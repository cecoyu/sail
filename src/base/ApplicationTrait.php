<?php

namespace sail\base;

use Sail;

trait ApplicationTrait
{
	/**
	 * 插件的基础路径
	 *
	 * @var string
	 */
	protected $pluginsPath;

	/**
	 * 主题的基础路径
	 *
	 * @var string
	 */
	protected $themesPath;

	/**
	 * 获取系统插件的基础路径
	 *
	 * @return string
	 */
	public function pluginsPath()
	{
		return $this->pluginsPath ?: $this->basePath . '/plugins';
	}

	/**
	 * 设置系统插件的基础路径
	 *
	 * @param $path string
	 */
	public function setPluginsPath($path)
	{
		$this->pluginsPath = $path;
		setAlias('@path/plugins', $path);
	}

	/**
	 * 获取系统主题的基础路径
	 *
	 * @return string
	 */
	public function themesPath()
	{
		return $this->themesPath ?: $this->basePath . '/themes';
	}

	/**
	 * 设置系统主题的基础路径
	 *
	 * @param $path string
	 */
	public function setThemesPath($path)
	{
		$this->themesPath = $path;
		setAlias('@path/themes', $path);
	}

	/**
	 * 获取系统配置实例
	 *
	 * @return \sail\services\Config
	 */
	public function getConfig()
	{
		return $this->get('config');
	}
}