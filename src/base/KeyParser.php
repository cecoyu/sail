<?php

namespace sail\base;

/**
 * Key Parser trait
 *
 * 将key解析为namespace, group & item
 * example: namespace::group.item
 *
 * @package sail\base
 */
trait KeyParser
{
	/**
	 * 缓存已解析项
	 *
	 * @var array
	 */
	protected $keyParserCache = [];

	/**
	 * @param string $key
	 * @param array $parsed
	 */
	public function setParsedKey($key, $parsed)
	{
		$this->keyParserCache[$key] = $parsed;
	}

	/**
	 * @param string $key
	 * @return array
	 */
	public function parseKey($key)
	{
		if (isset($this->keyParserCache[$key])) {
			return $this->keyParserCache[$key];
		}

		$segments = explode('.', $key);

		if (strpos($key, '::') === false) {
			$parsed = $this->keyParserParseBasicSegments($segments);
		} else {
			$parsed = $this->keyParserParseSegments($key);
		}

		return $this->keyParserCache[$key] = $parsed;
	}

	/**
	 * @param array $segments
	 * @return array
	 */
	protected function keyParserParseBasicSegments($segments)
	{
		$group = $segments[0];

		if (count($segments) === 1) {
			return [null, $group, null];
		}

		$item = implode('.', array_slice($segments, 1));

		return [null, $group, $item];
	}

	/**
	 * @param string $key
	 * @return array
	 */
	protected function keyParserParseSegments($key)
	{
		list($namespace, $item) = explode('::', $key);

		$itemSegments = explode('.', $item);

		$groupAndItem = array_slice($this->keyParserParseBasicSegments($itemSegments), 1);

		return array_merge([$namespace], $groupAndItem);
	}
}