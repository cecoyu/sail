<?php

namespace sail\base;

use Sail;

class Security extends \yii\base\Security
{
	/** @inheritdoc */
	public function validateData($data, $key = null, $rawHash = false)
	{
		if ($key === null) {
			$key = Sail::$app->getConfig()->getGeneral()->securityKey;
		}

		return parent::validateData($data, $key, $rawHash);
	}
}