<?php

namespace sail\base;

use yii\base\Component;

abstract class SavableComponent extends Component implements SavableComponentInterface
{
	/**
	 * @event
	 */
	const EVENT_BEFORE_SAVE = 'beforeSave';

	/**
	 * @event
	 */
	const EVENT_AFTER_SAVE = 'afterSave';

	/**
	 * @event
	 */
	const EVENT_BEFORE_DELETE = 'beforeDelete';

	/**
	 * @event
	 */
	const EVENT_AFTER_DELETE = 'afterDelete';

	/** @var int|string|null 组件ID */
	public $id;

	/** @var \DateTime|null 该组件创建时间 */
	public $dateCreated;

	/** @var \DateTime|null 该组件更新时间 */
	public $dateUpdated;

	/** @inheritdoc */
	public static function isSelectable()
	{
		return true;
	}

	/** @inheritdoc */
	public function getIsNew()
	{
		return (!$this->id || strpos($this->id, 'new') === 0);
	}

	/** @inheritdoc */
	public function getSettings()
	{
		$settings = [];

		foreach ($this->settingsAttributes() as $attribute) {
		    $settings[$attribute] = $this->$attribute;
		}

		return $settings;
	}

	/** @inheritdoc */
	public function settingsAttributes()
	{
		$class = new \ReflectionClass($this);
		$names = [];

		foreach ($class->getProperties(\ReflectionProperty::IS_PUBLIC) as $property) {
			if (!$property->isStatic() && !$property->getDeclaringClass()->isAbstract()) {
				$names[] = $property->getName();
			}
		}

		return $names;
	}

	/** @inheritdoc */
	public function beforeSave($isNew)
	{
		$event = new ModelEvent([
			'isNew' => $isNew
		]);
		$this->trigger(self::EVENT_BEFORE_SAVE, $event);

		return $event->isValid;
	}

	/** @inheritdoc */
	public function afterSave($isNew)
	{
		if ($this->hasEventHandlers(self::EVENT_AFTER_SAVE)) {
			$this->trigger(self::EVENT_AFTER_SAVE, new ModelEvent([
				'isNew' => $isNew
			]));
		}
	}

	/** @inheritdoc */
	public function beforeDelete()
	{
		$event = new ModelEvent();
		$this->trigger(self::EVENT_BEFORE_DELETE, $event);

		return $event->isValid;
	}

	/** @inheritdoc */
	public function afterDelete()
	{
		if ($this->hasEventHandlers(slef::EVENT_AFTER_DELETE)) {
			$this->trigger(self::EVENT_AFTER_DELETE);
		}
	}
}