<?php

namespace sail\base;

class ModelEvent extends \yii\base\ModelEvent
{
	/** @var bool 该模型是否为新增 */
	public $isNew = false;
}