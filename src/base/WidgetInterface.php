<?php

namespace sail\plugin;

interface WidgetInterface extends SavableComponentInterface
{
	/**
	 * @return string|null
	 */
	public static function iconPath();

	/**
	 * @return int|null
	 */
	public static function maxColspan();

	/**
	 * @return string
	 */
	public function getTitle();

	/**
	 * @return string|false
	 */
	public function getBodyHtml();
}