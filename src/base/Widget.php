<?php

namespace sail\plugin;

abstract class Widget extends \yii\base\Widget implements WidgetInterface
{
	/** @var int|null */
	public $colspan;
}