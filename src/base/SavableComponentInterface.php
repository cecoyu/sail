<?php

namespace sail\base;

Interface SavableComponentInterface
{
	/**
	 * @return bool
	 */
	public static function isSelectable();

	/**
	 * @return bool
	 */
	public function getIsNew();

	/**
	 * 验证组件
	 * @param string[]|null $attributeNames
	 * @param bool $clearErrors
	 * @return bool
	 */
	public function validate($attributeNames = null, $clearErrors = true);

	/**
	 * @return array
	 */
	public function settingsAttributes();

	/**
	 * @return array
	 */
	public function getSettings();

	/**
	 * @param bool $isNew
	 * @return bool
	 */
	public function beforeSave($isNew);

	/**
	 * @param bool $isNew
	 * @return bool
	 */
	public function afterSave($isNew);

	/**
	 * @return bool
	 */
	public function beforeDelete();

	/**
	 *
	 */
	public function afterDelete();
}