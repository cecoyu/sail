<?php

namespace sail\web;

use Sail;
use sail\helpers\Str;
use yii\web\BadRequestHttpException;

class Request extends \yii\web\Request
{
	/**
	 * @var bool|null
	 */
	private $_isMobileBrowser;

	/**
	 * @var bool|null
	 */
	private $_isMobileOrTabletBrowser;

	/**
	 * @var string|null
	 */
	private $_ipAddress;

	private $_encodedQueryParams = false;

	/**
	 * @var bool
	 */
	private $_encodedBodyParams = false;

	/**
	 * @var bool|null
	 */
	private $_isBackend = false;

	/**
	 * @var
	 */
	private $_fullPath;

	/**
	 * @var
	 */
	private $_path;

	/**
	 * @var
	 */
	private $_segments;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		if (Sail::getRootAlias('@webroot') === false) {
			Sail::setAlias('@webroot', dirname($this->getScriptFile()));
		}

		if (Sail::getRootAlias('@web') === false) {
			Sail::setAlias('@web', $this->getHostInfo() . $this->getBaseUrl());
		}

		$generalConfig = Sail::$app->getConfig()->getGeneral();

		$path = $this->getFullPath();

		// 路径片段
		$this->_segments = $this->_segments($path);

		$this->_isBackend = ($this->getSegment(1) === 'backend');

		if ($this->_isBackend) {
			//array_shift($this->_segments);
		}

		$this->_path = implode('/', $this->_segments);
	}

	/**
	 * 获取指定路径片段
	 *
	 * @param int $num
	 * @return string|null
	 */
	public function getSegment($num)
	{
		if ($num > 0 && isset($this->_segments[$num - 1])) {
			return $this->_segments[$num - 1];
		}

		if ($num < 0) {
			$totalSegs = count($this->_segments);

			if (isset($this->_segments[$totalSegs + $num])) {
				return $this->_segments[$totalSegs + $num];
			}
		}

		return null;
	}

	/**
	 * 检查当前是否是移动端进行访问
	 * @param bool $detectTablets
	 *
	 * @return bool
	 */
	public function isMobileBrowser($detectTablets = false)
	{
		if ($detectTablets) {
			$property = &$this->_isMobileOrTabletBrowser;
		} else {
			$property = &$this->_isMobileBrowser;
		}

		if ($property === null) {
			if ($this->getUserAgent() !== null) {
				$property = (
					preg_match(
						'/(android|bb\\d+|meego).+mobile|avantgo|bada\\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\\.(browser|link)|vodafone|wap|windows ce|xda|xiino'
						.($detectTablets ? '|android|ipad|playbook|silk' : '').'/i',
						$this->getUserAgent()
					) ||
					preg_match(
						'/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',
						mb_substr($this->getUserAgent(), 0, 4)
					)
				);
			} else {
				$property = false;
			}
		}

		return $property;
	}

	/**
	 * 获取用户IP地址
	 * @return string
	 */
	public function getUserIP()
	{
		if (is_null($this->_ipAddress)) {
			$ipMatch = false;

			if (!empty($_SERVER['HTTP_CLIENT_IP']) && $this->_validateIp($_SERVER['HTTP_CLIENT_IP'])) {
				$ipMatch = $_SERVER['HTTP_CLIENT_IP'];
			} else {
				// 检查该是否通过代理进行访问
				if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					// 检查是否存在多个IP
					$ipList = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

					foreach ($ipList as $ip) {
						if ($this->_validateIp($ip)) {
							$ipMatch = $ip;
						}
					}
				}
			}

			if (!$ipMatch) {
				if (!empty($_SERVER['HTTP_X_FORWARDED']) && $this->_validateIp($_SERVER['HTTP_X_FORWARDED'])) {
					$ipMatch = $_SERVER['HTTP_X_FORWARDED'];
				} else {
					if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && $this->_validateIp($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])) {
						$ipMatch = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
					} else {
						if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && $this->_validateIp($_SERVER['HTTP_FORWARDED_FOR'])) {
							$ipMatch = $_SERVER['HTTP_FORWARDED_FOR'];
						} else {
							if (!empty($_SERVER['HTTP_FORWARDED']) && $this->_validateIp($_SERVER['HTTP_FORWARDED'])) {
								$ipMatch = $_SERVER['HTTP_FORWARDED'];
							}
						}
					}
				}

				// 保证是正确的IP
				if (!$ipMatch) {
					$ipMatch = $_SERVER['REMOTE_ADDR'];
				}
			}

			$this->_ipAddress = $ipMatch;
		}

		return $this->_ipAddress;
	}

	/**
	 * 从当前请求的URL获取主机名
	 * @return string
	 */
	public function getHostName()
	{
		if (isset($_SERVER['HTTP_HOST'])) {
			return $_SERVER['HTTP_HOST'];
		}

		return $_SERVER['SERVER_NAME'];
	}

	/**
	 * 扩展支持返回转换成utf8格式参数值。以及支持.语法指定嵌套路径
	 *
	 * @param string $name
	 * @param null $defaultValue
	 *
	 * @return mixed
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getBodyParam($name, $defaultValue = null)
	{
		return $this->_getParam($name, $defaultValue, $this->getBodyParams());
	}

	/**
	 * @inheritdoc
	 */
	public function getBodyParams()
	{
		if ($this->_encodedBodyParams === false) {
			$this->setBodyParams($this->_utf8AllTheThings(parent::getBodyParams()));
			$this->_encodedBodyParams = true;
		}

		return parent::getBodyParams();
	}

	/**
	 * @param $name
	 *
	 * @return false|mixed|null|string
	 * @throws BadRequestHttpException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getValidatedBodyParam($name)
	{
		$value = $this->getBodyParam($name);

		if ($value === null) {
			return null;
		}

		$value = Sail::$app->security->validateData($value);

		if ($value === false) {
			throw new BadRequestHttpException('Request contained an invalid body param');
		}

		return $value;
	}

	/** @inheritdoc */
	public function getQueryParams()
	{
		if ($this->_encodedQueryParams === false) {
			$this->setQueryParams($this->_utf8AllTheThings(parent::getQueryParams()));
			$this->_encodedQueryParams = true;
		}

		return parent::getQueryParams();
	}

	/**
	 * 返回指定的GET参数值
	 * 如果GET参数不存在，则返回传递给该函数的第二个参数
	 *
	 * // get $_GET['foo'], 如果存在
	 * $foo = Sail::$app->request->getQueryParam('foo')
	 *
	 * // get $_GET['foo']['bar'], 如果存在
	 * $bar = Sail::$app->request->getQueryParam('foo.bar')
	 *
	 * @param string $name GET参数名称
	 * @param null $defaultValue 如果GET参数不存在则返回该默认值
	 * @return mixed GET参数值
	 */
	public function getQueryParam($name, $defaultValue = null)
	{
		return $this->_getParam($name, $defaultValue, $this->getQueryParams());
	}

	/**
	 * 从GET请求或request body返回指定的参数值
	 * 如果参数不存在则返回默认值
	 *
	 * @param string $name 参数名称
	 * @param mixed $defaultValue 参数的默认值
	 * @return mixed
	 */
	public function getParam($name, $defaultValue = null)
	{
		if (($value = $this->getQueryParam($name)) !== null) {
			return $value;
		}

		if (($value = $this->getBodyParam($name)) !== null) {
			return $value;
		}

		return $defaultValue;
	}

	/**
	 * 返回请求是否接受给定的内容类型
	 *
	 * @param string $contentType
	 *
	 * @return bool
	 */
	public function accepts($contentType)
	{
		return array_key_exists($contentType, $this->getAcceptableContentTypes());
	}

	/**
	 * 返回请求是否接受JSON数据进行响应
	 *
	 * @return bool
	 */
	public function getAcceptsJson()
	{
		return $this->accepts('application/json');
	}

	public function getFullPath()
	{
		if ($this->_fullPath === null) {
			$pathInfo = $this->getPathInfo(true);
			$this->_fullPath = $this->_normalizePath($pathInfo);
		}

		return $this->_fullPath;
	}

	public function getPathInfo($returnRealPathInfo = false)
	{
		if ($returnRealPathInfo) {
			return parent::getPathInfo();
		}

		return $this->_path;
	}

	/**
	 * 当前是否正在访问后台
	 *
	 * @return bool|null
	 */
	public function getIsBackend()
	{
		return $this->_isBackend;
	}

	/**
	 * @param string $ip
	 *
	 * @return bool
	 */
	private function _validateIp($ip)
	{
		return !(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) === false &&
			filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) === false);
	}

	/**
	 * @param array $things
	 * @return array
	 */
	private function _utf8AllTheThings($things)
	{
		foreach ($things as $key => $value) {
		    $things[$key] = $this->_utf8Value($value);
		}

		return $things;
	}

	/**
	 * @param array|string $value
	 * @return array|string
	 */
	private function _utf8Value($value)
	{
		if (is_array($value)) {
			return $this->_utf8AllTheThings($value);
		}

		return Str::convertToUtf8($value);
	}

	/**
	 * 返回制定的参数值，支持以【.】指定数组的嵌套路径
	 *
	 * @param string|null $name
	 * @param mixed $defaultValue
	 * @param array $params
	 *
	 * @return mixed
	 */
	private function _getParam($name = null, $defaultValue, $params)
	{
		// 是否获取整个数组
		if ($name === null) {
			return $this->_utf8AllTheThings($params);
		}

		// 还是获取具体的某个值
		if (isset($params[$name])) {
			return $this->_utf8Value($params[$name]);
		}

		// 对嵌套的参数进行查找
		if (Str::contains($name, '.')) {
			$path = explode('.', $name);
			$param = $params;

			foreach ($path as $step) {
				if (is_array($param) && isset($param[$step])) {
					$param = $param[$step];
				} else {
					return $defaultValue;
				}
			}

			return $this->_utf8Value($param);
		}

		return $defaultValue;
	}

	private function _normalizePath($path)
	{
		return preg_replace('/\/\+/', '/', trim($path, '/'));
	}

	private function _segments($path)
	{
		return array_values(array_filter(explode('/', $path), function ($segment) {
			return $segment !== '';
		}));
	}
}