<?php

namespace sail\web;

use yii\base\Component;

class CombineAssets extends Component
{
	/**
	 * @var array
	 */
	protected static $jsExtensions = ['js'];

	/**
	 * @var array
	 */
	protected static $cssExtensions = ['css', 'less', 'scss', 'sass'];

	/**
	 * @var array 要应用到每个文件的过滤器
	 */
	protected $filters = [];

	/**
	 * @var string
	 */
	protected $storagePath;

	/**
	 * @var bool
	 */
	public $useCache = false;

	/**
	 * @var bool
	 */
	public $useMinify = false;
}