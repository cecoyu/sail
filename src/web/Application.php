<?php

namespace sail\web;

use Sail;
use sail\db\Connection;
use sail\db\DbConnectException;
use sail\i18n\I18N;
use sail\i18n\Locale;
use sail\i18n\Formatter;
use sail\base\Security;
use sail\base\ApplicationTrait;
use sail\web\Application as WebApplication;
use sail\console\Application as ConsoleApplication;
use yii\web\Response;
use yii\mutex\Mutex;

/**
 * @property Request $request
 * @property-read Connection $db
 * @property-read \yii\mutex\Mutex $mutex
 * @property-read \sail\i18n\Locale $locale
 * @property-read \sail\plugin\PluginManager $plugins
 * @property-read \sail\services\Config $config
 * @property-read \sail\services\Path $path
 * @property-read \sail\auth\DbManager $authManager
 * @property-read Security $security
 *
 * @method Request getRequest()
 * @method Connection getDb()
 * @method I18N getI18n()
 * @method Formatter getFormatter()
 * @method Security getSecurity()
 * @method User getUser()
 */
class Application extends \yii\web\Application
{
	use ApplicationTrait;

	/**
	 * 系统是否已安装
	 * @var bool|null
	 */
	private $_isInstalled;

	/**
	 * @var bool|null
	 */
	private $_isDbConnectionValid;

	/**
	 * 该事件在应用初始化后触发
	 * @event \yii\base\Event
	 */
	const EVENT_INIT = 'init';

	/** @inheritdoc */
	public function __construct($config = [])
	{
		Sail::$app = $this;
		parent::__construct($config);
	}

	/** @inheritdoc */
	public function init()
	{
		parent::init();

		$this->_init();
	}

	/**
	 * @inheritdoc
	 *
	 * @param string $route
	 * @param array $params
	 *
	 * @return mixed|null|\yii\web\Response
	 */
	public function runAction($route, $params = [])
	{
		$result = parent::runAction($route, $params);

		if (!is_null($result)) {
			if ($result instanceof Response) {
				return $result;
			}

			$response       = $this->response;
			$response->data = $result;

			return $response;
		}

		return null;
	}

	/**
	 * 检查系统是否已安装，检查installed.lock是否存在或数据库状态
	 *
	 * @param bool $checkDb
	 *
	 * @return bool
	 *
	 * @throws \yii\base\NotSupportedException
	 * @throws \yii\db\Exception
	 */
	public function getIsInstalled($checkDb = false)
	{
		if ($this->_isInstalled !== null) {
			return $this->_isInstalled;
		}

		if ($checkDb === false) {
			return file_exists(Sail::getAlias('@storage') . '/installed.lock');
		} else {
			try {
				// 初始化数据库连接
				$this->getDb();

				// 如果数据库配置无效则认为未正常安装
				if (!$this->getIsDbConnectionValid()) {
					return false;
				}
			} catch (DbConnectException $e) {
				return false;
			}

			return $this->_isInstalled = $this->getDb()
				->tableExists('{{%info}}', false);
		}

	}

	/**
	 * 检查数据库连接是否有效
	 * @return bool|null
	 * @throws \yii\db\Exception
	 */
	public function getIsDbConnectionValid()
	{
		if ($this->_isDbConnectionValid !== null) {
			return $this->_isDbConnectionValid;
		}

		try {
			$this->getDb()->open();

			return $this->_isDbConnectionValid = true;
		} catch (DbConnectException $e) {
			return $this->_isDbConnectionValid = false;
		}
	}

	/**
	 * @return Mutex
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getMutex()
	{
		return $this->get('mutex');
	}

	/**
	 * @return \sail\services\Path
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getPath()
	{
		return $this->get('path');
	}

	/**
	 * 初始化应用组件等操作
	 */
	private function _init()
	{
		$this->getLog();

		// 设置时区
		$this->_setTimeZone();

		// 加载插件
		$this->plugins->loadPlugins();

		// 触发init事件
		if ($this->hasEventHandlers(self::EVENT_INIT)) {
			$this->trigger(self::EVENT_INIT);
		}
	}

	/**
	 * 设置系统时区
	 */
	private function _setTimeZone()
	{
		$timezone = $this->config->getGeneral()->timezone;

		if ($timezone) {
			$this->setTimeZone($timezone);
		}
	}
}