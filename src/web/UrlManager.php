<?php

namespace sail\web;

use Sail;
use sail\web\Request;
use sail\helpers\Arr;
use yii\web\UrlRule;

class UrlManager extends \yii\web\UrlManager
{
	/**
	 * @event
	 */
	const EVENT_REGISTER_CP_URL_RULES = 'registerCpUrlRules';

	/**
	 * @event
	 */
	const EVENT_REGISTER_URL_RULES = 'registerUrlRules';

	/**
	 * @var array
	 */
	private $_routeParams = [];

	/** @inheritdoc */
	public function __construct($config = [])
	{
		$config['showScriptName'] = false;

		// 加载规则
		$config['rules'] = $this->_getRules();

		parent::__construct($config);
	}

	/**
	 * @inheritdoc
	 */
	public function parseRequest($request)
	{
		if (($route = $this->_getRequestRoute($request)) !== false) {
			if (!empty($this->_routeParams)) {
				if (isset($route[1])) {
					$route[1] = Arr::merge($route[1], $this->_routeParams);
				} else {
					$route[1] = $this->_routeParams;
				}
			} else {
				$this->_routeParams = $route[1];
			}

			return $route;
		}

		return false;
	}

	/** @inheritdoc */
	public function createUrl($params)
	{
		return $this->createAbsoluteUrl($params);
	}

	/** @inheritdoc */
	public function createAbsoluteUrl($params, $scheme = null)
	{
		$params = (array)$params;
		unset($params[$this->routeParam]);

		$route = trim($params[0], '/');
		unset($params[0]);

		$path = Sail::$app->getConfig()->getGeneral()->actionTrigger . '/' . $route;
	}

	public function setRouteParams($params)
	{
		$this->_routeParams = Arr::merge($this->_routeParams, $params);
	}

	private function _getRules()
	{
		$request = Sail::$app->getRequest();

		$routesService = Sail::$app->getRoutes();
	}

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 * @throws \yii\base\InvalidConfigException
	 */
	private function _getRequestRoute($request)
	{
		if (($route = $this->_getTokenRoute($request)) !== false) {
			return $route;
		}

		$path = $request->getPathInfo();


	}

	/**
	 * @param Request $request
	 */
	private function _getTokenRoute($request)
	{
		$token = $request->getQueryParam('token');

		if (YII_DEBUG) {
			Sail::debug([
				'rule' => 'Token' . ($token !== null ? ': ' . $token : ''),
				'match' => $token !== null,
				'parent' => null
			], __METHOD__);
		}

		if ($token === null) {
			return false;
		}

		return Sail::$app->getTokens()->getTokenRoute($token);
	}

	private function _getMathedUrlRoute($request)
	{
		foreach ($this->rules as $rule) {
		    $route = $rule->parseRequest($this, $request);

		    if (YII_DEBUG) {
		    	Sail::debug([
		    		'rule' => 'URL Rule: ' . (method_exists($rule, '__toString') ? $rule->__toString() : get_class($rule)),
					'match' => $route !== false,
					'parent' => null
				], __METHOD__);
		    }

		    if ($route !== false) {
		    	if ($rule instanceof UrlRule && $rule->params) {
		    		$this->setRouteParams($rule->params);
		    	}

		    	return $route;
		    }
		}

		return false;
	}
}