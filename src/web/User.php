<?php

namespace Sail\web;

use Sail;
use sail\helpers\DT;
use sail\helpers\Db;
use sail\User\models\User as UserModel;
use sail\User\validators\UserPasswordValidator;
use yii\web\Cookie;

/**
 * Class User
 * @method UserModel|null getIdentity($autoRenew = true)
 */
class User extends \yii\web\User
{
	/**
	 * @var array
	 */
	public $usernameCookie;

	/**
	 * @var string
	 */
	public $elevatedSessionTimeoutParam = '__elevated_timeout';

	/**
	 * 设置用户名cookie, 用户登录后使用该方法.
	 * 将登陆用户的用户名保存在Cookie中，以便登录表单记住初始用户名
	 *
	 * @param $user
	 *
	 * @throws \yii\base\InvalidConfigException
	 */
	public function setUsernameCookie($user)
	{
		// todo 更改为系统设置
		$generalConfig = Sail::$app->getConfig()->getGeneral();

		if ($generalConfig->rememberUsernameDuration !== 0) {
			$cookie = new Cookie($this->usernameCookie);
			$cookie->value = $user->username;
			$cookie->expire = time() + $generalConfig->rememberUsernameDuration;
			Sail::$app->getResponse()->getCookies()->add($cookie);
		} else {
			Sail::$app->getResponse()->getCookies()->remove(new Cookie($this->usernameCookie));
		}
	}

	/**
	 * 获取存储在cookie的用户名
	 * @return string
	 */
	public function getRememberedUsername()
	{
		return Sail::$app->getRequest()->getCookies()->getValue($this->usernameCookie['name']);
	}

	/**
	 * 返回当前用户会话剩余的秒数，如果会话已结束则返回-1
	 *
	 * @return int
	 */
	public function getRemainingSessionTime()
	{
		if (!$this->getIsGuest()) {
			if ($this->authTimeout === null) {
				return -1;
			}

			$expire = Sail::$app->getSession()->get($this->authTimeoutParam);
			$time = time();

			if ($expire !== null && $expire > $time) {
				return $expire - $time;
			}
		}

		return 0;
	}

	/**
	 * 返回当前用户是否是管理员
	 * @return bool
	 * @throws \Throwable
	 */
	public function getIsAdmin()
	{
		$user = $this->getIdentity();

		return ($user && $user->admin);
	}

	/**
	 * 返回当前提升的用户会话剩余多少秒
	 *
	 * @return bool|int
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getElevatedSessionTimeout()
	{
		if (!$this->getIsGuest()) {
			$session = Sail::$app->getSession();
			$expires = $session->get($this->elevatedSessionTimeoutParam);

			if ($expires !== null) {
				$currentTime = time();

				if ($expires > $currentTime) {
					return $expires - $currentTime;
				}
			}
		}

		// 如果未启用则返回false
		if (Sail::$app->getConfig()->getGeneral()->elevatedSessionDuration === 0) {
			return false;
		}

		return 0;
	}

	/**
	 * 返回当前用户是否有提升的会话
	 *
	 * @return bool
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getHasElevatedSession()
	{
		// 已禁用
		if (Sail::$app->getConfig()->getGeneral()->elevatedSessionDuration === 0) {
			return true;
		}

		return ($this->getElevatedSessionTimeout() !== 0);
	}

	/**
	 * 为当前用户提升会话
	 *
	 * @param $password
	 *
	 * @return bool
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 */
	public function startElevatedSession($password)
	{
		// 当前用户的身份实例
		$user = $this->getIdentity();

		if (!$user) {
			return false;
		}

		// 验证密码
		$validator = new UserPasswordValidator();

		if (!$validator->validate($password) || !Sail::$app->getSecurity()->validatePassword($password, $user->password)) {
			return false;
		}

		$generalConfig = Sail::$app->getConfig()->getGeneral();
		if ($generalConfig->elevatedSessionDuration === 0) {
			return true;
		}

		$session = Sail::$app->getSession();
		$timeout = time() + $generalConfig->elevatedSessionDuration;
		$session->set($this->elevatedSessionTimeoutParam, $timeout);

		return true;
	}

	/**
	 * 登陆前执行该方法
	 * @inheritdoc
	 */
	protected function beforeLogin($identity, $cookieBased, $duration)
	{
		if ($this->_validateUserAgentAndIp()) {
			return parent::beforeLogin($identity, $cookieBased, $duration);
		}

		return false;
	}

	/**
	 * 登陆后执行该方法
	 * @inheritdoc
	 * @throws \yii\base\InvalidConfigException
	 */
	protected function afterLogin($identity, $cookieBased, $duration)
	{
		// 保存用户名到cookie
		$this->setUsernameCookie($identity);

		// 删除过时的session
		$this->_deleteStaleSessions();

		$session = Sail::$app->getSession();
		$session->remove($this->elevatedSessionTimeoutParam);

		parent::afterLogin($identity, $cookieBased, $duration);
	}

	/** @inheritdoc */
	protected function renewIdentityCookie()
	{
		$this->_updateSessionRow();

		parent::renewIdentityCookie();
	}

	/**
	 * 退出登录后执行该方法
	 * @inheritdoc
	 */
	protected function afterLogout($identity)
	{
		$value = Sail::$app->getRequest()->getCookies()->getValue($this->identityCookie['name']);
	}

	public function removeReturnUrl()
	{
		Sail::$app->getSession()->remove($this->returnUrlParam);
	}
	
	// Private Methods

	protected function _validateUserAgentAndIp()
	{
		$request = Sail::$app->getRequest();

		if ($request->getUserAgent() === null || $request->getUserIP() === null) {
			Sail::warning('Request didn’t meet the user agent and IP requirement for maintaining a user session.',  __METHOD__);

			return false;
		}

		return true;
	}

	private function _updateSessionRow()
	{
		$cookieValue = Sail::$app->getRequest()->getCookies()->getValue($this->identityCookie['name']);

		if ($cookieValue !== null) {
			$data = json_decode($cookieValue, true);

			if (is_array($data) && isset($data[2])) {

			}
		}
	}

	/**
	 * @throws \Exception
	 */
	private function _deleteStaleSessions()
	{
		// PHP将时间段成为“间隔”，但ISO 8601标准称为持续时间(Durations)。
		// 在ISO 8601中“间隔”是另一回事
		// 这里是3个月的“间隔”或“持续时间”...
		$interval = new \DateInterval('P3M');
		$expire = DT::currentUTCDateTime();
		$pastTime = $expire->sub($interval);

		Sail::$app->getDb()->createCommand()
			->delete('{{%session}}', ['<', 'updated_at', Db::prepareDateForDb($pastTime)])
			->execute();
	}
}