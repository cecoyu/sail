<?php

namespace sail\web;

use Sail;

class UploadedFile extends \yii\web\UploadedFile
{
	/**
	 * 根据给定的名称返回已上传的文件
	 * 名称可以是普通字符串或者数组元素的字符串('Post[imageFile]' 或 'Post[0][imageFile]')
	 *
	 * @param string $name the name of the file input field.
	 * @param bool $ensureTempFileExists
	 * @return static|null
	 */
	public static function getInstanceByName($name, $ensureTempFileExists = true)
	{
		/** @var static $instance */
		$instance = parent::getInstanceByName(self::_normalizeName($name));
		if ($instance === null) {
			return null;
		}

		if ($ensureTempFileExists && !is_uploaded_file($instance->tempName)) {
			return null;
		}

		return $instance;
	}

	/**
	 * @param string $name the name of the array of files
	 * @param bool $lookForSingleInstance
	 * @param bool $ensureTempFilesExists
	 * @return static[]
	 */
	public static function getInstancesByName($name, $lookForSingleInstance = true, $ensureTempFilesExists = true)
	{
		$name = self::_normalizeName($name);

		/** @var static[] $instances */
		$instances = parent::getInstancesByName($name);

		if (empty($instances) && $lookForSingleInstance) {
			$singleInstance = static::getInstanceByName($name);

			if ($singleInstance) {
				$instances[] = $singleInstance;
			}
		}

		if ($ensureTempFilesExists) {
			array_filter($instances, function ($instance) {
				return is_uploaded_file($instance->tempName);
			});

			$instances = array_values($instances);
		}

		return $instances;
	}

	/**
	 * 保存文件到临时目录
	 * @param bool $deleteTempFile
	 * @return bool
	 */
	public function saveAsTempFile($deleteTempFile = true)
	{
		if ($this->error !== UPLOAD_ERR_OK) {
			return false;
		}

		// 生成随机文件名
		$tempFilename = uniqid(pathinfo($this->name, PATHINFO_FILENAME), true) . '.' . pathinfo($this->name, PATHINFO_EXTENSION);
		$tempPath = Sail::$app->getPath()->getTempPath() . DIRECTORY_SEPARATOR . $tempFilename;

		if (!$this->saveAs($tempPath, $deleteTempFile)) {
			return false;
		}

		return $tempPath;
	}

	/**
	 * 转换点语法为标准格式
	 * @param string $name
	 *
	 * @return string
	 */
	private static function _normalizeName($name)
	{
		if (($pos = strpos($name, '.')) !== false) {
			// 将点表示法转换为普通格式。ex:fields.assetsField => fields[assetsField]
			$name = substr($name, 0, $pos) . '[' . str_replace('.', '][', substr($name, $pos + 1)) . ']';
		}

		return $name;
	}
}