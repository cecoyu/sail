<?php

namespace sail\web;

use Sail;
use sail\behaviors\JumpBehavior;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;

/**
 * @method success($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
 * @method error($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
 */
class Controller extends \yii\web\Controller
{
	/**
	 * @return array 要排除的动作列表ID
	 */
	public function allowActions()
	{
		return [];
	}

	/** @inheritdoc */
	public function behaviors()
	{
		return [
			JumpBehavior::class
		];
	}

	public function redirectToPostedUrl($object = null, $default = null)
	{
		$url = Sail::$app->request->getValidatedBodyParam('redirect');

		if ($url === null) {
			if ($default !== null) {
				$url = $default;
			} else {
				$url = Sail::$app->getRequest()->getPathInfo();
			}
		}
	}
}