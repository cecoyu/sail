<?php

use yii\helpers\VarDumper;

class Sail extends Yii
{
	/**
	 * @var \sail\web\Application|\sail\console\Application
	 */
	public static $app;

	public static function dump($var, $depth = 10, $highlight = true) {
		VarDumper::dump($var, $depth, $highlight);
	}
}