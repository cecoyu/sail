<?php

namespace sail\helpers;

use Stringy\Stringy;

class Str extends \yii\helpers\StringHelper
{
	/**
	 * 返回一个camelCase(驼峰式)版本的字符串
	 *
	 * @param string $str 要转换的字符串
	 *
	 * @return string 转换后的字符串
	 */
	public static function camelCase($str)
	{
		return (string)Stringy::create($str)->camelize();
	}

	/**
	 * 转换字符串为数组
	 *
	 * @param string $str 要转换的字符串
	 *
	 * @return string[] 返回由字符串中的字符组成的数组
	 */
	public static function chars($str)
	{
		return Stringy::create($str)->chars();
	}

	/**
	 * 修剪字符串，使用单个空格替换多个连续的空格。其中包括Tabs、换行符以及多字节空格
	 *
	 * @param string $str 要处理的字符串
	 *
	 * @return string 处理后的字符串
	 */
	public static function collapseWhitespace($str)
	{
		return (string)Stringy::create($str)->collapseWhitespace();
	}

	/**
	 * 如果字符串包含$needle则返回true，否则返回false。
	 * 默认比较区分大小写，通过设置$caseSensitive为false使其大小写不敏感。
	 *
	 * @param string $haystack    正在检查的字符串
	 * @param string $needle      要查找的子字符串
	 * @param bool $caseSensitive 是否强制区分大小写
	 *
	 * @return bool $haystack是否包含$needle
	 */
	public static function contains($haystack, $needle, $caseSensitive = true)
	{
		return Stringy::create($haystack)->contains($needle, $caseSensitive);
	}

	/**
	 * 如果字符串包含任何的$needles数组项则返回true，否则返回false。
	 * 默认比较区分大小写，通过设置$caseSensitive为false使其大小写不敏感。
	 *
	 * @param string $haystack    正在检查的字符串
	 * @param array $needles      要查找的子字符串
	 * @param bool $caseSensitive 是否强制区分大小写
	 *
	 * @return bool $haystack是否包含$needles中的任何一个
	 */
	public static function containsAny($haystack, $needles, $caseSensitive = true)
	{
		return Stringy::create($haystack)->containsAny($needles, $caseSensitive);
	}

	/**
	 * 如果字符串包含所有的$needles数组项则返回true，否则返回false。
	 * 默认比较区分大小写，通过设置$caseSensitive为false使其大小写不敏感。
	 *
	 * @param string $haystack    正在检查的字符串
	 * @param array $needles      要查找的子字符串
	 * @param bool $caseSensitive 是否强制区分大小写
	 *
	 * @return bool $haystack是否包含所有$needles
	 */
	public static function containsAll($haystack, $needles, $caseSensitive = true)
	{
		return Stringy::create($haystack)->containsAll($needles, $caseSensitive);
	}

	/**
	 * 返回给定字符串中$substring出现的次数。
	 * 默认比较区分大小写，通过设置$caseSensitive为fanse使其大小写不敏感。
	 *
	 * @param string $str         要搜索的字符串
	 * @param string $substring   要搜索的子字符串
	 * @param bool $caseSensitive 是否强制区分大小写
	 *
	 * @return int $substring出现的次数
	 */
	public static function countSubstrings($str, $substring, $caseSensitive = true)
	{
		return Stringy::create($str)->countSubstr($substring, $caseSensitive);
	}

	/**
	 * 返回由给定的分隔符分隔的小写和修剪的字符。
	 * 分隔符插入大写字符之前(字符串的第一个字符除外),代替空格,破折号和下划线.Alpha分隔符不会转换为小写.
	 *
	 * @param string $str       要分隔的字符串
	 * @param string $delimiter 分隔符
	 *
	 * @return string 分隔后的字符串
	 */
	public static function delimit($str, $delimiter)
	{
		return (string)Stringy::create($str)->delimit($delimiter);
	}

	/**
	 * 如果字符串以$substring结束则返回true，否则返回false。
	 * 默认比较区分大小写，通过设置$caseSensitive为fanse使其大小写不敏感。
	 *
	 * @param string $str
	 * @param string $substring
	 * @param bool $caseSensitive
	 *
	 * @return bool
	 */
	public static function endsWith($str, $substring, $caseSensitive = true)
	{
		return Stringy::create($str)->endsWith($substring, $caseSensitive);
	}

	/**
	 * 确保字符串以$substring开头。如果没有则添加
	 *
	 * @param string $str       要修改的字符串
	 * @param string $substring 要添加的子字符串，如果不存在。
	 *
	 * @return string 以$substring为前缀的字符串
	 */
	public static function ensureLeft($str, $substring)
	{
		return (string)Stringy::create($str)->ensureLeft($substring);
	}

	/**
	 * 确保字符串以$substring结尾。如果没有则添加
	 *
	 * @param string $str       要修改的字符串
	 * @param string $substring 要添加的子字符串，如果不存在。
	 *
	 * @return string 以$substring为后缀的字符串
	 */
	public static function ensureRight($str, $substring)
	{
		return (string)Stringy::create($str)->ensureRight($substring);
	}

	/**
	 * 返回字符串第一个到$number个字符
	 *
	 * @param string $str 将从中获取子串的字符串
	 * @param int $number 要检索的字符数
	 *
	 * @return string
	 */
	public static function first($str, $number)
	{
		return (string)Stringy::create($str)->first($number);
	}

	/**
	 * 返回$i处的字符，索引从0开始
	 *
	 * @param string $str 要检查的字符串
	 * @param int $i      偏移位置
	 *
	 * @return string
	 */
	public static function at($str, $i)
	{
		return (string)Stringy::create($str)->at($i);
	}

	/**
	 * 如果字符串包含小写字符则返回true，否则返回false。
	 *
	 * @param string $str 要检查的字符串
	 *
	 * @return bool
	 */
	public static function hasLowerCase($str)
	{
		return Stringy::create($str)->hasLowerCase();
	}

	/**
	 * 如果字符串包含大写字符粗则返回true，否则返回false。
	 *
	 * @param string $str 要检查的字符串
	 *
	 * @return bool
	 */
	public static function hasUpperCase($str)
	{
		return Stringy::create($str)->hasUpperCase();
	}

	/**
	 * 返回字符串中首次出现$needle的索引，如果没有找到则返回false。接受开始搜索的可选偏移值。负数索引则从最后开始搜索
	 *
	 * @param string $str    要检查的字符串
	 * @param string $needle 要查找的子字符串
	 * @param int $offset    开始搜索的偏移值
	 *
	 * @return int|bool
	 */
	public static function indexOf($str, $needle, $offset = 0)
	{
		return Stringy::create($str)->indexOf($needle, $offset);
	}

	/**
	 * 返回字符串中最后出现$needle的索引，如果没有找到则返回false
	 *
	 * @param string $str    要检查的字符串
	 * @param string $needle 要查找的子字符串
	 * @param int $offset    开始搜索的偏移值
	 *
	 * @return int|bool
	 */
	public static function indexOfLast($str, $needle, $offset = 0)
	{
		return Stringy::create($str)->indexOfLast($needle, $offset);
	}

	/**
	 * 在$index处插入$substring字符串
	 *
	 * @param string $str       输入字符串
	 * @param string $substring 要插入的子字符串
	 * @param int $index        要插入的索引位置
	 *
	 * @return string
	 */
	public static function insert($str, $substring, $index)
	{
		return (string)Stringy::create($str)->insert($substring, $index);
	}

	/**
	 * @param string $str
	 *
	 * @return bool
	 */
	public static function isAlpha($str)
	{
		return Stringy::create($str)->isAlpha();
	}

	/**
	 * @param string $str
	 *
	 * @return bool
	 */
	public static function isAlphanumeric($str)
	{
		return Stringy::create($str)->isAlphanumeric();
	}

	/**
	 * @param string $str
	 *
	 * @return bool
	 */
	public static function isBlank($str)
	{
		return Stringy::create($str)->isBlank();
	}

	/**
	 * @param string $str
	 *
	 * @return bool
	 */
	public static function isHexadecimal($str)
	{
		return Stringy::create($str)->isHexadecimal();
	}

	/**
	 * 如果字符串只包含小写字符则返回true，否则返回false
	 *
	 * @param string $str
	 *
	 * @return bool
	 */
	public static function isLowerCase($str)
	{
		return Stringy::create($str)->isLowerCase();
	}

	/**
	 * 如果字符串只包含大写字符则返回true，否则返回false
	 *
	 * @param string $str
	 *
	 * @return bool
	 */
	public static function isUpperCase($str)
	{
		return Stringy::create($str)->isUpperCase();
	}

	/**
	 * 返回给定的字符串是否匹配 V4 UUID 模式
	 *
	 * @param string $str
	 *
	 * @return bool 字符串是否匹配V4 UUID模式
	 */
	public static function isUUID($str)
	{
		return !empty($str) && preg_match('/[A-Z0-9]{8}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}/ui', $str);
	}

	/**
	 * 生成一个 V4 UUID
	 * @return string
	 */
	public static function UUID()
	{
		return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

			// 32 bits for "time_low"
			random_int(0, 0xffff), random_int(0, 0xffff),

			// 16 bits for "time_mid"
			random_int(0, 0xffff),

			// 16 bits for "time_hi_and_version", four most significant bits holds version number 4
			random_int(0, 0x0fff) | 0x4000,

			// 16 bits, 8 bits for "clk_seq_hi_res", 8 bits for "clk_seq_low", two most significant bits holds zero and
			// one for variant DCE1.1
			random_int(0, 0x3fff) | 0x8000,

			// 48 bits for "node"
			random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff)
		);
	}


	/**
	 * 返回字符串的最后$number个字符
	 *
	 * @param string $str 从中获取子串的字符串
	 * @param int $number 个数
	 *
	 * @return string
	 */
	public static function last($str, $number)
	{
		return (string)Stringy::create($str)->last($number);
	}

	/**
	 * 返回字符串的长度，mb_strlen的别名函数
	 *
	 * @param string $str
	 *
	 * @return int
	 */
	public static function length($str)
	{
		return Stringy::create($str)->length();
	}

	/**
	 * 根据字符串中的换行符和回车符对字符串进行分割，返回分割后的数组。
	 *
	 * @param string $str 要分割的字符串
	 *
	 * @return string[]
	 */
	public static function lines($str)
	{
		$lines = Stringy::create($str)->lines();

		foreach ($lines as $i => $line) {
			$lines[$i] = (string)$line;
		}

		/** @var string[] $lines */
		return $lines;
	}

	/**
	 * 对特定字符串的首字符转换为小写
	 *
	 * @param string $str 要修改的字符串
	 *
	 * @return string
	 */
	public static function lowercaseFirst($str)
	{
		return (string)Stringy::create($str)->lowerCaseFirst();
	}

	/**
	 * 将字符串转换为kebab-cases(短横线命名)格式
	 *
	 * @param string $string
	 * @param string $glue
	 * @param bool $lower
	 * @param bool $removePunctuation
	 *
	 * @return string
	 */
	public static function toKebabCase($string, $glue = '-', $lower = true, $removePunctuation = true)
	{
		$words = self::_prepStringForCasing($string, $lower, $removePunctuation);

		return implode($glue, $words);
	}

	/**
	 * 将字符串转换为驼峰格式
	 *
	 * @param string $string
	 *
	 * @return string
	 */
	public static function toCamelCase($string)
	{
		$words = self::_prepStringForCasing($string);

		if (empty($words))
			return '';

		$string = array_shift($words) . implode('', array_map([
					get_called_class(),
					'upperCaseFirst',
				], $words
				)
			);

		return $string;
	}

	/**
	 * 将字符串转换为帕斯卡拼写法，主要的特点是将描述变量作用所有单词的首字母大写
	 *
	 * @param string $string
	 *
	 * @return string
	 */
	public static function toPascalCase($string)
	{
		$words  = self::_prepStringForCasing($string);
		$string = implode('', array_map([
				get_called_class(),
				'upperCaseFirst',
			], $words
			)
		);

		return $string;
	}

	/**
	 * @param string $string
	 *
	 * @return string
	 */
	public static function toSnakecase($string)
	{
		$words = self::_prepStringForCasing($string);

		return implode('_', $words);
	}

	/**
	 * 使用给定的字符分割字符串
	 *
	 * @param string $string
	 * @param string $delimiter
	 *
	 * @return string[]
	 */
	public static function split($string, $delimiter = ',')
	{
		return preg_split('/\s*' . preg_quote($delimiter, '/') . '\s*/', $string, -1, PREG_SPLIT_NO_EMPTY);
	}

	/**
	 * 将字符串拆分为单词数组
	 *
	 * @param string $string
	 *
	 * @return string[]
	 */
	public static function splitOnWords($string)
	{
		preg_match_all('/[\p{L}\p{N}\p{M}\._-]+/u', $string, $matches);

		return Arr::filterEmptyStringsFromArray($matches[0]);
	}

	/**
	 * 从给定的字符串中剥离HTML标签
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function stripHtml($str)
	{
		return preg_replace('/<(.*?)>/u', '', $str);
	}

	/**
	 * 用指定的字符填充到字符串的两端以达到指定的字符串长度
	 *
	 * @param string $str
	 * @param int $length
	 * @param string $padStr
	 *
	 * @return string
	 */
	public static function padBoth($str, $length, $padStr = ' ')
	{
		return (string)Stringy::create($str)->padBoth($length, $padStr);
	}

	/**
	 * 用指定的字符填充到字符串的开头以达到指定的字符串长度
	 *
	 * @param string $str
	 * @param int $length
	 * @param string $padStr
	 *
	 * @return string
	 */
	public static function padLeft($str, $length, $padStr = ' ')
	{
		return (string)Stringy::create($str)->padLeft($length, $padStr);
	}

	/**
	 * 用指定的字符填充到字符串尾部以达到指定的字符串长度
	 *
	 * @param $str
	 * @param $length
	 * @param string $padStr
	 *
	 * @return string
	 */
	public static function padRight($str, $length, $padStr = ' ')
	{
		return (string)Stringy::create($str)->padRight($length, $padStr);
	}

	/**
	 * 用$replacement替换$str中所有出现的$pattern.mb_ereg_replace的别名函数
	 *
	 * @param string $str
	 * @param string $pattern
	 * @param string $replacement
	 * @param string $options
	 *
	 * @return string
	 */
	public static function regexReplace($str, $pattern, $replacement, $options = 'msr')
	{
		return (string)Stringy::create($str)->regexReplace($pattern, $replacement, $options);
	}

	/**
	 * 返回移除$substring前缀的新字符串
	 *
	 * @param string $str
	 * @param string $substring
	 *
	 * @return string
	 */
	public static function removeLeft($str, $substring)
	{
		return (string)Stringy::create($str)->removeLeft($substring);
	}

	/**
	 * 返回移除$substring后缀的新字符串
	 *
	 * @param string $str
	 * @param string $substring
	 *
	 * @return string
	 */
	public static function removeRight($str, $substring)
	{
		return (string)Stringy::create($str)->removeRight($substring);
	}

	/**
	 * 用$replacement替换$str中所有出现的$search
	 *
	 * @param string $str
	 * @param string $search
	 * @param string $replacement
	 *
	 * @return string
	 */
	public static function replace($str, $search, $replacement)
	{
		return (string)Stringy::create($str)->replace($search, $replacement);
	}

	/**
	 * 返回反转的字符串。strrev的多字节版本
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function reverse($str)
	{
		return (string)Stringy::create($str)->reverse();
	}

	/**
	 * 将字符串截断为给定的长度，同时确保它不分割单词。
	 * 如果提供$substring并且发生截断，则字符串将被进一步截断，以便可以在不超过所需长度的情况下添加子字符串
	 *
	 * @param string $str
	 * @param int $length
	 * @param string $substring
	 *
	 * @return string
	 */
	public static function safeTruncate($str, $length, $substring = '')
	{
		return (string)Stringy::create($str)->safeTruncate($length, $substring);
	}

	/**
	 * 如果字符串以$substring开头则返回true，否则返回false。默认比较区分大小写
	 *
	 * @param string $str
	 * @param string $substring
	 * @param bool $caseSensitive
	 *
	 * @return bool
	 */
	public static function startsWith($str, $substring, $caseSensitive = true)
	{
		return Stringy::create($str)->startsWith($substring, $caseSensitive);
	}

	/**
	 * 返回从$start开始到指定$length长度的子字符串.
	 * 与mb_substr函数的不同之处在于，提供null的长度值将返回字符串的其余部分，而不是空字符串。
	 *
	 * @param string $str
	 * @param int $start
	 * @param int|null $length
	 *
	 * @return string
	 */
	public static function substr($str, $start, $length = null)
	{
		return (string)Stringy::create($str)->substr($start, $length);
	}

	/**
	 * 返回字符串的大小写交换后的版本
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function swapCase($str)
	{
		return (string)Stringy::create($str)->swapCase();
	}

	/**
	 * 返回每个单词首字母大写的字符串，接受一个$ignore数组，允许列出不需要大写的单词
	 *
	 * @param string $str
	 * @param array|null $ignore
	 *
	 * @return string
	 */
	public static function titleize($str, $ignore = null)
	{
		return (string)Stringy::create($str)->titleize($ignore);
	}

	/**
	 * 将字符串中的所有字符转换为小写。mb_strtolower的别名函数
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function toLowerCase($str)
	{
		return (string)Stringy::create($str)->toLowerCase();
	}

	/**
	 * 将对象转换为字符串的表现形式
	 *
	 * @param mixed $object
	 * @param string $glue
	 *
	 * @return string
	 */
	public static function toString($object, $glue = ',')
	{
		if (is_scalar($object) || (is_object($object) && method_exists($object, '__toString'))) {
			return (string)$object;
		}

		if (is_array($object) || $object instanceof \IteratorAggregate) {
			$stringValues = [];

			foreach ($object as $value) {
				if (($value = static::toString($value, $glue)) !== '') {
					$stringValues[] = $value;
				}
			}

			return implode($glue, $stringValues);
		}

		return '';
	}

	/**
	 * 将字符串中每个单词的第一个字符串转换为大写
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function toTitleCase($str)
	{
		return (string)Stringy::create($str)->toTitleCase();
	}

	/**
	 * 将字符串中的所有字符转换为大写。mb_strtoupper的别名函数
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function toUpperCase($str)
	{
		return (string)Stringy::create($str)->toUpperCase();
	}

	/**
	 * 返回修剪后的字符串
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function trim($str)
	{
		return (string)Stringy::create($str)->trim();
	}

	/**
	 * @param string $str
	 *
	 * @return string
	 */
	public static function trimLeft($str)
	{
		return (string)Stringy::create($str)->trimLeft();
	}

	/**
	 * @param string $str
	 *
	 * @return string
	 */
	public static function trimRight($str)
	{
		return (string)Stringy::create($str)->trimRight();
	}

	/**
	 * 对指定字符串的第一个字符转换为大写
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function upperCaseFirst($str)
	{
		return (string)Stringy::create($str)->upperCaseFirst();
	}

	/**
	 * 生成一个随机的字母数字字符串，默认长度36位。
	 * $extendedChars设置为true则可以在字符串包含其他符号。
	 * 生成的字符串不是安全的密码，需要生成密码可以使用Security::generateRandomString()
	 *
	 * @param int $length
	 * @param bool $extendedChars
	 *
	 * @return string
	 */
	public static function randomString($length = 36, $extendedChars = false)
	{
		if ($extendedChars) {
			$validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890`~!@#$%^&*()-_=+[]\{}|;:\'",./<>?"';
		} else {
			$validChars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		}

		return static::randomStringWithChars($validChars, $length);
	}

	/**
	 * 使用指定字符集生成一个随机的字符串。
	 *
	 * @param $validChars
	 * @param $length
	 *
	 * @return string
	 */
	public static function randomStringWithChars($validChars, $length)
	{
		$randomString = '';

		// 计算有效字符串中的字符数
		$numValidChars = static::length($validChars);

		// 重复该步骤直到创建一个正确长度的字符串
		for ($i = 0; $i < $length; $i++) {
			// 生成一个伪随机数
			$randomPick = random_int(1, $numValidChars);

			// 根据该伪随机数得到一个随机的字符
			$randomChar = $validChars[$randomPick - 1];

			// 合并...
			$randomString .= $randomChar;
		}

		return $randomString;
	}

	/**
	 * 返回字符串的ASCII版本。一组非ASCII字符被替换为最接近的ASCII对应字符，其余的被删除。
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function toAscii($str)
	{
		return (string)Stringy::create($str)->toAscii();
	}

	/**
	 * 将字符串以UTF8格式编码
	 *
	 * @param $string
	 *
	 * @return string
	 */
	public static function convertToUtf8($string)
	{
		if (static::isUtf8($string)) {
			return HtmlPurifier::cleanUtf8($string);
		}

		$config = \HTMLPurifier_Config::createDefault();
		$config->set('Core.Encoding', static::encoding($string));

		// 先清除
		$string = HtmlPurifier::cleanUtf8($string);

		// 转换成UTF8
		$string = HtmlPurifier::convertToUtf8($string, $config);

		return $string;
	}

	/**
	 * 检查字符串是否以UTF8编码
	 *
	 * @param $string
	 *
	 * @return bool
	 */
	public static function isUtf8($string)
	{
		return static::encoding($string) === 'utf-8';
	}

	/**
	 * 获取字符串的当前编码格式
	 *
	 * @param $string
	 *
	 * @return mixed
	 */
	public static function encoding($string)
	{
		return static::toLowerCase(mb_detect_encoding($string, mb_detect_order(), true));
	}

	/**
	 * HTML编码任何4字节的UTF-8字符
	 *
	 * @param string $string
	 *
	 * @return string
	 */
	public static function encodeMb4($string)
	{
		// 该字符串是否有任何4+字节的Unicode字符？
		if (max(array_map('ord', str_split($string))) >= 240) {
			$string = preg_replace_callback('/./u', function ($match) {
				if (strlen($match[0]) >= 4) {
					// 从WP的wp_encode_emoji()函数中抽取逻辑
					// UTF-32的十六进制编码与HTML的十六进制编码相同
					// 所以，通过从UTF-8转为UTF-32，可以神奇地获取正确的十六进制编码
					$unpacked = unpack('H*', mb_convert_encoding($match[0], 'UTF-32', 'UTF-8'));

					return isset($unpacked[1]) ? '&#x' . ltrim($unpacked[1], '0') . ';' : '';
				}

				return $match[0];
			}, $string
			);
		}

		return $string;
	}

	/**
	 * 检查给定的字符串是否是一个邮箱地址
	 *
	 * @param string $str
	 *
	 * @return bool
	 */
	public static function isEmail($str)
	{
		return strlen($str) > 6 && preg_match("/^[\w\-\.]+@[\w\-]+(\.\w+)+$/", $str);
	}

	/**
	 * 检查给定的字符串是否是一个手机号
	 *
	 * @param string $str
	 *
	 * @return int
	 */
	public static function isMobile($str)
	{
		return preg_match("/^1\\d{10}$/", $str);
	}

	// Private Methods
	// ---------------------------------------------

	/**
	 * @param string $string
	 * @param bool $lower
	 * @param bool $removePunctuation
	 *
	 * @return string[]
	 */
	private static function _prepStringForCasing($string, $lower = true, $removePunctuation = true)
	{
		$string = preg_replace('/(?<![A-Z])[A-Z]/u', ' \0', $string);

		if ($lower) {
			// 转为小写
			$string = static::toLowerCase($string);
		}

		if ($removePunctuation) {
			$string = str_replace(['.', '_', '-'], ' ', $string);
		}

		// 移除标点符号
		$string = preg_replace('/[\'"‘’“”\[\]\(\)\{\}:]/u', '', $string);

		// 拆分单词并返回
		return static::splitOnWords($string);
	}
}