<?php

namespace sail\helpers;

class Db
{
	/**
	 * 预先处理要保存到数据库的值
	 * @param mixed $value
	 * @return mixed
	 */
	public static function prepareValueForDb($value)
	{
		// 是一个serializable对象？
		if ($value instanceof \Serializable) {
			return $value->serialize();
		}

		// 标准化时间日期
		if ($value instanceof \DateTime || DT::isIso8601($value)) {
			return self::prepareDateForDb($value);
		}

		// 将对象转为JSON格式保存
		if (is_object($value) || is_array($value)) {
			return Json::encode($value);
		}

		return $value;
	}

	/**
	 * 预先处理要保存到数据库的日期时间值
	 * @param mixed $date
	 * @return null|string
	 */
	public static function prepareDateForDb($date)
	{
		$date = DT::toDateTime($date);

		if ($date !== false) {
			$timezone = $date->getTimezone();
			$date->setTimezone(new \DateTimeZone('UTC'));
			$formattedDate = $date->format('Y-m-d H:i:s');
			$date->setTimezone($timezone);

			return $formattedDate;
		}

		return null;
	}
}