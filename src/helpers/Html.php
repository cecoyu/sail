<?php

namespace sail\helpers;

use Sail;
use enshrined\svgSanitize\Sanitizer;

class Html extends \yii\helpers\Html
{
	public static function svg($svg, $sanitize = true)
	{
		// 如果字符串开头不是<svg>标签则可能是一个文件路径
		if (stripos($svg, '<svg') === false) {
			// 支持别名获取文件路径
			$svg = Sail::getAlias($svg);
			if (!is_file($svg) || !File::isSvg($svg)) {
				return '';
			}
			$svg = file_get_contents($svg);
		}

		if ($sanitize) {
			$svg = (new Sanitizer())->sanitize($svg);
		}

		// 清除XML声明
		$svg = preg_replace('/<\?xml.*?\?>/', '', $svg);

		if (strpos($svg, 'id=') !== false) {
			$namespace = Str::randomString(10) . '-';
			$ids = [];
			$svg = preg_replace_callback('/\bid=([\'"])([^\'"]+)\\1/i', function ($matches) use ($namespace, &$ids) {
				$ids[] = $matches[2];
				return "id={$matches[1]}{$namespace}{$matches[2]}{$matches[1]}";
			}, $svg);
			foreach ($ids as $id) {
				$quotedId = preg_quote($id, '\\');
				$svg = preg_replace("/#{$quotedId}\b(?!\-)/", "#{$namespace}{$id}", $svg);
			}
		}

		return $svg;
	}
}