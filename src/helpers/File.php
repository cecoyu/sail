<?php

namespace sail\helpers;

use \yii\helpers\FileHelper;

class File extends FileHelper
{
	/** @inheritdoc */
	public static $mimeMagicFile = '@sail/helpers/mimeTypes.php';

	/**
	 * 检查特定文件是否为svg图像
	 *
	 * @param $file
	 * @param null $magicFile
	 * @param bool $checkExtension
	 *
	 * @return bool
	 * @throws \yii\base\InvalidConfigException
	 */
	public static function isSvg($file, $magicFile = null, $checkExtension = true)
	{
		return self::getMimeType($file, $magicFile, $checkExtension) === 'image/svg+xml';
	}

	/** @inheritdoc */
	public static function getMimeType($file, $magicFile = null, $checkExtension = true)
	{
		$mimeType = parent::getMimeType($file, $magicFile, $checkExtension);

		if ($checkExtension && in_array($mimeType, ['text/plain', 'text/html'])) {
			return static::getMimeTypeByExtension($file, $magicFile) ?: $mimeType;
		}

		return $mimeType;
	}

	/**
	 * 检查给定的是否为绝对路径
	 * @param $path
	 *
	 * @return bool
	 */
	public static function isAbsolutePath($path)
	{
		return substr($path, 0, 1) === '/' || substr($path, 1, 1) === ':';
	}

	/**
	 * 确定文件或目录是否存在
	 *
	 * @param string $path
	 * @return bool
	 */
	public static function exists($path)
	{
		return file_exists($path);
	}

	/**
	 * 确定给定的路径是一个文件
	 * @param string $file
	 * @return bool
	 */
	public static function isFile($file)
	{
		return is_file($file);
	}

	public static function getRequire($path)
	{
		if (static::isFile($path)) {
			return require $path;
		}
	}
}