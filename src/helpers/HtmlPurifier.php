<?php

namespace sail\helpers;

use HTMLPurifier_Config;
use HTMLPurifier_Encoder;

class HtmlPurifier extends \yii\helpers\HtmlPurifier
{
	/**
	 * @param $string
	 *
	 * @return string
	 */
	public static function cleanUtf8($string)
	{
		return HTMLPurifier_Encoder::cleanUTF8($string);
	}

	/**
	 * @param $string
	 * @param $config
	 *
	 * @return string
	 */
	public static function convertToUtf8($string, $config)
	{
		return HTMLPurifier_Encoder::convertToUTF8($string, $config, null);
	}
}