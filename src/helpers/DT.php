<?php

namespace sail\helpers;

use Sail;
use sail\i18n\Locale;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use DateTimeZone;

/**
 * 日期/时间的助手类
 *
 * 基本概念
 *
 *  - DateTime
 *      日期和时间类
 *      手册参考地址：http://php.net/manual/zh/class.datetime.php
 *
 *  - DateInterval
 *      表示一个时间周期的类
 *      手册参考地址：http://php.net/manual/zh/class.dateinterval.php
 *
 *  - DateTimeZone
 *      时区类
 */
class DT
{
	/**
	 * @var int 一分钟的秒数
	 */
	const SECONDS_MINUTE = 60;

	/**
	 * @var int 一小时的秒数
	 */
	const SECONDS_HOUR = 3600;

	/**
	 * @var int 一天的秒数
	 */
	const SECONDS_DAY = 86400;

	/**
	 * @var int 一周的秒数
	 */
	const SECONDS_WEEK = 604800;

	/**
	 * @var int 一个月的秒数，平均每月30.4368天
	 */
	const SECONDS_MONTH = 2629740;

	/**
	 * @var int 一年的秒数，平均每年365.2416天
	 */
	const SECONDS_YEAR = 31556874;

	/**
	 * @var array
	 */
	private static $_translationPairs;

	/**
	 * 将值转换为DateTime对象
	 *
	 * 支持格式：
	 *
	 *  - 当前的语言环境的短格式日期和时间的数组
	 *  - 所有W3C规范的日期和时间格式 (http://www.w3.org/TR/NOTE-datetime)
	 *  - MySQL的 DATE 和 DATETIME 格式 (http://dev.mysql.com/doc/refman/5.1/en/datetime.html)
	 *  - 宽松的W3C和MySQL格式（单数的月，日和小时）
	 *  - Unix时间戳
	 *
	 * @param mixed $value               要转换为DateTime对象的时间值
	 * @param bool $assumeSystemTimeZone 未指定时区是否假定系统时区已设置，如果为false则默认使用'UTC'
	 * @param bool $setToSystemTimeZone  对生成的DateTime对象是否设置为系统时区(默认为true)
	 *
	 * @return array|bool|DateTime
	 */
	public static function toDateTime($value, $assumeSystemTimeZone = false, $setToSystemTimeZone = true)
	{
		// 已经是一个DataTime对象了，不用转换
		if ($value instanceof DateTime) {
			return $value;
		}

		// 确定时区
		$defaultTimeZone = ($assumeSystemTimeZone ? Sail::$app->getTimeZone() : 'UTC');

		// 数组格式
		if (is_array($value) && (isset($value['date']) || isset($value['time']))) {
			$dt = $value;

			if (empty($dt['date']) && empty($dt['time'])) {
				return false;
			}

			$locale = Sail::$app->getLocale();

			if (!empty($value['timezone']) && ($normalizendTimeZone = static::normalizeTimeZone($value['timezone'])) !== false) {
				$timeZone = $normalizendTimeZone;
			} else {
				$timeZone = $defaultTimeZone;
			}

			if (!empty($dt['date'])) {
				$date   = $dt['date'];
				$format = $locale->getDateFormat(Locale::LENGTH_SHORT, Locale::FORMAT_PHP);

				// 确保是一个4位数的年份格式
				$format = Str::replace($format, 'y', 'Y');

				// 有效的分隔符是 '-', '.' 以及'/'
				if (Str::contains($format, '.')) {
					$separator = '.';
				} elseif (Str::contains($format, '-')) {
					$separator = '-';
				} else {
					$separator = '/';
				}

				// 确保提交的日期正在使用区域设置的分隔符
				$date = Str::replace($date, '-', $separator);
				$date = Str::replace($date, '.', $separator);
				$date = Str::replace($date, '/', $separator);

				// Check for a two-digit year as well
				$altFormat = Str::replace($format, 'Y', 'y');

				if (DateTime::createFromFormat($altFormat, $date) !== false) {
					$format = $altFormat;
				}
			} else {
				// 默认为当前日期
				$current = new DateTime('now', new DateTimeZone($timeZone));
				$format  = 'n/j/Y';
				$date    = $current->format($format);
			}

			if (!empty($dt['time'])) {
				$timePickerPhpFormat = $locale->getTimeFormat(Locale::LENGTH_SHORT, Locale::FORMAT_PHP);
				// 替换为本地化的 'AM' 和 'PM'
				if (preg_match('/(.*)(' . preg_quote($locale->getAMName(), '/') . '|' . preg_quote($locale->getPMName(), '/') . ')(.*)/u', $dt['time'], $matchs)) {
					$dt['time'] = $matchs[1] . $matchs[3];

					if ($matchs[2] == $locale->getAMName()) {
						$dt['time'] .= 'AM';
					} else {
						$dt['time'] .= 'PM';
					}

					$timePickerPhpFormat = str_replace('A', '', $timePickerPhpFormat) . 'A';
				}

				$date   .= ' ' . $dt['time'];
				$format .= ' ' . $timePickerPhpFormat;
			}

			// 添加时区
			$format .= ' e';
			$date   .= ' ' . $timeZone;
		} else {
			$date = trim((string)$value);

			if (preg_match('/^
                (?P<year>\d{4})                                  # YYYY (4位数的年份)
                (?:
                    -(?P<mon>\d\d?)                              # -M or -MM (1或2位数的月份)
                    (?:
                        -(?P<day>\d\d?)                          # -D or -DD (1或2位数的日)
                        (?:
                            [T\ ](?P<hour>\d\d?)\:(?P<min>\d\d)  # [T或空格]hh:mm (1或2位数的小时和2位数的分钟)
                            (?:
                                \:(?P<sec>\d\d)                  # :ss (2位数的秒)
                                (?:\.\d+)?                       # .s (decimal fraction of a second -- not supported)
                            )?
                            (?:[ ]?(?P<ampm>(AM|PM|am|pm))?)?    # 可选的空格与AM或PM
                            (?P<tz>Z|(?P<tzd>[+\-]\d\d\:?\d\d))? # Z 或 [+ 或 -]hh(:)ss (UTC or a timezone offset)
                        )?
                    )?
                )?$/x', $date, $m
			)) {
				$format = 'Y-m-d H:i:s';

				$date = $m['year'] . '-' . (!empty($m['mon']) ? sprintf('%02d', $m['mon']) : '01') . '-' . (!empty($m['day']) ? sprintf('%02d', $m['day']) : '01') . ' ' . (!empty($m['hour']) ? sprintf('%02d', $m['hour']) : '00') . ':' . (!empty($m['min']) ? $m['min'] : '00') . ':' . (!empty($m['sec']) ? $m['sec'] : '00');

				if (!empty($m['ampm'])) {
					$format .= ' A';
					$date   .= ' ' . $m['ampm'];
				}

				// 是否指定了时区？
				if (!empty($m['tz'])) {
					if (!empty($m['tzd'])) {
						$format .= strpos($m['tzd'], ':') !== false ? 'P' : 'O';
						$date   .= $m['tzd'];
					} else {
						// "Z" = UTC
						$format .= 'e';
						$date   .= 'UTC';
					}
				} else {
					$format .= 'e';
					$date   .= $defaultTimeZone;
				}
			} else if (static::isValidTimeStamp((int)$date)) {
				$format = 'U';
			} else {
				return false;
			}
		}

		$dt = DateTime::createFromFormat('!' . $format, $date);

		if ($dt !== false && $setToSystemTimeZone) {
			$dt->setTimezone(new DateTimeZone(Sail::$app->getTimeZone()));
		}

		return $dt;
	}

	/**
	 * 将时区字符串规范化为PHP时区标识符
	 *
	 * 支持格式:
	 *  - 时区缩写(EST, MDT)
	 *  - GMT的时差, 小时和分钟之间可以有/无冒号(+0200, -0200, +02:00, -02:00)
	 *  - PHP的时区标识符(UTC, GMT, Atlantic/Azores)
	 *
	 * @param string $timeZone
	 *
	 * @return bool|false|string
	 */
	public static function normalizeTimeZone($timeZone)
	{
		// 是否已经是一个PHP时区标识符
		if (in_array($timeZone, timezone_identifiers_list(), true)) {
			return $timeZone;
		}

		// 是否是时区的缩写
		if (($timeZoneName = timezone_name_from_abbr($timeZone)) !== false) {
			return $timeZoneName;
		}

		// GMT +08:00 即北京时间
		if (preg_match('/[+\-]\d\d\:?\d\d/', $timeZone, $matches)) {
			$format = strpos($timeZone, ':') !== false ? 'e' : 'O';
			$dt     = DateTime::createFromFormat($format, $timeZone, new DateTimeZone('UTC'));

			if ($dt !== false) {
				return $dt->format('e');
			}
		}

		// 未知情况
		return false;
	}

	/**
	 * 确定给定的值是否为ISO-8601格式的日期
	 *
	 * @param mixed $value
	 *
	 * @return bool
	 */
	public static function isIso8601($value)
	{
		return is_string($value) && preg_match('/^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d[\+\-]\d\d\:?\d\d$/', $value);
	}

	/**
	 * 将日期转换为ISO-8601字符串
	 * ISO-8601是日期和时间的一种表示方法，年由4位数组成，月为2位数。如：2018-01-02或20180102
	 *
	 * @param mixed $date
	 *
	 * @return bool|string
	 */
	public static function toIso8601($date)
	{
		$date = static::toDateTime($date);

		if ($date !== false) {
			return $date->format(DateTime::ATOM);
		}

		return false;
	}

	/**
	 * @return DateTime
	 */
	public static function currentUTCDateTime()
	{
		return new DateTime(null, new DateTimeZone('UTC'));
	}

	/**
	 * 获取当前时间戳
	 *
	 * @return int
	 */
	public static function currentTimeStamp()
	{
		$date = static::currentUTCDateTime();

		return $date->getTimestamp();
	}

	/**
	 * 将秒数转换为人类可读的时间间隔
	 *
	 * @param int $seconds
	 * @param bool $showSeconds
	 *
	 * @return string
	 */
	public static function secondsToHumanTimeDuration($seconds, $showSeconds = true)
	{
		$weeks   = floor($seconds / self::SECONDS_WEEK);
		$seconds %= self::SECONDS_WEEK;

		$days    = floor($seconds / self::SECONDS_DAY);
		$seconds %= self::SECONDS_DAY;

		$hours   = floor($seconds / self::SECONDS_HOUR);
		$seconds %= self::SECONDS_HOUR;

		if ($showSeconds) {
			$minutes = floor($seconds / self::SECONDS_MINUTE);
			$seconds %= self::SECONDS_MINUTE;
		} else {
			$minutes = round($seconds / self::SECONDS_MINUTE);
			$seconds = 0;
		}

		$timeComponents = [];

		if ($weeks) {
			$timeComponents[] = $weeks . ' ' . ($weeks == 1 ? Sail::t('app', 'week') : Sail::t('app', 'weeks'));
		}

		if ($days) {
			$timeComponents[] = $days . ' ' . ($days == 1 ? Sail::t('app', 'day') : Sail::t('app', 'days'));
		}

		if ($hours) {
			$timeComponents[] = $hours . ' ' . ($hours == 1 ? Sail::t('app', 'hour') : Sail::t('app', 'hours'));
		}

		if ($minutes || (!$showSeconds && !$weeks && !$days && !$hours)) {
			$timeComponents[] = $minutes . ' ' . ($minutes == 1 ? Sail::t('app', 'minute') : Sail::t('app', 'minutes'));
		}

		if ($seconds || ($showSeconds && !$weeks && !$days && !$hours && !$minutes)) {
			$timeComponents[] = $seconds . ' ' . ($seconds == 1 ? Sail::t('app', 'second') : Sail::t('app', 'seconds'));
		}

		return implode(', ', $timeComponents);
	}

	public static function isValidTimeStamp($timestamp)
	{
		if (!is_numeric($timestamp)) {
			return false;
		}

		$timestamp = (int)$timestamp;

		return $timestamp <= PHP_INT_MAX && $timestamp >= ~PHP_INT_MAX;
	}

	/**
	 * @param $date
	 *
	 * @return bool
	 */
	public static function isToday($date)
	{
		$date = self::toDateTime($date);
		$now  = new DateTime();

		return $date->format('Y-m-d') == $now->format('Y-m-d');
	}

	/**
	 * @param $date
	 *
	 * @return bool
	 */
	public static function isYesterday($date)
	{
		$date      = self::toDateTime($date);
		$yesterday = new DateTime('@' . strtotime('yesterday'));

		return $date->format('Y-m-d') == $yesterday->format('Y-m-d');
	}

	/**
	 * @param $date
	 *
	 * @return bool
	 */
	public static function isThisYear($date)
	{
		$date = self::toDateTime($date);
		$now  = new DateTime();

		return $date->format('Y') == $now->format('Y');
	}

	/**
	 * @param $date
	 *
	 * @return bool
	 */
	public static function isThisWeek($date)
	{
		$date = self::toDateTime($date);
		$now  = new DateTime();

		return $date->format('W Y') == $now->format('W Y');
	}

	/**
	 * @param $date
	 *
	 * @return bool
	 */
	public static function isThisMonth($date)
	{
		$date = self::toDateTime($date);
		$now  = new DateTime();

		return $date->format('m Y') == $now->format('m Y');
	}

	/**
	 * @param $date
	 * @param $timeInterval
	 *
	 * @return bool
	 */
	public static function isWithinLast($date, $timeInterval)
	{
		if (is_numeric($timeInterval)) {
			$timeInterval .= ' days';
		}

		$date      = self::toDateTime($date);
		$timestamp = $date->getTimestamp();

		// Bail early if it's in the future
		if ($timestamp > time()) {
			return false;
		}

		$earliestTimestamp = strtotime('-' . $timeInterval);

		return ($timestamp >= $earliestTimestamp);
	}

	/**
	 * @param $date
	 *
	 * @return bool
	 */
	public static function isInThePast($date)
	{
		$date = self::toDateTime($date);

		return $date->getTimestamp() < time();
	}

	/**
	 * 基于给定的秒数创建DateInterval对象
	 *
	 * @param int $seconds
	 *
	 * @return DateInterval
	 * @throws \Exception
	 */
	public static function secondsToInterval($seconds)
	{
		return new DateInterval("PT{$seconds}S");
	}

	/**
	 * @param $dateInterval
	 *
	 * @return int
	 * @throws \Exception
	 */
	public static function intervalToSeconds($dateInterval)
	{
		$reference = new DateTimeImmutable();
		$endTime   = $reference->add($dateInterval);

		return $endTime->getTimestamp() - $reference->getTimestamp();
	}

	/**
	 * @param $intervalString
	 *
	 * @return bool
	 */
	public static function isValidIntervalString($intervalString)
	{
		$interval = DateInterval::createFromDateString($intervalString);

		return $interval->s != 0 || $interval->i != 0 || $interval->h != 0 || $interval->d != 0 || $interval->m != 0 || $interval->y != 0;
	}

	/**
	 * @param $dateInterval
	 * @param bool $showSeconds
	 *
	 * @return string
	 */
	public static function humanDurationFromInterval($dateInterval, $showSeconds = true)
	{
		$timeComponents = [];

		if ($dateInterval->y) {
			$timeComponents[] = $dateInterval->y . ' ' . ($dateInterval->y > 1 ? Sail::t('app', 'years') : Sail::t('app', 'year'));
		}

		if ($dateInterval->m) {
			$timeComponents[] = $dateInterval->m . ' ' . ($dateInterval->m > 1 ? Sail::t('app', 'months') : Sail::t('app', 'month'));
		}

		if ($dateInterval->d) {
			$timeComponents[] = $dateInterval->d . ' ' . ($dateInterval->d > 1 ? Sail::t('app', 'days') : Sail::t('app', 'day'));
		}

		if ($dateInterval->h) {
			$timeComponents[] = $dateInterval->h . ' ' . ($dateInterval->h > 1 ? Sail::t('app', 'hours') : Sail::t('app', 'hour'));
		}

		$minutes = $dateInterval->i;

		if (!$showSeconds) {
			if ($minutes && round($dateInterval->s / 60)) {
				$minutes++;
			} else if (!$dateInterval->y && !$dateInterval->m && !$dateInterval->d && !$dateInterval->h && !$minutes) {
				return Sail::t('app', 'less than a minute');
			}
		}

		if ($minutes) {
			$timeComponents[] = $minutes . ' ' . ($minutes > 1 ? Sail::t('app', 'minutes') : Sail::t('app', 'minute'));
		}

		if ($showSeconds && $dateInterval->s) {
			$timeComponents[] = $dateInterval->s . ' ' . ($dateInterval->s > 1 ? Sail::t('app', 'seconds') : Sail::t('app', 'second'));
		}

		return implode(', ', $timeComponents);
	}

	private static function _getDateTranslations($language)
	{
		if (!isset(self::$_translationPairs[$language])) {
			if (strpos(Sail::$app->language, 'en') === 0) {
				$sourceLocale = Sail::$app->getLocale();
			} else {
				$sourceLocale = Sail::$app->getI18n()->getLocaleById('en-US');
			}

			$targetLocale = Sail::$app->getI18n()->getLocaleById($language);

			$amName = $targetLocale->getAMName();
			$pmName = $targetLocale->getPMName();

			self::$_translationPairs[$language] = array_merge(array_combine($sourceLocale->getMonthNames(Locale::LENGTH_FULL), $targetLocale->getMonthNames(Locale::LENGTH_FULL)), array_combine($sourceLocale->getWeekDayNames(Locale::LENGTH_FULL), $targetLocale->getWeekDayNames(Locale::LENGTH_FULL)), array_combine($sourceLocale->getMonthNames(Locale::LENGTH_MEDIUM), $targetLocale->getMonthNames(Locale::LENGTH_MEDIUM)), array_combine($sourceLocale->getWeekDayNames(Locale::LENGTH_MEDIUM), $targetLocale->getWeekDayNames(Locale::LENGTH_MEDIUM)), [
					'AM' => Str::toUpperCase($amName),
					'PM' => Str::toUpperCase($pmName),
					'am' => Str::toLowerCase($amName),
					'pm' => Str::toLowerCase($pmName),
				]
			);
		}

		return self::$_translationPairs[$language];
	}
}