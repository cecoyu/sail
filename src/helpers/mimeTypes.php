<?php

$mimeTypes = require Sail::getAlias('@yii/helpers/mimeTypes.php');

return array_merge(
	$mimeTypes,
	[
		'woff2' => 'application/font-woff2'
	]
);