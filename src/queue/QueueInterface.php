<?php

namespace sail\queue;

interface QueueInterface
{
	/**
	 * 执行所有队列中的任务
	 */
	public function run();

	/**
	 * 将失败的任务重新添加到队列中
	 * @param string $id
	 */
	public function retry($id);

	/**
	 * 从队列中释放任务
	 * @param string $id
	 */
	public function release($id);

	/**
	 * 设置当前保留任务的进度
	 * @param int $progress
	 */
	public function setProgress($progress);

	/**
	 * 是否有等待执行的任务
	 * @return bool
	 */
	public function getHasWaitingJobs();

	/**
	 *
	 * @return bool
	 */
	public function getHasReservedJobs();

	/**
	 * 返回队列中相关任务的信息
	 * 响应数组包括以下keys的子数组
	 * - 'id': 任务ID
	 * - 'status': 任务状态 (1 = waiting(等待中), 2 = reserved, 3 = done(已完成), 4 = failed(已失败))
	 * - 'progress': 任务进度 (0-100)
	 * - 'description': 任务描述
	 * - 'error': 错误信息 (如果任务失败)
	 *
	 * @param int|null $limit
	 * @return array
	 */
	public function getJobInfo($limit = null);
}