<?php

namespace sail\queue;

interface JobInterface extends \yii\queue\JobInterface
{
	/**
	 * @return string|null
	 */
	public function getDescription();

	/**
	 * @param \yii\queue\Queue|QueueInterface $queue
	 */
	public function execute($queue);
}