<?php

namespace sail\queue;

class Command extends \yii\queue\cli\Command
{
	/**
	 * @var Queue
	 */
	public $queue;

	/**
	 * @var string
	 */
	public $defaultAction = 'info';

	/**
	 * @inheritdoc
	 */
	public $verboseConfig = [
		'class' => VerboseBehavior::class
	];

	/**
	 * @param string $actionID
	 * @return bool
	 */
	protected function isWorkerAction($actionID)
	{
		return in_array($actionID, ['run', 'listen'], true);
	}

	/**
	 * @param \yii\base\Action $action
	 * @return bool
	 */
	public function beforeAction($action)
	{
		if (!parent::beforeAction($action)) {
			return false;
		}

		return true;
	}

	public function actions()
	{
		return [
			'info' => InfoAction::class
		];
	}

	/**
	 *
	 */
	public function actionRun()
	{
		$this->queue->run();
	}

	/**
	 * @param int $delay
	 */
	public function actionListen($delay = 3)
	{
		$this->queue->listen($delay);
	}
}