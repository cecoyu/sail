<?php

namespace sail\queue;

use Sail;
use sail\helpers\Db;
use sail\helpers\Json;
use sail\helpers\UrlHelper;
use yii\base\Exception;
use yii\db\Query;
use yii\queue\cli\Signal;
use yii\queue\ExecEvent;
use yii\web\Response;

class Queue extends \yii\queue\cli\Queue implements QueueInterface
{
	/**
	 * @var int 任务失败状态
	 */
	const STATUS_FAILED = 4;

	/**
	 * @var int 超时
	 */
	public $mutexTimeout = 3;

	/**
	 * @var string 命令类名
	 */
	public $commandClass = Command::class;

	/**
	 * @var string|null 被推入队列的任务描述
	 */
	private $_jobDescription;

	/**
	 * @var string|null 当前正在执行的任务ID
	 */
	private $_executingJobId;

	/**
	 * @var int 最近一次任务的时间戳将被保留
	 */
	private $_reserveTime;

	/**
	 * @var bool 是否正在监听网络响应
	 */
	private $_listeningForResponse = false;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		$this->on(self::EVENT_BEFORE_EXEC, function (ExecEvent $e) {
			$this->_executingJobId = $e->id;
		});

		$this->on(self::EVENT_AFTER_EXEC, function (ExecEvent $e) {
			$this->_executingJobId = null;
		});
	}

	/**
	 * @inheritdoc
	 */
	public function run()
	{
		while (!Signal::isExit() && ($payload = $this->reserve())) {
			if ($this->handleMessage($payload['id'], $payload['job'], $payload['ttr'], $payload['attempt'])) {
				$this->release($payload['id']);
			}
		}
	}

	/**
	 * 监听队列并运行新任务
	 *
	 * @param int $delay 等待新任务的秒数
	 */
	public function listen($delay)
	{
		do {
			$this->run();
		} while (!$delay || sleep($delay) === 0);
	}

	/**
	 * @param string $id
	 * @return bool
	 */
	public function isFailed($id)
	{
		return $this->status($id) === self::STATUS_FAILED;
	}

	public function status($id)
	{
		$payload = $this->_createJobQuery()
			->select(['fail', 'timeUpdated'])
			->where(['id' => $id])
			->one();

		return $this->_status($payload);
	}

	/**
	 * @inheritdoc
	 */
	public function push($job)
	{
		if ($job instanceof JobInterface) {
			$this->_jobDescription = $job->getDescription();
		} else {
			$this->_jobDescription = null;
		}

		if (($id = parent::push($job)) === null) {
			return null;
		}

		if (Sail::$app->getConfig()->getGeneral()->runQueueAutomatically && !$this->_listeningForResponse) {
			$request = Sail::$app->getRequest();
			if (!$request->getIsAjax()) {
				Sail::$app->getResponse()->on(Response::EVENT_AFTER_PREPARE, [$this, 'handleResponse']);
				$this->_listeningForResponse = true;
			}
		}

		return $id;
	}

	/**
	 * @inheritdoc
	 */
	public function retry($id)
	{
		Sail::$app->getDb()->createCommand()
			->update(
				'{{%queue}}',
				[
					'dateReserved' => null,
					'timeUpdated' => null,
					'progress' => 0,
					'attempt' => 0,
					'fail' => false,
					'dateFailed' => null,
					'error' => null
				],
				['id' => $id],
				[],
				false
			)
			->execute();
	}

	/**
	 * @inheritdoc
	 */
	public function release($id)
	{
		Sail::$app->getDb()->createCommand()
			->delete('{{%queue}}', ['id' => $id])
			->execute();
	}

	/**
	 * @inheritdoc
	 */
	public function setProgress($progress)
	{
		Sail::$app->getDb()->createCommand()
			->update(
				'{{%queue}}',
				[
					'progress' => $progress,
					'timeUpdated' => time()
				],
				['id' => $this->_executingJobId],
				[],
				false
			)
			->execute();
	}

	/**
	 * @inheritdoc
	 */
	public function getHasWaitingJobs()
	{
		return $this->_createWaitingJobQuery()->exists();
	}

	/**
	 * @inheritdoc
	 */
	public function getHasReservedJobs()
	{
		return $this->_createReservedJobQuery()->exists();
	}

	/**
	 * 返回等待中的任务总数
	 *
	 * @return int
	 */
	public function getTotalWaiting()
	{
		return $this->_createWaitingJobQuery()->count();
	}

	/**
	 *
	 * @return int
	 */
	public function getTotalDelayed()
	{
		return $this->_createDelayedJobQuery()->count();
	}

	/**
	 * @return int
	 */
	public function getTotalReserved()
	{
		return $this->_createReservedJobQuery()->count();
	}

	/**
	 * 返回失败的任务总数
	 *
	 * @return int
	 */
	public function getTotalFailed()
	{
		return $this->_createFailedJobQuery()->count();
	}

	/**
	 * @inheritdoc
	 */
	public function getJobInfo($limit = null)
	{
		$results = $this->_createJobQuery()
			->select(['id', 'description', 'progress', 'timeUpdated', 'fail', 'error'])
			->where('[[timePushed]] <= :time - [[delay]]', [':time' => time()])
			->orderBy(['priority' => SORT_ASC, 'id' => SORT_ASC])
			->limit($limit)
			->all();

		$info = [];

		foreach ($results as $result) {
			$info[] = [
				'id' => $result['id'],
				'status' => $this->_status($result),
				'progress' => (int)$result['progress'],
				'description' => $result['description'],
				'error' => $result['error']
			];
		}

		return $info;
	}

	/** @inheritdoc */
	public function handleError($id, $job, $ttr, $attempt, $error)
	{
		$this->_executingJobId = null;

		if (parent::handleError($id, $job, $ttr, $attempt, $error)) {
			Sail::$app->getErrorHandler()->logException($error);

			Sail::$app->getDb()->createCommand()
				->update(
					'{{%queue}}',
					[
						'fail' => true,
						'dateFailed' => Db::prepareDateForDb(new \DateTime()),
						'error' => $error->getMessage()
					],
					['id' => $id],
					[],
					false
				)
				->execute();
		}

		return false;
	}

	public function handleResponse()
	{
		$response = Sail::$app->getResponse();
		$response->off(Response::EVENT_AFTER_PREPARE, [$this, 'handleResponse']);

		if ($this->getHasReservedJobs()) {
			return;
		}

		if (!in_array($response->getContentType(), ['text/html', 'application/xhtml+xml'], true)) {
			return;
		}

		$url = Json::encode(UrlHelper::actionUrl('queue/run'));
		$js = <<<EOD
<script type="text/javascript">
/*<![CDATA[*/
(function(){
    var XMLHttpFactories = [
        function () {return new XMLHttpRequest()},
        function () {return new ActiveXObject("Msxml2.XMLHTTP")},
        function () {return new ActiveXObject("Msxml3.XMLHTTP")},
        function () {return new ActiveXObject("Microsoft.XMLHTTP")}
    ];
    var req = false;
    for (var i = 0; i < XMLHttpFactories.length; i++) {
        try {
            req = XMLHttpFactories[i]();
        }
        catch (e) {
            continue;
        }
        break;
    }
    if (!req) return;
    req.open('GET', $url, true);
    if (req.readyState == 4) return;
    req.send();
})();
/*]]>*/
</script>
EOD;

		if ($response->content === null) {
			$response->content = $js;
		} else {
			$response->content .= $js;
		}
	}

	/**
	 * @inheritdoc
	 */
	protected function pushMessage($message, $ttr, $delay, $priority)
	{
		$db = Sail::$app->getDb();
		$db->createCommand()
			->insert(
				'{{%queue}}',
				[
					'job' => $message,
					'description' => $this->_jobDescription,
					'timePushed' => time(),
					'ttr' => $ttr,
					'delay' => $delay,
					'priority' => $priority ?: 1024,
				],
				false)
			->execute();

		return $db->getLastInsertId('{{%queue}}');
	}

	protected function reserve()
	{
		$mutex = Sail::$app->getMutex();

		if (!$mutex->acquire(__CLASS__, $this->mutexTimeout)) {
			throw new Exception('Has not waited the lock.');
		}

		$db = Sail::$app->getDb();

		if ($this->_reserveTime !== time()) {
			$this->_reserveTime = time();
			$db->createCommand()
				->update(
					'{{%queue}}',
					[
						'dateReserved' => null,
						'timeUpdated' => null,
						'progress' => 0,
					],
					'[[timeUpdated]] < :time - [[ttr]]',
					[':time' => $this->_reserveTime],
					false
				)
				->execute();
		}

		$payload = $this->_createJobQuery()
			->where(['and', ['fail' => false, 'timeUpdated' => null], '[[timePushed]] <= :time - [[delay]]', [':time' => time()]])
			->orderBy(['priority' => SORT_ASC, 'id' => SORT_ASC])
			->limit(1)
			->one($db);

		if (is_array($payload)) {
			$payload['dateReserved'] = new \DateTime();
			$payload['timeUpdated'] = $payload['dateReserved']->getTimestamp();
			$payload['attempt'] = (int)$payload['attempt'] + 1;
			$db->createCommand()
				->update(
					'{{%queue}}',
					[
						'dateReserved' => Db::prepareDateForDb($payload['dateReserved']),
						'timeUpdated' => $payload['timeUpdated'],
						'attempt' => $payload['attempt']
					],
					['id' => $payload['id']],
					[],
					false
				)
				->execute();
		}

		$mutex->release(__CLASS__);

		if (is_array($payload) && is_resource($payload['job'])) {
			$payload['job'] = stream_get_contents($payload['job']);
		}

		return $payload;
	}

	/**
	 * 返回一个新的任务查询
	 *
	 * @return Query
	 */
	private function _createJobQuery()
	{
		return (new Query())
			->from('{{%queue}}');
	}

	/**
	 * @return Query
	 */
	private function _createWaitingJobQuery()
	{
		return $this->_createJobQuery()
			->where(['fail' => false, 'timeUpdated' => null])
			->andWhere('[[timePushed]] + [[delay]] <= :time', ['time' => time()]);
	}

	/**
	 * @return Query
	 */
	private function _createDelayedJobQuery()
	{
		return $this->_createJobQuery()
			->where(['fail' => false, 'timeUpdated' => null])
			->andWhere('[[timePushed]] + [[delay]] > :time', ['time' => time()]);
	}

	/**
	 * @return Query
	 */
	private function _createReservedJobQuery()
	{
		return $this->_createJobQuery()
			->where(['and', ['fail' => false], ['not', ['timeUpdated' => null]]]);
	}

	/**
	 * @return Query
	 */
	private function _createFailedJobQuery()
	{
		return $this->_createJobQuery()
			->where(['fail' => true]);
	}

	/**
	 * @param array|false $payload
	 * @return int
	 */
	private function _status($payload)
	{
		if (!$payload) {
			return self::STATUS_DONE;
		}

		if ($payload['fail']) {
			return self::STATUS_FAILED;
		}

		if (!$payload['timeUpdated']) {
			return self::STATUS_WAITING;
		}

		return self::STATUS_RESERVED;
	}
}