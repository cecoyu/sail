<?php

namespace sail\queue;

use yii\helpers\Console;
use yii\queue\cli\Action;

class InfoAction extends Action
{
	/**
	 * @var Queue
	 */
	public $queue;

	/**
	 *
	 */
	public function run()
	{
		Console::output($this->format('Jobs', Console::FG_GREEN));

		Console::stdout($this->format('- waiting: ', Console::FG_YELLOW));
		console::output($this->queue->getTotalWaiting());

		Console::stdout($this->format('- delayed: ', Console::FG_YELLOW));
		Console::output($this->queue->getTotalDelayed());

		Console::stdout($this->format('- reserved: ', Console::FG_YELLOW));
		Console::output($this->queue->getTotalReserved());

		Console::stdout($this->format('- failed: ', Console::FG_YELLOW));
		console::output($this->queue->getTotalFailed());
	}
}