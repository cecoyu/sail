<?php

namespace sail\queue;

use yii\base\BaseObject;

abstract class BaseJob extends BaseObject implements JobInterface
{
	/**
	 * @var string|null 任务描述
	 */
	public $description;

	/**
	 * @var int 当前进度
	 */
	private $_progress;

	public function init()
	{
		parent::init();

		// 设置默认进度
		$this->_progress = 0;
	}

	/**
	 * @inheritdoc
	 */
	public function getDescription()
	{
		return $this->description ?: $this->defaultDescription();
	}

	/**
	 *
	 * @return string|null
	 */
	protected function defaultDescription()
	{
		return null;
	}

	/**
	 * @param \yii\queue\Queue|QueueInterface $queue
	 * @param float $progress
	 */
	protected function setProgress($queue, $progress)
	{
		if ($progress !== $this->_progress && $queue instanceof QueueInterface) {
			$queue->setProgress(round(100 * $progress));
		}
	}
}