<?php

namespace sail\queue;

use yii\queue\ExecEvent;

class VerboseBehavior extends \yii\queue\cli\VerboseBehavior
{
	/**
	 * @param ExecEvent $event
	 * @return string
	 */
	protected function jobTitle($event)
	{
		if (!$event->job instanceof JobInterface) {
			return parent::jobTitle($event);
		}

		$description = $event->job->getDescription();
		$extra = 'attempt: ' . $event->attempt;

		if ($pid = $event->sender->getWorkerPid()) {
			$extra .= ', pid: ' . $pid;
		}

		return " [{$event->id}] {$description} ({$extra})";
	}
}