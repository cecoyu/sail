<?php

namespace sail\events;

use yii\base\Event;

class CancelableEvent extends Event
{
	/**
	 * @var bool 是否继续执行调用该事件的操作
	 */
	public $isValid = true;
}