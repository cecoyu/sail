<?php

namespace sail\behaviors;

use Sail;
use yii\base\Behavior;
use yii\base\Controller;
use yii\web\Response;
use yii\helpers\Url;


/**
 * @property Controller $owner
 */
class JumpBehavior extends Behavior
{
	/** @var string 视图文件 */
	public $viewFile = '@sail/behaviors/dispatch_jump.tpl';

	/**
	 * 操作成功跳转的快捷方法
	 * @param mixed $msg 提示信息
	 * @param string|null $url 跳转的URL地址
	 * @param mixed $data 返回的数据
	 * @param int $wait 跳转等待时间
	 * @param array $header 发送的Header信息
	 * @return void
	 */
	public function success($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
	{
		$code = 1;
		if (is_numeric($msg)) {
			$code = $msg;
			$msg  = '';
		}
		if (is_null($url) && isset($_SERVER['HTTP_REFERER'])) {
			$url = $_SERVER['HTTP_REFERER'];
		} elseif ($url !== '') {
			$url = (strpos($url, '://') || strpos($url, '/') === 0) ? $url : Url::to($url);
		}
		$result = [
			'code' => $code,
			'msg'  => $msg,
			'data' => $data,
			'url'  => $url,
			'wait' => $wait,
		];

		$response = Sail::$app->response;
		$format   = $this->getResponseType();
		if (Response::FORMAT_HTML === $format) {
			$response->content = $this->owner->renderPartial($this->viewFile, $result);
		} else {
			$response->data = $result;
		}

		$headers = $response->headers;
		if (is_array($header) && !empty($header)) {
			foreach ($header as $name => $value) {
				$headers->set($name, $value);
			}
		}

		$response->format = $format;
		$response->send();
	}

	public function error($msg = '', $url = null, $data = '', $wait = 3, array $header = [])
	{
		$code = 0;
		if (is_numeric($msg)) {
			$code = $msg;
			$msg  = '';
		}
		if (is_null($url)) {
			$url = Sail::$app->request->isAjax ? '' : 'javascript:history.back(-1);';
		} elseif ($url !== '') {
			$url = (strpos($url, '://') || strpos($url, '/') === 0) ? $url : Url::to($url);
		}
		$result = [
			'code' => $code,
			'msg'  => $msg,
			'data' => $data,
			'url'  => $url,
			'wait' => $wait,
		];

		$response = Sail::$app->response;
		$format   = $this->getResponseType();
		if (Response::FORMAT_HTML === $format) {
			$response->content = Sail::$app->view->renderFile($this->viewFile, $result);
		} else {
			$response->data = $result;
		}

		$headers = $response->headers;
		if (is_array($header) && !empty($header)) {
			foreach ($header as $name => $value) {
				$headers->set($name, $value);
			}
		}

		$response->format = $format;
		$response->send();
	}

	public function result($data, $code = 0, $msg = '', $type = '', array $header = [])
	{
		$result = [
			'code' => $code,
			'msg'  => $msg,
			'time' => $_SERVER['REQUEST_TIME'],
			'data' => $data,
		];

		$response = Sail::$app->response;
		$format   = $type ?: $this->getResponseType();

		$headers = $response->headers;
		if (is_array($header) && !empty($header)) {
			foreach ($header as $name => $value) {
				$headers->set($name, $value);
			}
		}
		$response->format = $format;
		$response->data   = $result;
		$response->send();
	}

	public function getResponseType()
	{
		$isAjax = Sail::$app->request->isAjax;

		return $isAjax ? Response::FORMAT_JSON : Response::FORMAT_HTML;
	}
}