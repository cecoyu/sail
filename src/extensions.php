<?php

$pluginsDir = dirname(__DIR__);

return array (
  'backend' => 
  array (
    'class' => 'sail\\backend\\Plugin',
    'basePath' => 'E:\\www\\sail\\plugins\\backend',
    'handle' => 'backend',
    'aliases' => 
    array (
      '@sail/backend' => 'E:\\www\\sail\\plugins\\backend',
    ),
    'name' => '后台管理',
    'version' => '1.0.0',
    'description' => '提供后台管理所需功能',
    'author' => 'Money',
  ),
  'user' => 
  array (
    'class' => 'sail\\user\\Plugin',
    'basePath' => 'E:\\www\\sail\\plugins\\user',
    'handle' => 'user',
    'aliases' => 
    array (
      '@sail/user' => 'E:\\www\\sail\\plugins\\user',
    ),
    'name' => '用户模块',
    'version' => '1.0.0',
    'description' => '核心模块-用户',
    'author' => 'Money',
    'components' => 
    array (
      'userService' => 'user\\services\\UserService',
    ),
  ),
);
