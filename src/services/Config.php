<?php

namespace sail\services;

use ArrayAccess;
use Sail;
use sail\config\FileLoader;
use sail\config\RepositoryInterface;
use sail\helpers\Arr;
use sail\base\KeyParser;

use yii\base\Component;

class Config extends Component implements ArrayAccess, RepositoryInterface
{
	use KeyParser;

	/**
	 * 加载器
	 *
	 * @var \sail\config\LoaderInterface
	 */
	protected $loader;

	/**
	 * 当前环境
	 *
	 * @var string
	 */
	protected $environment;

	/**
	 * 所有配置项
	 *
	 * @var array
	 */
	protected $items = [];

	/**
	 * 所有已注册的包
	 *
	 * @var array
	 */
	protected $packages = [];

	/**
	 *
	 * @var array
	 */
	protected $afterLoad = [];

	public function init()
	{
		parent::init();

		$fileLoader = new FileLoader(ROOT . '/config');

		$this->loader = $fileLoader;
		$this->environment = YII_ENV;
	}

	/**
	 * 确定给定的配置值是否存在
	 *
	 * @param string $key
	 * @return bool
	 */
	public function has($key)
	{
		$default = microtime(true);

		return $this->get($key, $default) !== $default;
	}

	/**
	 * 确定给定的配置组是否存在
	 *
	 * @param string $key
	 * @return bool
	 */
	public function hasGroup($key)
	{
		list($namespace, $group, $item) = $this->parseConfigKey($key);

		return $this->loader->exists($group, $namespace);
	}

	/**
	 * 获取指定的配置值
	 *
	 * @param string $key
	 * @param null $default
	 * @return mixed
	 */
	public function get($key, $default = null)
	{
		list($namespace, $group, $item) = $this->parseConfigKey($key);

		$collection = $this->getCollection($group, $namespace);

		$this->load($group, $namespace, $collection);

		return Arr::getValue($this->items[$collection], $item, $default);
	}

	/**
	 * 设置给定的配置值
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	public function set($key, $value = null)
	{
		if (is_array($key)) {
			foreach ($key as $innerKey => $innerValue) {
				$this->set($innerKey, $innerValue);
			}
		} else {
			list($namespace, $group, $item) = $this->parseConfigKey($key);

			$collection = $this->getCollection($group, $namespace);

			$this->load($group, $namespace, $collection);

			if (is_null($item)) {
				$this->items[$collection] = $value;
			} else {
				Arr::setValue($this->items[$collection], $item, $value);
			}
		}
	}

	/**
	 * Prepend a value onto an array configuration value.
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return void
	 */
	public function prepend($key, $value)
	{
		$array = $this->get($key);

		array_unshift($array, $value);

		$this->set($key, $array);
	}

	/**
	 *
	 * @param string $key
	 * @param mixed $value
	 */
	public function push($key, $value)
	{
		$array = $this->get($key);

		$array[] = $value;

		$this->set($key, $array);
	}

	/**
	 * 获取所有配置项
	 *
	 * @return array
	 */
	public function all()
	{
		return $this->items;
	}

	/**
	 * 解析key到namespace, group以及item
	 *
	 * @param string $key
	 * @return array
	 */
	public function parseConfigKey($key)
	{
		if (strpos($key, '::') === false) {
			return $this->parseKey($key);
		}

		if (isset($this->keyParserCache[$key])) {
			return $this->keyParserCache[$key];
		}

		$parsed = $this->parseNamespacedSegments($key);
		return $this->keyParserCache[$key] = $parsed;
	}

	/**
	 * register a package for cascading configuration.
	 *
	 * @param string $namespace
	 * @param string $hint
	 */
	public function package($namespace, $hint)
	{
		$this->packages[] = $namespace;

		$this->addNamespace($namespace, $hint);

		$this->afterLoading($namespace, function ($me, $group, $items) use ($namespace) {
			$env = $me->getEnvironment();

			$loader = $me->getLoader();

			return $loader->cascadePackage($env, $namespace, $group, $items);
		});
	}

	/**
	 * register an after load callback for a given namespace.
	 *
	 * @param string $namespace
	 * @param \Closure $callback
	 */
	public function afterLoading($namespace, $callback)
	{
		$this->afterLoad[$namespace] = $callback;
	}

	/**
	 * 添加新的命名空间到加载器
	 *
	 * @param string $namespace
	 * @param string $hint
	 */
	public function addNamespace($namespace, $hint)
	{
		$this->loader->addNamespace($namespace, $hint);
	}

	/**
	 * 返回所有已注册的命名空间
	 *
	 * @return array
	 */
	public function getNamespaces()
	{
		return $this->loader->getNamespaces();
	}

	/**
	 * get the loader implementation.
	 *
	 * @return \sail\config\LoaderInterface
	 */
	public function getLoader()
	{
		return $this->loader;
	}

	/**
	 * set the loader implementation.
	 *
	 * @param $loader \sail\config\LoaderInterface
	 */
	public function setLoader($loader)
	{
		$this->loader = $loader;
	}

	/**
	 * 获取当前配置环境
	 *
	 * @return string
	 */
	public function getEnvironment()
	{
		return $this->environment;
	}

	/**
	 * 获取加载后配置后调用的回调函数数组
	 *
	 * @return array
	 */
	public function getAfterLoadCallbacks()
	{
		return $this->afterLoad;
	}

	/**
	 * 获取所有配置项
	 *
	 * @return array
	 */
	public function getItems()
	{
		return $this->items;
	}

	/**
	 * 根据给定的key加载配置组
	 *
	 * @param string $group
	 * @param string $namespace
	 * @param string $collection
	 */
	protected function load($group, $namespace, $collection)
	{
		$env = $this->environment;

		if (isset($this->items[$collection])) {
			return;
		}

		$items = $this->loader->load($env, $group, $namespace);

		if (isset($this->afterLoad[$namespace])) {
			$items = $this->callAfterLoad($namespace, $group, $items);
		}

		$this->items[$collection] = $items;
	}

	/**
	 * call the after load callback for a namespace.
	 *
	 * @param string $namespace
	 * @param string $group
	 * @param array $items
	 * @return mixed
	 */
	protected function callAfterLoad($namespace, $group, $items)
	{
		$callback = $this->afterLoad[$namespace];

		return call_user_func($callback, $this, $group, $items);
	}

	/**
	 * parse an array of namespaced segments.
	 *
	 * @param string $key
	 * @return array
	 */
	protected function parseNamespacedSegments($key)
	{
		list($namespace, $item) = explode('::', $key);

		if (in_array($namespace, $this->packages)) {
			return $this->parsePackageSegments($key, $namespace, $item);
		}

		return $this->keyParserParseSegments($key);
	}

	/**
	 * @param string $key
	 * @param string $namespace
	 * @param string $item
	 * @return array
	 */
	protected function parsePackageSegments($key, $namespace, $item)
	{
		$itemSegments = explode('.', $item);

		if (!$this->loader->exists($itemSegments[0], $namespace)) {
			return [$namespace, 'config', $item];
		}

		return $this->keyParserParseSegments($key);
	}

	/**
	 * 获取集合识别符
	 *
	 * @param string $group
	 * @param string $namespace
	 * @return string
	 */
	protected function getCollection($group, $namespace = null)
	{
		$namespace = $namespace ?: '*';

		return $namespace . '::' . $group;
	}

	/**
	 * Whether a offset exists
	 * @link http://php.net/manual/en/arrayaccess.offsetexists.php
	 * @param mixed $offset <p>
	 * An offset to check for.
	 * </p>
	 * @return boolean true on success or false on failure.
	 * </p>
	 * <p>
	 * The return value will be casted to boolean if non-boolean was returned.
	 * @since 5.0.0
	 */
	public function offsetExists($offset)
	{
		return $this->has($offset);
	}

	/**
	 * Offset to retrieve
	 * @link http://php.net/manual/en/arrayaccess.offsetget.php
	 * @param mixed $offset <p>
	 * The offset to retrieve.
	 * </p>
	 * @return mixed Can return all value types.
	 * @since 5.0.0
	 */
	public function offsetGet($offset)
	{
		return $this->get($offset);
	}

	/**
	 * Offset to set
	 * @link http://php.net/manual/en/arrayaccess.offsetset.php
	 * @param mixed $offset <p>
	 * The offset to assign the value to.
	 * </p>
	 * @param mixed $value <p>
	 * The value to set.
	 * </p>
	 * @return void
	 * @since 5.0.0
	 */
	public function offsetSet($offset, $value)
	{
		$this->set($offset, $value);
	}

	/**
	 * Offset to unset
	 * @link http://php.net/manual/en/arrayaccess.offsetunset.php
	 * @param mixed $offset <p>
	 * The offset to unset.
	 * </p>
	 * @return void
	 * @since 5.0.0
	 */
	public function offsetUnset($offset)
	{
		$this->set($offset, null);
	}
}