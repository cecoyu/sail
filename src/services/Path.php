<?php

namespace sail\services;

use Sail;
use sail\helpers\File;
use yii\base\Component;

class Path extends Component {

	/** @var string|null 临时目录路径 */
	private $_tempPath;

	/**
	 * 获取临时目录路径
	 *
	 * @param bool $create 是否创建目录
	 *
	 * @return null|string
	 * @throws \yii\base\Exception
	 */
	public function getTempPath($create = true)
	{
		if ($this->_tempPath === null) {
			$path = Sail::getAlias('@runtime') . DIRECTORY_SEPARATOR . 'temp';
			$this->setTempPath($path);

			if ($create) {
				File::createDirectory($path);
			}
		}

		return $this->_tempPath;
	}

	/**
	 * 创建临时目录路径
	 * @param string $path
	 */
	public function setTempPath($path)
	{
		$this->_tempPath = Sail::getAlias($path);
	}
}