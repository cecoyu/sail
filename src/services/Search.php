<?php

namespace sail\services;

use Sail;
use yii\base\Component;

class Search extends Component
{
	/**
	 * @event
	 */
	const EVENT_BEFORE_SEARCH = 'beforeSearch';

	/**
	 * @event
	 */
	const EVENT_AFTER_SEARCH = 'afterSearch';

	/**
	 * @var
	 */
	public $minFullTextWordLength;

	/**
	 * @var
	 */
	private $_tokens;

	/**
	 * @var
	 */
	private $_groups;

	public $maxPostgresKeywordLength = 2450;

	/**
	 * @inheritdoc
	 */
	public function init()
	{
		parent::init();

		if ($this->minFullTextWordLength === null) {
			if (Sail::$app->getDb()->getIsMysql()) {
				$this->minFullTextWordLength = 4;
			} else {
				$this->minFullTextWordLength = 1;
			}
		}
	}

	public function indexModelAttributes($model)
	{
		$searchableAttributes = $model::searchableAttributes();
	}
}