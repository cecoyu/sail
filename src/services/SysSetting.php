<?php

namespace sail\services;

use yii\base\Component;
use yii\helpers\Json;

class SysSetting extends Component
{
	/**
	 * @var array 默认设置项
	 */
	public $defaults;

	/**
	 * @var array|null 已存储设置项
	 */
	private $_settings;

	/**
	 * 返回指定名称的系统设置项
	 *
	 * @param string $name
	 *
	 * @return array
	 */
	public function getSettings($name)
	{
		$record = $this->_getSettingsRecord($name);

		if (!is_null($record)) {
			$options = Json::decode($record->value);
		} else {
			$options = [];
		}

		if (isset($this->defaults[$name])) {
			$options = array_merge($this->defaults[$name], $options);
		}

		return $options;
	}

	/**
	 * @param string $name
	 * @param string $key
	 *
	 * @return array|null
	 */
	public function getOption($name, $key)
	{
		$options = $this->getSettings($name);

		if (isset($options[$key])) {
			return $options;
		}

		return null;
	}

	/**
	 * 根据名称返回设置项记录
	 * @param string $name
	 *
	 * @return SysSetting|null
	 */
	private function _getSettingsRecord($name)
	{
		if (!isset($this->_settings[$name])) {
			$record = SysSetting::findOne([
				'name' => $name
			]);

			if ($record) {
				$this->_settings[$name] = $record;
			} else {
				$this->_settings[$name] = false;
			}
		}

		if ($this->_settings[$name] !== false) {
			return $this->_settings[$name];
		}

		return null;
	}
}