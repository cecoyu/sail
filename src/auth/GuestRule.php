<?php

namespace sail\auth;

use yii\rbac\Item;
use yii\rbac\Rule;

class GuestRule extends Rule
{
	/**
	 * @inheritdoc
	 */
	public $name = 'guest_rule';

	/**
	 * @inheritdoc
	 */
	public function execute($user, $item, $params)
	{
		return $user->getIsGuest();
	}
}