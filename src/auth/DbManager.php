<?php

namespace sail\auth;

use yii\caching\TagDependency;

class DbManager extends \yii\rbac\DbManager
{
	const CACHE_TAG = 'sail.auth';

	/**
	 * @var bool 是否只对已注册的路由进行权限检查
	 */
	public $onlyRegisteredRoute = true;

	// 缓存默认授权数据
	private $_assignments = [];
	private $_childrenList;

	// 缓存路由授权数据
	private $_routes;
	private $_defaultRoutes;
	private $_userRoutes;

	/**
	 * @inheritdoc
	 */
	public function getAssignments($userId)
	{
		if (!isset($this->_assignments[$userId])) {
			$this->_assignments[$userId] = parent::getAssignments($userId);
		}

		return $this->_assignments[$userId];
	}

	/**
	 * @inheritdoc
	 */
	protected function getChildrenList()
	{
		if ($this->_childrenList === null) {
			$this->_childrenList = parent::getChildrenList();
		}

		return $this->_childrenList;
	}

	/**
	 * 获取已注册路由列表
	 * @return array|null
	 */
	protected function getRegisteredRoutes()
	{
		if ($this->_routes === null) {
			$this->_routes = [];
			foreach ($this->getPermissions() as $item) {
				// 只获取路由
			    if ($item->name[0] === '/') {
			    	$this->_routes[$item->name] = $item->name;
			    }
			}
		}

		return $this->_routes;
	}

	/**
	 * 获取默认角色的授权路由地址
	 *
	 * @return array
	 */
	protected function getDefaultRoutes()
	{
		if ($this->_defaultRoutes === null) {
			$roles = $this->defaultRoles;
			$cache = $this->cache;
			if ($cache && ($routes = $cache->get($roles)) !== false) {
				$this->_defaultRoutes = $routes;
			} else {
				$permissions = $this->_defaultRoutes = [];
				foreach ($roles as $role) {
				    $permissions = array_merge($permissions, $this->getPermissionsByRole($role));
				}
				foreach ($permissions as $item) {
				    if ($item->name[0] === '/') {
				    	$this->_defaultRoutes[$item->name] = true;
				    }
				}

				// 缓存路由
				if ($cache) {
					$cache->set($roles, $this->_defaultRoutes, null, new TagDependency([
						'tags' => self::CACHE_TAG
					]));
				}
			}
		}

		return $this->_defaultRoutes;
	}

	/**
	 * 获取特定用户的授权路由
	 *
	 * @param int $userId
	 * @return array
	 */
	protected function getRoutesByUser($userId)
	{
		if (!isset($this->_userRoutes[$userId])) {
			$cache = $this->cache;
			// 缓存是个好东西
			if ($cache && ($routes = $cache->get([__METHOD__, $userId])) !== false) {
				$this->_userRoutes[$userId] = $routes;
			} else {
				// 提供最基本的权限和附加个人权限.不过依然没有人权
				$routes = $this->getDefaultRoutes();
				foreach ($this->getPermissionsByUser($userId) as $item) {
					// 以"/"开头都判定为路由地址
				    if ($item->name[0] === '/') {
				    	$routes[$item->name] = true;
				    }
				}
				$this->_userRoutes[$userId] = $routes;
				if ($cache) {
					// 除字符串作为key外，其他数据类型都将先进行json编码再加密为MD5串
					$cache->set([__METHOD__, $userId], $routes, null, new TagDependency([
						'tags' => self::CACHE_TAG
					]));
				}
			}
		}

		return $this->_userRoutes[$userId];
	}
}