<?php

namespace sail\auth;

use Sail;
use sail\web\Request;
use sail\web\User;
use yii\base\Module;
use yii\di\Instance;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

class AccessControl extends ActionFilter
{
	/**
	 * @var User|string 用户应用组件ID
	 */
	public $user = 'user';

	/**
	 * @var callable 如果提供该回调函数，在访问被拒绝后将被调用
	 */
	public $denyCallback;

	/**
	 * @var bool 是否只对已注册的路由进行权限检查
	 */
	public $onlyRegisteredRoute = false;

	/**
	 * @var bool 是否开启严格验证
	 */
	public $strict = true;

	/**
	 * 初始化用户组件
	 * @throws \yii\base\InvalidConfigException
	 */
	public function init()
	{
		parent::init();
		if ($this->user !== false) {
			$this->user = Instance::ensure($this->user, User::class);
		}
	}

	/**
	 * @inheritdoc 检查当前用户是否有权限继续执行该动作;
	 * @throws ForbiddenHttpException
	 */
	public function beforeAction($action)
	{
		$user    = $this->user;
		$request = Sail::$app->getRequest();

		// 允许当前用户访问该方法？
		if ($this->allow('/' . $action->getUniqueId(), $user, $request)) {
			return true;
		}

		if ($this->denyCallback !== null) {
			// 调用自定义回调函数处理无效访问
			call_user_func($this->denyCallback, null, $action);
		} else {
			// 系统默认处理
			$this->denyAccess($user);
		}

		return false;
	}

	/**
	 * 禁止用户访问后的处理
	 *
	 * @todo 优化处理方法
	 *
	 * @param User $user
	 *
	 * @throws ForbiddenHttpException
	 */
	protected function denyAccess($user)
	{
		if ($user !== false && $user->getIsGuest()) {
			$user->loginRequired();
		} else {
			throw new ForbiddenHttpException(Sail::t('yii', 'You are not allowed to perform this action.'));
		}
	}

	/**
	 * 该方法在beforeFilter中被调用
	 * 返回一个布尔值，该值将指示过滤器对于给定的动作是否生效
	 * 该函数在调用beforeAction之前执行。
	 *
	 * @inheritdoc
	 *
	 * @param \yii\base\Action $action
	 *
	 * @return bool 对于给定的动作，过滤器是否有效
	 */
	protected function isActive($action)
	{
		$uniqueId = $action->getUniqueId();

		// 当前是否是错误页面(40x, 50x)
		if ($uniqueId === Sail::$app->getErrorHandler()->errorAction) {
			return false;
		}

		// 检查当前是否是登录页
		$user = $this->user;
		if ($user->isGuest) {
			$loginUrl = null;
			if (is_array($user->loginUrl) && isset($user->loginUrl[0])) {
				$loginUrl = $user->loginUrl[0];
			} elseif (is_string($user->loginUrl)) {
				$loginUrl = $user->loginUrl;
			}

			// 当前访问的是登录页面
			if (!is_null($loginUrl) && trim($loginUrl, '/') === $uniqueId) {
				return false;
			}
		}

		if ($this->owner instanceof Module) {
			$mid = $this->owner->getUniqueId();
			$id  = $uniqueId;
			if ($mid !== '' && strpos($id, $mid . '/') === 0) {
				$id = substr($id, strlen($mid) + 1);
			}
		} else {
			$id = $action->id;
		}

		// 检查当前路由是否在全局排除列表中
		foreach ($this->except as $route) {
			// 支持范围性的路由排除
			if (substr($route, -1) === '*') {
				$route = rtrim($route, '*');
				if ($route === '' || strpos($id, $route) === 0) {
					return false;
				}
			}

			// 当前路由？
			if ($id === $route) {
				return false;
			}
		}

		// 检查是否存在当前控制器的排除列表
		if ($action->controller->hasMethod('except') && in_array($action->id, $action->controller->allowAnonymous())) {
			return false;
		}

		// 禁止访问该动作
		return true;
	}

	/**
	 * 是否允许该用户访问当前路由
	 *
	 * @param string $route
	 * @param User $user
	 * @param Request $request
	 *
	 * @return bool
	 */
	protected function allow($route, $user, $request)
	{
		$params = $request->get();
		/** @var  $authManager */
		$authManager = Sail::$app->getAuthManager();

		// 只检查已注册的路由
		// 即注册了该路由地址，当前又正在访问该路由，就要检查该用户是否有授权了。
		if ($this->onlyRegisteredRoute && !isset($authManager->getRegisteredRoutes()[$route])) {
			return true;
		}

		$userId = $user instanceof User ? $user->getId() : $user;
		// 权限、AFC都会检查
		if ($this->strict) {
			if ($user->can($route, $params)) {
				return true;
			}

			// 检查该用户是否有当前路由的授权，支持范围性检查。
			while (($pos = strrpos($route, '/')) > 0) {
				$route = substr($route, 0, $pos);
				if ($user->can($route . '/*', $params)) {
					return true;
				}
			}

			return $user->can('/*', $params);
		} else {
			$routes = $authManager->getRoutesByUser($userId);
			if (isset($routes[$route])) {
				return true;
			}

			while (($pos = strrpos($route, '/')) > 0) {
				$route = substr($route, 0, $pos);
				if (isset($routes[$route . '/*'])) {
					return true;
				}

				return isset($routes['/*']);
			}
		}
	}
}