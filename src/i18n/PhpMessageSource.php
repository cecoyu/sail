<?php

namespace sail\i18n;

use Sail;
use yii\base\Exception;

class PhpMessageSource extends \yii\i18n\PhpMessageSource
{
	/**
	 * @var bool
	 */
	public $allowOverrides = false;

	/** @inheritdoc */
	protected function loadMessages($category, $language)
	{
		$messages = parent::loadMessages($category, $language);

		if ($this->allowOverrides) {
			$overrideMessages = $this->_loadOverrideMessages($category, $language);
			$messages = array_merge($messages, $overrideMessages);
		}

		return $messages;
	}

	/**
	 * @param string $category
	 * @param string $language
	 *
	 * @return array|null
	 * @throws Exception
	 */
	private function _loadOverrideMessages($category, $language)
	{
		$oldBasePath = $this->basePath;
		$newBasePath = Sail::getAlias('@translations');

		if ($newBasePath === false) {
			throw new Exception('There was a problem getting the translations path.');
		}

		$this->basePath = $newBasePath;

		$messageFile = $this->getMessageFilePath($category, $language);
		$messages = $this->loadMessagesFromFile($messageFile);

		$fallbackLanguage = substr($language, 0, 2);
		if ($fallbackLanguage !== $language) {
			$fallbackMessageFile = $this->getMessageFilePath($category, $fallbackLanguage);
			$fallbackMessages = $this->loadMessagesFromFile($fallbackMessageFile);

			if (empty($messages)) {
				$messages = $fallbackMessages;
			} elseif (!empty($fallbackMessages)) {
				foreach ($fallbackMessages as $key => $value) {
					if (!empty($value) && empty($messages[$key])) {
						$messages[$key] = $fallbackMessages[$key];
					}
				}
			}
		}

		$this->basePath = $oldBasePath;

		return (array)$messages;
	}
}