<?php

namespace sail\i18n;

class I18N extends \yii\i18n\I18N
{
	/**
	 * @var bool intl扩展是否已加载
	 */
	private $_intlLoaded = false;

	/**
	 * @var array|null
	 */
	private $_allLocaleIds;

	/**
	 * @var
	 */
	private $_appLocales;

	/**
	 * @var bool|null
	 */
	private $_translationDebugOutput;

	/** @inheritdoc */
	public function init()
	{
		parent::init();

		$this->_intlLoaded = extension_loaded('intl');
	}

	/**
	 * @return bool
	 */
	public function getIsIntlLoaded()
	{
		return $this->_intlLoaded;
	}

	/**
	 * @param string $localeId
	 *
	 * @return Locale
	 */
	public function getLocaleById($localeId)
	{
		return new Locale($localeId);
	}

	public function getAllLocaleIds()
	{
		if ($this->_allLocaleIds === null) {
			$this->_allLocaleIds = ResourceBundle::getLocales(null);
		} else {
			
		}
	}
}