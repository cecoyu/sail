<?php

namespace sail\i18n;

use Sail;
use sail\helpers\Localization;
use DateTime;
use IntlDateFormatter;
use NumberFormatter;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\base\InvalidArgumentException;
use yii\base\BaseObject;
use yii\helpers\FormatConverter;

class Locale extends BaseObject
{
	/**
	 * @var int 正前缀
	 */
	const ATTR_POSITIVE_PREFIX = 0;

	/**
	 * @var int 正后缀
	 */
	const ATTR_POSITIVE_SUFFIX = 1;

	/**
	 * @var int 负前缀
	 */
	const ATTR_NEGATIVE_PREFIX = 2;

	/**
	 * @var int 负后缀
	 */
	const ATTR_NEGATIVE_SUFFIX = 3;

	/**
	 * @var int 填充宽度
	 */
	const ATTR_PADDING_CHARACTER = 4;

	/**
	 * @var int ISO货币代码
	 */
	const ATTR_CURRENCY_CODE = 5;

	/**
	 * @var int
	 */
	const ATTR_DEFAULT_RULESET = 6;

	/**
	 * @var int
	 */
	const ATTR_PUBLIC_RULESETS = 7;

	/**
	 * @var int 十进制样式
	 */
	const STYLE_DECIMAL = 1;

	/**
	 * @var int 货币样式
	 */
	const STYLE_CURRENCY = 2;

	/**
	 * @var int 百分比样式
	 */
	const STYLE_PERCENT = 3;

	/**
	 * @var int
	 */
	const STYLE_SCIENTIFIC = 4;

	/**
	 * @var int 小数分隔符
	 */
	const SYMBOL_DECIMAL_SEPARATOR = 0;

	/**
	 * @var int
	 */
	const SYMBOL_GROUPING_SEPARATOR = 1;

	/**
	 * @var int
	 */
	const SYMBOL_PATTERN_SEPARATOR = 2;

	/**
	 * @var int
	 */
	const SYMBOL_PERCENT = 3;

	/**
	 * @var int
	 */
	const SYMBOL_ZERO_DIGIT = 4;

	/**
	 * @var int
	 */
	const SYMBOL_DIGIT = 5;

	/**
	 * @var int
	 */
	const SYMBOL_MINUS_SIGN = 6;

	/**
	 * @var int
	 */
	const SYMBOL_PLUS_SIGN = 7;

	/**
	 * @var int
	 */
	const SYMBOL_CURRENCY = 8;

	/**
	 * @var int
	 */
	const SYMBOL_INTL_CURRENCY = 9;

	/**
	 * @var int
	 */
	const SYMBOL_MONETARY_SEPARATOR = 10;

	/**
	 * @var int
	 */
	const SYMBOL_EXPONENTIAL = 11;

	/**
	 * @var int
	 */
	const SYMBOL_PERMILL = 12;

	/**
	 * @var int
	 */
	const SYMBOL_PAD_ESCAPE = 13;

	/**
	 * @var int
	 */
	const SYMBOL_INFINITY = 14;

	/**
	 * @var int
	 */
	const SYMBOL_NAN = 15;

	/**
	 * @var int
	 */
	const SYMBOL_SIGNIFICANT_DIGIT = 16;

	/**
	 * @var int
	 */
	const SYMBOL_MONETARY_GROUPING_SEPARATOR = 17;

	/**
	 * @var string
	 */
	const LENGTH_ABBREVIATED = 'abbreviated';

	/**
	 * @var string
	 */
	const LENGTH_SHORT = 'short';

	/**
	 * @var string
	 */
	const LENGTH_MEDIUM = 'medium';

	/**
	 * @var string
	 */
	const LENGTH_LONG = 'long';

	/**
	 * @var string
	 */
	const LENGTH_FULL = 'full';

	/**
	 * @var string
	 */
	const FORMAT_ICU = 'icu';

	/**
	 * @var string
	 */
	const FORMAT_PHP = 'php';

	/**
	 * @var string
	 */
	const FORMAT_JUI = 'jui';

	/**
	 * @var array 使用RTL方向的语言
	 */
	private static $_rtlLanguages = ['ar', 'he', 'ur'];

	/**
	 * @var string|null locale ID
	 */
	public $id;

	/**
	 * @var array|null 如果[PHP intl]扩展未加载，则使用配置的区域设置数据
	 */
	private $_data;

	/**
	 * @var Formatter|null 本地化的格式
	 */
	private $_formatter;

	/**
	 * @param string $id    区域ID
	 * @param array $config 用于初始化对象属性的 名称-值 对
	 */
	public function __construct($id, $config = [])
	{
		if (strpos($id, '_') !== false) {
			$id = str_replace('_', '-', $id);
		}

		$this->id = $id;

		if (!Sail::$app->getI18n()->getIsIntlLoaded()) {
			$this->_data = Localization::localeData($this->id);

			if ($this->_data === null) {
				$this->_data = Localization::localeData('en-US');
			}

			parent::__construct($config);
		}
	}

	/**
	 * 使用ID作为区域设置的字符串表示形式
	 *
	 * @return string
	 */
	public function __toString()
	{
		return (string)$this->id;
	}

	/**
	 * 返回区域语言ID
	 *
	 * @return string 当前区域的语言ID
	 */
	public function getLanguageID()
	{
		if (($pos = strpos($this->id, '-')) !== false) {
			return substr($this->id, 0, $pos);
		}

		return $this->id;
	}

	/**
	 * 返回区域的脚本ID, 脚本ID仅包含区域ID中的破折号后的最后4个字符
	 *
	 * @return string|null
	 */
	public function getScriptID()
	{
		// 查找子标签
		if (($pos = strpos($this->id, '-')) !== false) {
			$subTag = explode('-', $this->id);

			// 脚本子标签可以与territory子标签的长度区分开来
			if (strlen($subTag[1]) === 4) {
				return $subTag[1];
			}
		}

		return null;
	}

	/**
	 * 返回区域的territory ID, territory ID仅包含区域ID中的破折号后的最后2到3个字母或数字
	 *
	 * @return string|null
	 */
	public function getTerritoryID()
	{
		// 查找子标签
		if (($pos = strpos($this->id, '-')) !== false) {
			$subTag = explode('-', $this->id);

			if (isset($subTag[2]) && strlen($subTag[2]) < 4) {
				return $subTag[2];
			}

			if (strlen($subTag[1]) < 4) {
				return $subTag[1];
			}
		}

		return null;
	}

	/**
	 * @param string|null $inLocale
	 *
	 * @return string
	 */
	public function getDisplayName($inLocale = null)
	{
		if ($inLocale === null) {
			$inLocale = $this->id;
		}

		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			return \Locale::getDisplayName($this->id, $inLocale);
		}

		if ($inLocale === $this->id) {
			$locale = $this;
		} else {
			try {
				$locale = new Locale($inLocale);
			} catch (InvalidConfigException $e) {
				$locale = $this;
			}
		}

		if (isset($locale->_data['localeDisplayNames'][$this->id])) {
			return $locale->_data['localeDisplayNames'][$this->id];
		}

		$languageId = $this->getLanguageID();

		if ($languageId !== $this->id && isset($locale->_data['localeDisplayNames'][$languageId])) {
			return $locale->_data['localeDisplayNames'][$languageId];
		}

		if ($locale !== $this) {
			return $this->getDisplayName($this->id);
		}

		return $this->id;
	}

	/**
	 * 返回语言的方向(ltr 或 rtl)
	 *
	 * @return string
	 */
	public function getOrientation()
	{
		if (in_array($this->getLanguageID(), self::$_rtlLanguages, true)) {
			return 'rtl';
		}

		return 'ltr';
	}

	/**
	 * 返回此区域的[[Formatter]]
	 *
	 * @return Formatter
	 */
	public function getFormatter()
	{
		if ($this->_formatter === null) {
			$config = [
				'locale' => $this->id,
			];

			if (!Sail::$app->getI18n()->getIsIntlLoaded()) {
				$config['dateTimeFormats']        = $this->_data['dateTimeFormats'];
				$config['standAloneMonthNames']   = $this->_data['standAloneMonthNames'];
				$config['monthNames']             = $this->_data['monthNames'];
				$config['standAloneWeekDayNames'] = $this->_data['standAloneWeekDayNames'];
				$config['weekDayNames']           = $this->_data['weekDayNames'];
				$config['amName']                 = $this->_data['amName'];
				$config['pmName']                 = $this->_data['pmName'];
				$config['currencySymbols']        = $this->_data['currencySymbols'];
				$config['decimalSeparator']       = $this->getNumberSymbol(self::SYMBOL_DECIMAL_SEPARATOR);
				$config['thousandSeparator']      = $this->getNumberSymbol(self::SYMBOL_GROUPING_SEPARATOR);
				$config['currencyCode']           = $this->getNumberSymbol(self::SYMBOL_INTL_CURRENCY);
			} else {
				$config['dateTimeFormats'] = [
					'short'  => [
						'date'     => $this->getDateFormat(Locale::LENGTH_SHORT),
						'time'     => $this->getTimeFormat(Locale::LENGTH_SHORT),
						'datetime' => $this->getDateTimeFormat(Locale::LENGTH_SHORT),
					],
					'medium' => [
						'date'     => $this->getDateFormat(Locale::LENGTH_MEDIUM),
						'time'     => $this->getTimeFormat(Locale::LENGTH_MEDIUM),
						'datetime' => $this->getDateTimeFormat(Locale::LENGTH_MEDIUM),
					],
					'full'   => [
						'date'     => $this->getDateFormat(Locale::LENGTH_FULL),
						'time'     => $this->getTimeFormat(Locale::LENGTH_FULL),
						'datetime' => $this->getDateTimeFormat(Locale::LENGTH_FULL),
					],
				];
			}

			$this->_formatter = new Formatter($config);
		}

		return $this->_formatter;
	}

	/**
	 * 返回本地化的ICU日期格式
	 *
	 * @param string|null $length
	 * @param string $format
	 *
	 * @return string
	 */
	public function getDateFormat($length = null, $format = self::FORMAT_ICU)
	{
		return $this->_getDateTimeFormat($length, true, false, $format);
	}

	/**
	 * 返回本地化的ICU时间格式
	 *
	 * @param string|null $length
	 * @param string $format
	 *
	 * @return string
	 */
	public function getTimeFormat($length = null, $format = self::FORMAT_ICU)
	{
		return $this->_getDateTimeFormat($length, false, true, $format);
	}

	/**
	 * 返回本地化的ICU日期+时间格式
	 *
	 * @param string|null $length
	 * @param string $format
	 *
	 * @return string
	 */
	public function getDateTimeFormat($length = null, $format = self::FORMAT_ICU)
	{
		return $this->_getDateTimeFormat($length, true, true, $format);
	}

	/**
	 * 返回本地化的月份名称
	 *
	 * @param int $month 月份(1-12)
	 * @param string|null $length
	 * @param bool $standAlone
	 *
	 * @return string
	 */
	public function getMonthName($month, $length = null, $standAlone = true)
	{
		if ($length === null) {
			$length = self::LENGTH_FULL;
		}

		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			$formatter = new IntlDateFormatter($this->id, IntlDateFormatter::NONE, IntlDateFormatter::NONE);

			switch ($length) {
				case self::LENGTH_ABBREVIATED:
					$formatter->setPattern($standAlone ? 'LLLLL' : 'MMMM');
					break;
				case self::LENGTH_SHORT:
				case self::LENGTH_MEDIUM:
					$formatter->setPattern($standAlone ? 'LLL' : 'MMM');
					break;
				default:
					$formatter->setPattern($standAlone ? 'LLLL' : 'MMMM');
					break;
			}

			return $formatter->format(new DateTime('1970-' . sprintf('%02d', $month) . '-01'));
		} else {
			$which = $standAlone ? 'standAloneMonthNames' : 'monthNames';

			switch ($length) {
				case self::LENGTH_ABBREVIATED:
					return $this->_data[$which]['abbreviated'][$month - 1];
					break;
				case self::LENGTH_SHORT:
				case self::LENGTH_MEDIUM:
					return $this->_data[$which]['medium'][$month - 1];
					break;
				default:
					return $this->_data[$which]['full'][$month - 1];
					break;
			}
		}
	}

	/**
	 * 返回所有本地化的月份名称
	 *
	 * @param string|null $length
	 * @param bool $standAlone
	 *
	 * @return array
	 */
	public function getMonthNames($length = null, $standAlone = true)
	{
		$monthNames = [];

		for ($month = 1; $month <= 12; $month++) {
			$monthNames[] = $this->getMonthName($month, $length, $standAlone);
		}

		return $monthNames;
	}

	/**
	 * 返回本地化的星期名称
	 *
	 * @param int $day 星期几(0-6),0代表星期日
	 * @param string|null $length
	 * @param bool $standAlone
	 *
	 * @return string
	 */
	public function getWeekDayName($day, $length = null, $standAlone = true)
	{
		if ($length === null) {
			$length = self::LENGTH_FULL;
		}

		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			$formatter = new IntlDateFormatter($this->id, IntlDateFormatter::NONE, IntlDateFormatter::NONE);

			switch ($length) {
				case self::LENGTH_ABBREVIATED:
					$formatter->setPattern($standAlone ? 'ccccc' : 'eeeee');
					break;
				case self::LENGTH_SHORT:
					$formatter->setPattern($standAlone ? 'cccccc' : 'eeeeee');
					break;
				case self::LENGTH_MEDIUM:
					$formatter->setPattern($standAlone ? 'ccc' : 'eee');
					break;
				default:
					$formatter->setPattern($standAlone ? 'cccc' : 'eeee');
					break;
			}

			// 1970-01-04 => Sunday (0 + 4)
			// 1970-01-05 => Monday (1 + 4)
			// 1970-01-06 => Tuesday (2 + 4)
			// 1970-01-07 => Wednesday (3 + 4)
			// 1970-01-08 => Thursday (4 + 4)
			// 1970-01-09 => Friday (5 + 4)
			// 1970-01-10 => Saturday (6 + 4)
			return $formatter->format(new DateTime('1970-01-' . sprintf('%02d', $day + 4)));
		} else {
			$which = $standAlone ? 'standAloneWeekDayNames' : 'weekDayNames';

			switch ($length) {
				case self::LENGTH_ABBREVIATED:
					return $this->_data[$which]['abbreviated'][$day];
				case self::LENGTH_SHORT:
					return $this->_data[$which]['short'][$day];
				case self::LENGTH_MEDIUM:
					return $this->_data[$which]['medium'][$day];
				default:
					return $this->_data[$which]['full'][$day];
			}
		}
	}

	/**
	 * 返回所有的本地化的星期名称
	 *
	 * @param string|null $length
	 * @param bool $standAlone
	 *
	 * @return array
	 */
	public function getWeekDayNames($length = null, $standAlone = true)
	{
		$weekDayNames = [];

		for ($day = 0; $day <= 6; $day++) {
			$weekDayNames[] = $this->getWeekDayName($day, $length, $standAlone);
		}

		return $weekDayNames;
	}

	/**
	 * 返回此区域的"AM"名称
	 *
	 * @return string
	 */
	public function getAMName()
	{
		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			return $this->getFormatter()->asDate(new DateTime('00:00'), 'a');
		}

		return $this->_data['amName'];
	}

	/**
	 * 返回此区域的"PM"名称
	 *
	 * @return string
	 */
	public function getPMName()
	{
		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			return $this->getFormatter()->asDate(new DateTime('12:00'), 'a');
		}

		return $this->_data['pmName'];
	}

	/**
	 * 返回此区域设置使用的文本属性
	 *
	 * @param int $attribute
	 *
	 * @return string
	 */
	public function getTextAttribute($attribute)
	{
		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			$formatter = new NumberFormatter($this->id, NumberFormatter::DECIMAL);

			return $formatter->getTextAttribute($attribute);
		}

		switch ($attribute) {
			case self::ATTR_POSITIVE_PREFIX:
				return $this->_data['textAttributes']['positivePrefix'];
			case self::ATTR_POSITIVE_SUFFIX:
				return $this->_data['textAttributes']['positiveSuffix'];
			case self::ATTR_NEGATIVE_PREFIX:
				return $this->_data['textAttributes']['negativePrefix'];
			case self::ATTR_NEGATIVE_SUFFIX:
				return $this->_data['textAttributes']['negativeSuffix'];
			case self::ATTR_PADDING_CHARACTER:
				return $this->_data['textAttributes']['paddingCharacter'];
			case self::ATTR_CURRENCY_CODE:
				return $this->_data['textAttributes']['currencyCode'];
			case self::ATTR_DEFAULT_RULESET:
				return $this->_data['textAttributes']['defaultRuleset'];
			case self::ATTR_PUBLIC_RULESETS:
				return $this->_data['textAttributes']['publicRulesets'];
		}

		return null;
	}

	/**
	 * 返回此区域设置使用的数字模式
	 *
	 * @param int $style
	 *
	 * @return string
	 */
	public function getNumberPattern($style)
	{
		if (Sail::$app->getI18n()->getIsIntLoaded()) {
			$formatter = new NumberFormatter($this->id, $style);

			return $formatter->getPattern();
		}

		switch ($style) {
			case self::STYLE_DECIMAL:
				return $this->_data['numberPatterns']['decimal'];
			case self::STYLE_CURRENCY:
				return $this->_data['numberPatterns']['currency'];
			case self::STYLE_PERCENT:
				return $this->_data['numberPatterns']['percent'];
			case self::STYLE_SCIENTIFIC:
				return $this->_data['numberPatterns']['scientific'];
		}

		return null;
	}

	/**
	 * 返回此区域设置使用的数字符号
	 *
	 * @param int $symbol
	 *
	 * @return string
	 */
	public function getNumberSymbol($symbol)
	{
		if (Craft::$app->getI18n()->getIsIntlLoaded()) {
			$formatter = new NumberFormatter($this->id, NumberFormatter::DECIMAL);

			return $formatter->getSymbol($symbol);
		}

		switch ($symbol) {
			case self::SYMBOL_DECIMAL_SEPARATOR:
				return $this->_data['numberSymbols']['decimalSeparator'];
			case self::SYMBOL_GROUPING_SEPARATOR:
				return $this->_data['numberSymbols']['groupingSeparator'];
			case self::SYMBOL_PATTERN_SEPARATOR:
				return $this->_data['numberSymbols']['patternSeparator'];
			case self::SYMBOL_PERCENT:
				return $this->_data['numberSymbols']['percent'];
			case self::SYMBOL_ZERO_DIGIT:
				return $this->_data['numberSymbols']['zeroDigit'];
			case self::SYMBOL_DIGIT:
				return $this->_data['numberSymbols']['digit'];
			case self::SYMBOL_MINUS_SIGN:
				return $this->_data['numberSymbols']['minusSign'];
			case self::SYMBOL_PLUS_SIGN:
				return $this->_data['numberSymbols']['plusSign'];
			case self::SYMBOL_CURRENCY:
				return $this->_data['numberSymbols']['currency'];
			case self::SYMBOL_INTL_CURRENCY:
				return $this->_data['numberSymbols']['intlCurrency'];
			case self::SYMBOL_MONETARY_SEPARATOR:
				return $this->_data['numberSymbols']['monetarySeparator'];
			case self::SYMBOL_EXPONENTIAL:
				return $this->_data['numberSymbols']['exponential'];
			case self::SYMBOL_PERMILL:
				return $this->_data['numberSymbols']['permill'];
			case self::SYMBOL_PAD_ESCAPE:
				return $this->_data['numberSymbols']['padEscape'];
			case self::SYMBOL_INFINITY:
				return $this->_data['numberSymbols']['infinity'];
			case self::SYMBOL_NAN:
				return $this->_data['numberSymbols']['nan'];
			case self::SYMBOL_SIGNIFICANT_DIGIT:
				return $this->_data['numberSymbols']['significantDigit'];
			case self::SYMBOL_MONETARY_GROUPING_SEPARATOR:
				return $this->_data['numberSymbols']['monetaryGroupingSeparator'];
		}

		return null;
	}

	/**
	 * 返回本地化的货币符号
	 *
	 * @param string $currency
	 *
	 * @return string
	 */
	public function getCurrencySymbol($currency)
	{
		if (Craft::$app->getI18n()->getIsIntlLoaded()) {
			$formatter = new NumberFormatter($this->id, NumberFormatter::CURRENCY);
			$formatter->setPattern('¤');
			$formatter->setAttribute(NumberFormatter::MAX_SIGNIFICANT_DIGITS, 0);
			$formattedPrice = $formatter->formatCurrency(0, $currency);
			$zero           = $formatter->getSymbol(NumberFormatter::ZERO_DIGIT_SYMBOL);

			return str_replace($zero, '', $formattedPrice);
		}

		if (isset($this->_data['currencySymbols'][$currency])) {
			return $this->_data['currencySymbols'][$currency];
		}

		return $currency;
	}

	/**
	 * 返回区域ID
	 *
	 * @return string
	 */
	public function getId()
	{
		Sail::$app->getDeprecator()
			->log('Locale::getId()', 'Locale::getId() has been deprecated. Use the id property instead.');

		return $this->id;
	}

	/**
	 * 返回给定区域的语言环境名称
	 *
	 * @param string|null $targetLocaleId
	 *
	 * @return string|null
	 */
	public function getName($targetLocaleId = null)
	{
		Sail::$app->getDeprecator()
			->log('Locale::getName()', 'Locale::getName() has been deprecated. Use getDisplayName() instead.');

		if ($targetLocaleId === null) {
			$targetLocaleId = Sail::$app->language;
		}

		return $this->getDisplayName($targetLocaleId);
	}

	public function getNativeName()
	{
		Sail::$app->getDeprecator()
			->log('Locale::getNativeName()', 'Locale::getNativeName() has been deprecated. Use getDisplayName() instead.');

		return $this->getDisplayName();
	}

	/**
	 * 返回本地化的日期/时间格式
	 *
	 * @param string $length
	 * @param bool $withDate
	 * @param bool $withTime
	 * @param  string $format
	 *
	 * @return string
	 */
	private function _getDateTimeFormat($length, $withDate, $withTime, $format)
	{
		$icuFormat = $this->_getDateTimeIcuFormat($length, $withDate, $withTime);

		if ($format !== self::FORMAT_ICU) {
			$type = ($withDate ? 'date' : '') . ($withTime ? 'time' : '');

			switch ($format) {
				case self::FORMAT_PHP:
					return FormatConverter::convertDateIcuToPhp($icuFormat, $type, $this->id);
				case self::FORMAT_JUI:
					return FormatConverter::convertDateIcuToJui($icuFormat, $type, $this->id);
			}
		}

		return $icuFormat;
	}

	/**
	 * 返回本地化的ICU日期/时间格式
	 *
	 * @param string $length
	 * @param bool $withDate
	 * @param bool $withTime
	 *
	 * @return string
	 *
	 * @throws Exception
	 */
	private function _getDateTimeIcuFormat($length, $withDate, $withTime)
	{
		if ($length === null) {
			$length = self::LENGTH_MEDIUM;
		}
		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			// Convert length to IntlDateFormatter constants
			switch ($length) {
				case self::LENGTH_FULL:
					/** @noinspection CallableParameterUseCaseInTypeContextInspection */
					$length = IntlDateFormatter::FULL;
					break;
				case self::LENGTH_LONG:
					/** @noinspection CallableParameterUseCaseInTypeContextInspection */
					$length = IntlDateFormatter::LONG;
					break;
				case self::LENGTH_MEDIUM:
					/** @noinspection CallableParameterUseCaseInTypeContextInspection */
					$length = IntlDateFormatter::MEDIUM;
					break;
				case self::LENGTH_SHORT:
					/** @noinspection CallableParameterUseCaseInTypeContextInspection */
					$length = IntlDateFormatter::SHORT;
					break;
				default:
					throw new Exception('Invalid date/time format length: ' . $length);
			}
			$dateType  = ($withDate ? $length : IntlDateFormatter::NONE);
			$timeType  = ($withTime ? $length : IntlDateFormatter::NONE);
			$formatter = new IntlDateFormatter($this->id, $dateType, $timeType);
			$pattern   = $formatter->getPattern();

			// Use 4-digit year, and no leading zeroes on days/months
			return strtr($pattern, [
				'yyyy'  => 'yyyy',
				'yy'    => 'yyyy',
				'MMMMM' => 'MMMMM',
				'MMMM'  => 'MMMM',
				'MMM'   => 'MMM',
				'MM'    => 'M',
				'dd'    => 'd',
			]
			);
		}
		$type = ($withDate ? 'date' : '') . ($withTime ? 'time' : '');
		switch ($length) {
			case self::LENGTH_SHORT:
				return $this->_data['dateTimeFormats']['short'][$type];
			case self::LENGTH_MEDIUM:
				return $this->_data['dateTimeFormats']['medium'][$type];
			case self::LENGTH_LONG:
				return $this->_data['dateTimeFormats']['long'][$type];
			case self::LENGTH_FULL:
				return $this->_data['dateTimeFormats']['full'][$type];
		}

		return null;
	}
}