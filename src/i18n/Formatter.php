<?php

namespace sail\i18n;

use Sail;
use sail\helpers\DT;
use DateTime;
use DateTimeZone;
use NumberFormatter;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\helpers\FormatConverter;

class Formatter extends \yii\i18n\Formatter
{
	/**
	 * @var array
	 */
	public $dateTimeFormats;

	/**
	 * @var array|null
	 */
	public $standAloneMonthNames;

	/**
	 * @var array|null
	 */
	public $monthNames;

	/**
	 * @var array|null
	 */
	public $standAloneWeekDayNames;

	/**
	 * @var array|null
	 */
	public $weekDayNames;

	/**
	 * @var string|null
	 */
	public $amName;

	/**
	 * @var string|null
	 */
	public $pmName;

	/**
	 * @var array|null
	 */
	public $currencySymbols;

	/**
	 * @param int|string|dateTime $value
	 * @param string|null $format
	 *
	 * @return string
	 */
	public function asDate($value, $format = null)
	{
		if (is_null($format)) {
			$format = $this->dateFormat;
		}

		if (isset($this->dateTimeFormats[$format]['date'])) {
			$format = $this->dateTimeFormats[$format]['date'];
		}

		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			return parent::asDate($value, $format);
		}

		return $this->_formatDateTimeValue($value, $format, 'date');
	}

	/**
	 * @inheritdoc
	 *
	 * @param int|string|DateTime $value
	 * @param string|null         $format
	 *
	 * @return string
	 * @throws InvalidParamException
	 * @throws InvalidConfigException
	 */
	public function asTime($value, $format = null)
	{
		if (is_null($format)) {
			$format = $this->timeFormat;
		}

		if (isset($this->dateTimeFormats[$format]['time'])) {
			$format = $this->dateTimeFormats[$format]['time'];
		}

		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			return parent::asTime($value, $format);
		}

		return $this->_formatDateTimeValue($value, $format, 'time');
	}

	/**
	 * @inheritdoc
	 *
	 * @param int|string|DateTime $value
	 * @param string|null         $format
	 *
	 * @return string
	 * @throws InvalidParamException
	 * @throws InvalidConfigException
	 */
	public function asDatetime($value, $format = null)
	{
		if ($format === null) {
			$format = $this->datetimeFormat;
		}

		if (isset($this->dateTimeFormats[$format]['datetime'])) {
			$format = $this->dateTimeFormats[$format]['datetime'];
		}

		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			return parent::asDatetime($value, $format);
		}

		return $this->_formatDateTimeValue($value, $format, 'datetime');
	}

	public function asTimestamp($value, $format = null)
	{
		/** @var DateTime $timestamp */
		/** @var bool $hasTimeInfo */
		/** @var bool $hasDateInfo */
		list($timestamp, $hasTimeInfo, $hasDateInfo) = $this->normalizeDatetimeValue($value, true);

		if (!$hasDateInfo || DT::isToday($timestamp)) {
			return $hasTimeInfo ? $this->asTime($timestamp, $format) : Sail::t('app', 'Today');
		}

		if (DT::isYesterday($timestamp)) {
			return Sail::t('app', 'Yesterday');
		}

		if (DT::isWithinLast($timestamp, '7 days')) {
			$day = $timestamp->format('w');
			Sail::$app->getI18n()->getLocaleById($this->locale)->getWeekDayName($day);
		}

		return $this->asDate($timestamp, $format);
	}

	public function asCurrency($value, $currency = null, $options = [], $textOptions = [], $stripZeros = false)
	{
		$omitDecimals = ($stripZeros && (int)$value == $value);

		if (Sail::$app->getI18n()->getIsIntlLoaded()) {
			if ($omitDecimals) {
				$options[NumberFormatter::MAX_FRACTION_DIGITS] = 0;
				$options[NumberFormatter::MIN_FRACTION_DIGITS] = 0;
			}

			return parent::asCurrency($value, $currency, $options, $textOptions);
		}

		if (is_null($value)) {
			return $this->nullDisplay;
		}

		$value = $this->normalizeNumericValue($value);

		if (is_null($currency)) {
			if (is_null($this->currencyCode)) {
				throw new InvalidConfigException('The default currency code for the formatter is not defined.');
			}

			$currency = $this->currencyCode;
		}

		if (isset($this->currencySymbols[$currency])) {
			$currency = $this->currencySymbols[$currency];
		}

		$decimals = $omitDecimals ? 0 : 2;

		return $currency . $this->asDecimal($value, $decimals, $options, $textOptions);
	}

	public function asText($value)
	{
		if ($value instanceof DateTime) {
			return $this->asDatetime($value);
		}

		return parent::asText($value);
	}

	private function _formatDateTimeValue($value, $format, $type)
	{
		$timeZone = $this->timeZone;

		// Avoid time zone conversion for date-only values
		if ($type === 'date') {
			list($timestamp, $hasTimeInfo) = $this->normalizeDatetimeValue($value, true);

			if (!$hasTimeInfo) {
				$timeZone = $this->defaultTimeZone;
			}
		} else {
			$timestamp = $this->normalizeDatetimeValue($value);
		}

		if ($timestamp === null) {
			return $this->nullDisplay;
		}

		if (strpos($format, 'php:') === 0) {
			$format = substr($format, 4);
			$format = FormatConverter::convertDatePhpToIcu($format);
		}

		if ($timeZone != null) {
			$timestamp->setTimezone(new DateTimeZone($timeZone));
		}

		// Parse things that we can translate before passing it off to DateTime::format()
		$tr = [];

		if (preg_match_all('/(?<!\')\'.*?[^\']\'(?!\')/', $format, $matches)) {
			foreach ($matches[0] as $match) {
				$tr[$match] = $match;
			}
		}

		if ($this->standAloneMonthNames !== null || $this->monthNames !== null) {
			$month = $timestamp->format('n') - 1;

			if ($this->standAloneMonthNames !== null) {
				$tr['LLLLL'] = '\''.$this->standAloneMonthNames['abbreviated'][$month].'\'';
				$tr['LLLL'] = '\''.$this->standAloneMonthNames['full'][$month].'\'';
				$tr['LLL'] = '\''.$this->standAloneMonthNames['medium'][$month].'\'';
			}

			if ($this->monthNames !== null) {
				$tr['MMMMM'] = '\''.$this->monthNames['abbreviated'][$month].'\'';
				$tr['MMMM'] = '\''.$this->monthNames['full'][$month].'\'';
				$tr['MMM'] = '\''.$this->monthNames['medium'][$month].'\'';
			}
		}

		if ($this->standAloneWeekDayNames !== null || $this->weekDayNames !== null) {
			$day = $timestamp->format('w');

			if ($this->standAloneWeekDayNames !== null) {
				$tr['cccccc'] = '\''.$this->standAloneWeekDayNames['short'][$day].'\'';
				$tr['ccccc'] = '\''.$this->standAloneWeekDayNames['abbreviated'][$day].'\'';
				$tr['cccc'] = '\''.$this->standAloneWeekDayNames['full'][$day].'\'';
				$tr['ccc'] = '\''.$this->standAloneWeekDayNames['medium'][$day].'\'';
			}

			if ($this->weekDayNames !== null) {
				$tr['EEEEEE'] = '\''.$this->weekDayNames['short'][$day].'\'';
				$tr['EEEEE'] = '\''.$this->weekDayNames['abbreviated'][$day].'\'';
				$tr['EEEE'] = '\''.$this->weekDayNames['full'][$day].'\'';
				$tr['EEE'] = '\''.$this->weekDayNames['medium'][$day].'\'';
				$tr['EE'] = '\''.$this->weekDayNames['medium'][$day].'\'';
				$tr['E'] = '\''.$this->weekDayNames['medium'][$day].'\'';

				$tr['eeeeee'] = '\''.$this->weekDayNames['short'][$day].'\'';
				$tr['eeeee'] = '\''.$this->weekDayNames['abbreviated'][$day].'\'';
				$tr['eeee'] = '\''.$this->weekDayNames['full'][$day].'\'';
				$tr['eee'] = '\''.$this->weekDayNames['medium'][$day].'\'';
			}
		}

		$amPmName = $timestamp->format('a').'Name';

		if ($this->$amPmName !== null) {
			$tr['a'] = '\''.$this->$amPmName.'\'';
		}

		if (!empty($tr)) {
			$format = strtr($format, $tr);
		}

		$format = FormatConverter::convertDateIcuToPhp($format, $type, $this->locale);

		return $timestamp->format($format);
	}
}